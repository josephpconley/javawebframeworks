package joejava.rest.client;

import java.io.IOException;

import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

public class FirstClientResource {
	public static void main(String[] args) throws ResourceException, IOException{
		new ClientResource("http://www.restlet.org").get().write(System.out);
	}
}
