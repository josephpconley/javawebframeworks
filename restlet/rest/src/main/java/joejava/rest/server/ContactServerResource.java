package joejava.rest.server;

import joejava.rest.model.Contact;
import joejava.rest.resource.ContactResource;

import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

public class ContactServerResource extends ServerResource implements ContactResource{

	private static volatile Contact contact = 
        new Contact("Martin", "Kaymer", 26); //new Address("10 bd Google", null, "20010", "Mountain View", "USA")

		@Get
		public Representation retrieve() {
			return contact.toHTML();
		}
		
		@Put
		public void store(Contact contact) {
			ContactServerResource.contact = contact;
		}
		
		@Delete
		public void remove() {
			contact = null;
		}
	
	
}
