package joejava.rest.server;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;

public class TestServerApplication extends Application {

    /**
     * When launched as a standalone application.
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Component component = new Component();
        component.getServers().add(Protocol.HTTP, 8180);
        component.getDefaultHost().attach(new TestServerApplication());
        component.start();
    }
    
    @Override
    public synchronized Restlet createInboundRoot() {
    	Router router = new Router(getContext());
        router.attach("/contacts/123", ContactServerResource.class);
        
        return router;
    }
    
}