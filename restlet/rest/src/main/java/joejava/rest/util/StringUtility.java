/**
 *  Copyright BDP International, Inc.
 */
package joejava.rest.util;

import java.lang.reflect.Method;

import javax.xml.bind.JAXBException;

import joejava.rest.model.Contact;

public class StringUtility{
     
	public static void main(String[] args) throws JAXBException{
		Contact kaymer = new Contact("Martin", "Kaymer", 26); //new Address("10 bd Google", null, "20010", "Mountain View", "USA")
		System.out.println(kaymer.toXML());
	}

	public static String getBean(Object bean)	{
		StringBuffer buffer=new StringBuffer();
		Class objClass=bean.getClass();
		Method[] methods=objClass.getMethods();
		buffer.append(bean.getClass().getName()+"[");
		
		for (Method method : methods){
			if (method.getName().startsWith("get") && method.getParameterTypes().length==0){
				String name=method.getName();
				buffer.append(name);
				buffer.append("=");
				try	{
					buffer.append(method.invoke(bean));
				}catch(Exception exp){
					exp.printStackTrace();
				}
				buffer.append(" ");
			}
		}
		buffer.append("]");
		return buffer.toString();
	}
}
