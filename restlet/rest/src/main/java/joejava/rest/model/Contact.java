package joejava.rest.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import joejava.rest.util.HibernateUtil;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@XmlRootElement(name="contact")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
public class Contact{

	@XmlAttribute
	private Integer id;
	
	@XmlElement
    private String firstName;

	@XmlElement
    private String lastName;

    //private Address homeAddress;

	@XmlElement
    private int age;

	//Needed for reflection (Hibernate, JAXB)
    public Contact() {}

    public Contact(String firstName, String lastName, int age){ //Address homeAddress,) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        //this.homeAddress = homeAddress;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

	public void setId(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	
	public void save(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			int cityId = (Integer)session.save(this);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}		
	}
	
    public Representation toHTML(){
    	String str = "";
    	Configuration cfg = new Configuration();
    	ClassTemplateLoader ctl = new ClassTemplateLoader(this.getClass(),"/");
    	cfg.setTemplateLoader(ctl);
    	
    	try {
			Template temp = cfg.getTemplate("contact.html");
			final Map<String, Object> dataModel = new TreeMap<String, Object>();
	        dataModel.put("contact", this);
	    	
	    	//str = this.firstName + " " + this.lastName;

	        Writer out = new StringWriter();
	        temp.process(dataModel, out);
	        str = out.toString();
	        
	        out.flush();			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
    	
    	return new StringRepresentation(str, MediaType.TEXT_HTML);
    }

    public String toXML() throws JAXBException{
		JAXBContext ctx = JAXBContext.newInstance(Contact.class);
		Marshaller m = ctx.createMarshaller();
		StringWriter writer = new StringWriter();
		
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(this, writer);

		return writer.toString();
    }
}
