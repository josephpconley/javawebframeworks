/** 
 *  Copyright BDP International, Inc.
 */
package joejava.pensieve.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import joejava.ibatis.bean.EmailContact;
import joejava.ibatis.DAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AjaxAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		String type = request.getParameter("type");
		String value = request.getParameter("value");
		
		if("EMAIL".equals(type)){
			List<EmailContact> emails = (List<EmailContact>)DAO.client.queryForList("getEmailAddresses", value);
			session.setAttribute("emails", emails);
			return mapping.findForward("email_results");
		}else if("CATEGORY".equals(type)){
			List<String> categories = (List<String>)DAO.client.queryForList("getCategories",value);
			session.setAttribute("categories", categories);
			return mapping.findForward("category_results");
		}else if("FREQUENCY".equals(type)){
			List<String> frequencies = (List<String>)DAO.client.queryForList("getFrequencies",value);
			session.setAttribute("frequencies", frequencies);
			return mapping.findForward("frequency_results");
		}
		return mapping.findForward("");
	}
}
