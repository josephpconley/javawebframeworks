package joejava.pensieve.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import joejava.pensieve.struts.form.NoteForm;
import joejava.ibatis.bean.Note;
import joejava.ibatis.DAO;
import joejava.pensieve.util.StringUtility;

public class SaveNoteAction extends Action {
	
	boolean newNote = false;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		NoteForm bean = (NoteForm) form;
		
		DAO.client.startTransaction();
		
		Note note = new Note();
			note.setId(Integer.valueOf(bean.getId()));
			note.setCategory(StringUtility.clean(bean.getCategory()));
			note.setContent(StringUtility.clean(bean.getContent()));
			note.setStartDate(StringUtility.getDate(bean.getStartDay(),
						bean.getStartMonth(),bean.getStartYear()));
			note.setEndDate(StringUtility.getDate(bean.getEndDay(),
					bean.getEndMonth(),bean.getEndYear()));
			if(bean.getStartMinutes().length() > 0){
				note.setStartTime(StringUtility.getTime(bean.getStartHours(),
						Integer.valueOf(bean.getStartMinutes()),bean.getStartMeridiem()));
			}
			if(bean.getEndMinutes().length() > 0){
				note.setEndTime(StringUtility.getTime(bean.getEndHours(),
				Integer.valueOf(bean.getEndMinutes()),bean.getEndMeridiem()));
			}
			note.setFrequency(StringUtility.clean(bean.getFrequency()));
			note.setReminder(bean.isReminder());
			
		if(note.getId() == 0){
			DAO.client.insert("insertNote",note);
		}else{
			DAO.client.update("updateNote",note);
		}
		
		DAO.client.commitTransaction();
		DAO.client.endTransaction();
		
		HttpSession session = request.getSession();
		return mapping.findForward("status");
	}
}

