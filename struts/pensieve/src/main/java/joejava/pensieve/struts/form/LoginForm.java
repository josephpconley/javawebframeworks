package joejava.pensieve.struts.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class LoginForm extends ActionForm {
	
	private static final long serialVersionUID = 2412743594872922101L;
	
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors actionErrors = new ActionErrors();
		String user = request.getParameter("user");
				
		if(user == null || user.length() == 0) {
			if(this.username == null || this.username.length() == 0) {
				ActionMessage msg = new ActionMessage("LoginForm.username.required");
				actionErrors.add("userName",msg);
				return actionErrors;
			}
			if(this.password == null || this.password.length() == 0) {
				ActionMessage msg = new ActionMessage("LoginForm.password.required");
				actionErrors.add("pass",msg);
				return actionErrors;
			}
		}
		return actionErrors;
	}
}
