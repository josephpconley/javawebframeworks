package joejava.pensieve.struts.action;

import java.sql.SQLException;
import java.util.List;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import joejava.ibatis.DAO;

import joejava.ibatis.bean.Contact;

public class ShowContactsAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		List<Contact> contacts = null;
		try {
			contacts = (List<Contact>)DAO.client.queryForList("getAllContacts",null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		//Destroy any previous sessions and create a new one
		HttpSession session = request.getSession();
		
		session.invalidate();
		session = request.getSession(true);
		request.setAttribute("contacts",contacts);
		
		return mapping.findForward("contacts");
	}
}
