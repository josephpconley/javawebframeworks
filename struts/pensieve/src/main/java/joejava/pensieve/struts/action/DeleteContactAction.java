package joejava.pensieve.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import joejava.ibatis.DAO;
/**
 * Class: DeleteISFAction
 *
 * Description:  
 * Created on Dec 30, 2008
 *
 * @author 
 * @version 
 */
public class DeleteContactAction extends Action {
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		
		DAO.client.startTransaction();
		
		try {

			DAO.client.delete("deleteContact",id);
			
			DAO.client.commitTransaction();
			DAO.client.endTransaction();
			
			session.setAttribute("message" ,"Contact was deleted successfully");
			
		} catch (Exception e) {
			DAO.client.endTransaction();
			e.printStackTrace(System.out);
			session.setAttribute("message" ,"Unable to delete Contact"); 
		}
		
		return mapping.findForward("status");
	}
}
