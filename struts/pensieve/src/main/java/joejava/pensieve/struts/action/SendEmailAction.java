package joejava.pensieve.struts.action;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import joejava.pensieve.struts.form.EmailForm;
import joejava.util.EmailUtility;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SendEmailAction extends Action{
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		EmailForm bean = (EmailForm) form;
		EmailUtility.sendSimpleEmail(bean.getTo(),"joe@pensieve.com", bean.getSubject(), bean.getText());
	    return mapping.findForward("status");
	}
}