package joejava.pensieve.struts.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import joejava.ibatis.bean.Contact;
import joejava.ibatis.DAO;

public class ContactForm extends ActionForm {

	private String firstName;
	private String lastName;
	private String id;
	private String birthDay, birthMonth, birthYear;
	private String address1,address2,address3,address4;
	private String phone1,phone2,phone3,phone4;
	private String email1,email2,email3,email4;
	private String cellService;
	
	DateFormat formatMonth = new SimpleDateFormat("MMM");
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getBirthMonth() {
		return birthMonth;
	}
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}
	public String getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getAddress4() {
		return address4;
	}
	public void setAddress4(String address4) {
		this.address4 = address4;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getPhone4() {
		return phone4;
	}
	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public String getEmail2() {
		return email2;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}
	public String getEmail3() {
		return email3;
	}
	public void setEmail3(String email3) {
		this.email3 = email3;
	}
	public String getEmail4() {
		return email4;
	}
	public void setEmail4(String email4) {
		this.email4 = email4;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		String id = request.getParameter("id");
		
		if(id != null) {
			try{
				Contact contact = (Contact)DAO.client.queryForObject("getContactById",id);
				if(contact != null){
					fillInForm(contact);
					request.setAttribute("contact", contact);
				}
			}catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}
	}
	private void fillInForm(Contact contact) {
		
		this.setId(contact.getId());
		
		StringTokenizer sb = new StringTokenizer(contact.getId());
		this.setFirstName(sb.nextToken());
		
		if(sb.hasMoreTokens())
			contact.setLastName(sb.nextToken());
		
		if(contact.getBday() != null){
			this.setBirthDay(Integer.toString(contact.getBday().getDate()));
			this.setBirthYear(Integer.toString(contact.getBday().getYear()+1900));
			this.setBirthMonth(formatMonth.format(contact.getBday()).toUpperCase());
		}
		this.setAddress1(contact.getAddress1());
		this.setAddress2(contact.getAddress2());
		this.setAddress3(contact.getAddress3());
		this.setAddress4(contact.getAddress4());
		this.setPhone1(contact.getPhone1());
		this.setPhone2(contact.getPhone2());
		this.setPhone3(contact.getPhone3());
		this.setPhone4(contact.getPhone4());
		this.setEmail1(contact.getEmail1());
		this.setEmail2(contact.getEmail2());
		this.setEmail3(contact.getEmail3());
		this.setEmail4(contact.getEmail4());	
	}
	public String getCellService() {
		return cellService;
	}
	public void setCellService(String cellService) {
		this.cellService = cellService;
	}
}
