package joejava.pensieve.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Class: LogoutAction
 *
 * Description:  Logs the contact out of the system.  Destroys the session.
 * Created on Jun 1, 2009
 *
 * @author Joe Conley
 * @version 
 */
public class LogoutAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession(false);
		String page = request.getParameter("page");
				
		session.invalidate();
		
		return mapping.findForward("index");
	}
}