package joejava.pensieve.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import joejava.ibatis.DAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteNoteAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String id = request.getParameter("id");
		
		DAO.client.startTransaction();
		
		try {

			DAO.client.delete("deleteNote",Integer.valueOf(id));
			
			DAO.client.commitTransaction();
			DAO.client.endTransaction();
			
		} catch (Exception e) {
			DAO.client.endTransaction();
			e.printStackTrace(System.out); 
		}
		
		HttpSession session = request.getSession();
		String type = (String) session.getAttribute("type");
		return mapping.findForward("status");
	}
}
