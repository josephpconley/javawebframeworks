package joejava.pensieve.struts.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import joejava.ibatis.bean.Category;
import joejava.ibatis.bean.Day;
import joejava.ibatis.bean.Schema;
import joejava.ibatis.DAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ShowNotesAction extends Action {

	private static DateFormat yearFormat = new SimpleDateFormat("yyyy");
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		
		session.invalidate();
		session = request.getSession(true);
		
		List<Day> days = new ArrayList<Day>();
		List<Category> categories = new ArrayList<Category>();
		Calendar c = Calendar.getInstance(); 
		int day = c.get(Calendar.DAY_OF_YEAR);

		
		//account for bordering years later
		for(int i=0;i<30;i++){
			c.set(Calendar.DAY_OF_YEAR, day+i);
			Day d = new Day(c.getTime());
			
			//If note(s) exists for this day...
			if(d.getNotes() != null && d.getNotes().size()>0)
				days.add(d);
		}
		
		List<String> categoryList = (List<String>)DAO.client.queryForList("getAllCategories",null);
		for(String category : categoryList){
			categories.add(new Category(category));
		}
			
		request.setAttribute("days",days);
		request.setAttribute("categories",categories);
		
		return mapping.findForward("notes");
	}
}
