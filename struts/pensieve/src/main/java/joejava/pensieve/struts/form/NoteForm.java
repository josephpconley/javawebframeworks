package joejava.pensieve.struts.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;

import joejava.ibatis.bean.Note;
import joejava.ibatis.DAO;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class NoteForm extends ActionForm {
	
	private String id;
	private String startDay;
	private String startMonth;
	private String startYear;
	private String endDay;
	private String endMonth;
	private String endYear;	
	private int startHours;
	private String startMinutes;
	private String startMeridiem;
	private int endHours;
	private String endMinutes;
	private String endMeridiem;	
	private String category;
	private String content;
	private String frequency;
	private boolean reminder;
	
	DateFormat formatMonth = new SimpleDateFormat("MMM");
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStartDay() {
		return startDay;
	}
	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}
	public String getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	public String getStartYear() {
		return startYear;
	}
	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}
	public String getEndDay() {
		return endDay;
	}
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	public String getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	public String getEndYear() {
		return endYear;
	}
	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getStartHours() {
		return startHours;
	}
	public void setStartHours(int startHours) {
		this.startHours = startHours;
	}
	public String getStartMinutes() {
		return startMinutes;
	}
	public void setStartMinutes(String startMinutes) {
		this.startMinutes = startMinutes;
	}
	public String getEndMinutes() {
		return endMinutes;
	}
	public void setEndMinutes(String endMinutes) {
		this.endMinutes = endMinutes;
	}
	public String getStartMeridiem() {
		return startMeridiem;
	}
	public void setStartMeridiem(String startMeridiem) {
		this.startMeridiem = startMeridiem;
	}
	public int getEndHours() {
		return endHours;
	}
	public void setEndHours(int endHours) {
		this.endHours = endHours;
	}
	public String getEndMeridiem() {
		return endMeridiem;
	}
	public void setEndMeridiem(String endMeridiem) {
		this.endMeridiem = endMeridiem;
	}
	public boolean isReminder() {
		return reminder;
	}
	public void setReminder(boolean reminder) {
		this.reminder = reminder;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		String idRefNo = request.getParameter("id");
		
		if(idRefNo != null) {
			try {
				Note note = (Note)DAO.client.queryForObject("getNoteById",Integer.valueOf(idRefNo));
				if(note != null){
					fillInForm(note);
					request.setAttribute("note", note);
				}
				//Get the errors associated with the filing (if they exist)
				//List<Error> errors = (List<Error>)DAO.client.queryForList("getISFErrors",idRefNo);
				//request.setAttribute("errors", errors);
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
		}
	}
	
	public void fillInForm(Note data) {
		this.id = Integer.toString(data.getId());
		this.category = data.getCategory();
		this.content = data.getContent(); 
		this.reminder = data.isReminder();
		this.frequency = data.getFrequency();
		
		if(data.getStartDate() != null){
			this.startDay = Integer.toString(data.getStartDate().getDate());
			this.startYear = Integer.toString(data.getStartDate().getYear() + 1900);
			this.startMonth = formatMonth.format(data.getStartDate()).toUpperCase();
		}
		if(data.getEndDate() != null){
			this.endDay = Integer.toString(data.getEndDate().getDate());
			this.endYear = Integer.toString(data.getEndDate().getYear() + 1900);
			this.endMonth = formatMonth.format(data.getEndDate()).toUpperCase();
		}
		if(data.getStartTime() != null){
			this.startHours = data.getStartTime().getHours() % 12;
			this.startMinutes = Integer.toString(data.getStartTime().getMinutes());
			
			if(this.startMinutes.length() == 1)
				this.startMinutes = "0"+this.startMinutes;
			
			if(data.getStartTime().getHours() > 11)
				this.startMeridiem = "PM";
			else
				this.startMeridiem = "AM";
		}
		if(data.getEndTime() != null){
			this.endHours = data.getEndTime().getHours() % 12;
			this.endMinutes = Integer.toString(data.getEndTime().getMinutes());
			
			if(this.endMinutes.length() == 1)
				this.endMinutes = "0"+this.endMinutes;
			
			if(data.getEndTime().getHours() > 11)
				this.endMeridiem = "PM";
			else
				this.endMeridiem = "AM";
		}
	}
}
