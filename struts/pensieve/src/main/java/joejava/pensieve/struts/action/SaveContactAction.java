package joejava.pensieve.struts.action;

import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import joejava.pensieve.struts.form.ContactForm;
import joejava.ibatis.bean.Contact;
import joejava.ibatis.DAO;
import joejava.pensieve.util.StringUtility;

public class SaveContactAction extends Action {
	
	boolean newContact = true;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		ContactForm bean = (ContactForm) form;
		
		//Find if Contact is new
		int id = (Integer)DAO.client.queryForObject("isContact",bean.getId());
		
		System.out.println(id);
		
		if(id > 0)
			newContact = false;
		
		DAO.client.startTransaction();
		
		Contact contact = new Contact();
			contact.setId(bean.getId());
			//System.out.println(contact.getID());
			
			StringTokenizer sb = new StringTokenizer(bean.getId());
			
			contact.setFirstName(sb.nextToken());
			
			if(sb.hasMoreTokens())
				contact.setLastName(sb.nextToken());
			
			contact.setBday(StringUtility.getDate(bean.getBirthDay(),bean.getBirthMonth(),bean.getBirthYear()));
			contact.setAddress1(StringUtility.clean(bean.getAddress1()));
			contact.setAddress2(StringUtility.clean(bean.getAddress2()));
			contact.setAddress3(StringUtility.clean(bean.getAddress3()));
			contact.setAddress4(StringUtility.clean(bean.getAddress4()));
			contact.setPhone1(StringUtility.clean(bean.getPhone1()));
			contact.setPhone2(StringUtility.clean(bean.getPhone2()));
			contact.setPhone3(StringUtility.clean(bean.getPhone3()));
			contact.setPhone4(StringUtility.clean(bean.getPhone4()));
			contact.setEmail1(StringUtility.clean(bean.getEmail1()));
			contact.setEmail2(StringUtility.clean(bean.getEmail2()));
			contact.setEmail3(StringUtility.clean(bean.getEmail3()));
			contact.setEmail4(StringUtility.clean(bean.getEmail4()));
			contact.setCellService(StringUtility.clean(bean.getCellService()));
			
		if(newContact){
			DAO.client.insert("insertContact",contact);
		}else{
			DAO.client.update("updateContact",contact);
		}
		
		DAO.client.commitTransaction();
		DAO.client.endTransaction();
		
		return mapping.findForward("status");
	}
}

