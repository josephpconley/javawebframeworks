package joejava.pensieve.struts.action;

import java.util.HashMap;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import joejava.ibatis.DAO;
import joejava.ibatis.bean.User;
import joejava.pensieve.struts.form.LoginForm;

public class LoginAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
				
		LoginForm bean = (LoginForm) form;
		String username = request.getParameter("user");
		
		HashMap<String,String> params = new HashMap<String,String>();
		
		if(username != null && username.equals("jolty")){
			return mapping.findForward("contacts");
		}else{
			params.put("username",bean.getUsername());
			params.put("password",bean.getPassword());
			
			User user = (User)DAO.client.queryForObject("getValidUser",params);
			if(user == null){
				request.setAttribute("message", "Invalid username/Password");
				return mapping.findForward("index");
			}else{
				//Destroy any previous sessions and create a new one
				HttpSession session = request.getSession();
				
				session.invalidate();
				session = request.getSession(true);
				session.setAttribute("user",user);
				
				return mapping.findForward("contacts");
			}
		}
	}
}
