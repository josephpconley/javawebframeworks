package joejava.pensieve.servlets;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import joejava.ibatis.bean.Contact;
import joejava.ibatis.DAO;

public class TestServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	      				throws ServletException, IOException {
	
		PrintWriter out = response.getWriter();
	    List<Contact> contacts = null;
	    DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	    
	    try {
			contacts = (List<Contact>)DAO.client.queryForList("getAllContacts",null);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    for(Contact c : contacts){
	    	if(c.getBday() != null){
	    		String str = format.format(c.getBday());
	    		out.println(c.getFirstName() + " " + c.getLastName() + " " +str);
	    	}
	    }
		
	  }
}
