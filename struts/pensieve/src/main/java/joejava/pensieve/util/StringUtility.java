/** 
 *  Copyright BDP International, Inc.
 */
package joejava.pensieve.util;

import java.lang.reflect.Method;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtility {
	
	public static String clean(String str){
		if(str.length()==0){
			return null;
		}else{
			return str;
		}
	}
	
	public static Date getDate(String day, String month, String year) {
		DateFormat dFormat = new SimpleDateFormat("ddMMMyyyy");
		Date d = null;
		try {
			d = dFormat.parse(day + month + year);
		} catch (ParseException e) {}
		return d;
	}
	
	public static String today(){
		Date today = new Date();
		return today.toString();
	}
	
	public static Date getDateTime(String value) {
		DateFormat tFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = null;
		try {
			d = tFormat.parse(value);
		} catch (ParseException e) {}
		return d;
	}
	
	public static Time getTime(int hours, int minutes, String meridiem){
		if(meridiem.equals("PM")){
			hours = hours + 12;
		}
		
		return new Time(hours,minutes,0);
	}
	
	public static String printBean(Object bean)	{
		
		StringBuffer buffer=new StringBuffer();
		Class objClass=bean.getClass();
		Method[] methods=objClass.getMethods();
		buffer.append(bean.getClass().getName()+"[");
		
		for (Method method : methods){
			if (method.getName().startsWith("get") && method.getParameterTypes().length==0){
				String name=method.getName();
				buffer.append(name);
				buffer.append("=");
				try	{
					buffer.append(method.invoke(bean));
				}catch(Exception exp){
					exp.printStackTrace();
				}
				buffer.append(" ");
			}
		}
		buffer.append("]");
		return buffer.toString();
	}
}
