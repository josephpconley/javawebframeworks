package joejava.pensieve.util;

import java.io.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import joejava.ibatis.bean.Contact;


public class JCalendar{

	public static Color lotr = new Color(98,191,13);
	public static Color gold = new Color(150,169,14);
	public static Color royal = new Color(163,28,187);
	
	static String[] daysNoLeap = {"0101","0102","0103","0104","0105","0106","0107","0108","0109","0110","0111","0112","0113","0114","0115","0116","0117","0118","0119","0120","0121","0122","0123","0124","0125","0126","0127","0128","0129","0130","0131","0201","0202","0203","0204","0205","0206","0207","0208","0209","0210","0211","0212","0213","0214","0215","0216","0217","0218","0219","0220","0221","0222","0223","0224","0225","0226","0227","0228","0301","0302","0303","0304","0305","0306","0307","0308","0309","0310","0311","0312","0313","0314","0315","0316","0317","0318","0319","0320","0321","0322","0323","0324","0325","0326","0327","0328","0329","0330","0331","0401","0402","0403","0404","0405","0406","0407","0408","0409","0410","0411","0412","0413","0414","0415","0416","0417","0418","0419","0420","0421","0422","0423","0424","0425","0426","0427","0428","0429","0430","0501","0502","0503","0504","0505","0506","0507","0508","0509","0510","0511","0512","0513","0514","0515","0516","0517","0518","0519","0520","0521","0522","0523","0524","0525","0526","0527","0528","0529","0530","0531","0601","0602","0603","0604","0605","0606","0607","0608","0609","0610","0611","0612","0613","0614","0615","0616","0617","0618","0619","0620","0621","0622","0623","0624","0625","0626","0627","0628","0629","0630","0701","0702","0703","0704","0705","0706","0707","0708","0709","0710","0711","0712","0713","0714","0715","0716","0717","0718","0719","0720","0721","0722","0723","0724","0725","0726","0727","0728","0729","0730","0731","0801","0802","0803","0804","0805","0806","0807","0808","0809","0810","0811","0812","0813","0814","0815","0816","0817","0818","0819","0820","0821","0822","0823","0824","0825","0826","0827","0828","0829","0830","0831","0901","0902","0903","0904","0905","0906","0907","0908","0909","0910","0911","0912","0913","0914","0915","0916","0917","0918","0919","0920","0921","0922","0923","0924","0925","0926","0927","0928","0929","0930","1001","1002","1003","1004","1005","1006","1007","1008","1009","1010","1011","1012","1013","1014","1015","1016","1017","1018","1019","1020","1021","1022","1023","1024","1025","1026","1027","1028","1029","1030","1031","1101","1102","1103","1104","1105","1106","1107","1108","1109","1110","1111","1112","1113","1114","1115","1116","1117","1118","1119","1120","1121","1122","1123","1124","1125","1126","1127","1128","1129","1130","1201","1202","1203","1204","1205","1206","1207","1208","1209","1210","1211","1212","1213","1214","1215","1216","1217","1218","1219","1220","1221","1222","1223","1224","1225","1226","1227","1228","1229","1230","1231"};
	static String[] daysWithLeap = {"0101","0102","0103","0104","0105","0106","0107","0108","0109","0110","0111","0112","0113","0114","0115","0116","0117","0118","0119","0120","0121","0122","0123","0124","0125","0126","0127","0128","0129","0130","0131","0201","0202","0203","0204","0205","0206","0207","0208","0209","0210","0211","0212","0213","0214","0215","0216","0217","0218","0219","0220","0221","0222","0223","0224","0225","0226","0227","0228","0229","0301","0302","0303","0304","0305","0306","0307","0308","0309","0310","0311","0312","0313","0314","0315","0316","0317","0318","0319","0320","0321","0322","0323","0324","0325","0326","0327","0328","0329","0330","0331","0401","0402","0403","0404","0405","0406","0407","0408","0409","0410","0411","0412","0413","0414","0415","0416","0417","0418","0419","0420","0421","0422","0423","0424","0425","0426","0427","0428","0429","0430","0501","0502","0503","0504","0505","0506","0507","0508","0509","0510","0511","0512","0513","0514","0515","0516","0517","0518","0519","0520","0521","0522","0523","0524","0525","0526","0527","0528","0529","0530","0531","0601","0602","0603","0604","0605","0606","0607","0608","0609","0610","0611","0612","0613","0614","0615","0616","0617","0618","0619","0620","0621","0622","0623","0624","0625","0626","0627","0628","0629","0630","0701","0702","0703","0704","0705","0706","0707","0708","0709","0710","0711","0712","0713","0714","0715","0716","0717","0718","0719","0720","0721","0722","0723","0724","0725","0726","0727","0728","0729","0730","0731","0801","0802","0803","0804","0805","0806","0807","0808","0809","0810","0811","0812","0813","0814","0815","0816","0817","0818","0819","0820","0821","0822","0823","0824","0825","0826","0827","0828","0829","0830","0831","0901","0902","0903","0904","0905","0906","0907","0908","0909","0910","0911","0912","0913","0914","0915","0916","0917","0918","0919","0920","0921","0922","0923","0924","0925","0926","0927","0928","0929","0930","1001","1002","1003","1004","1005","1006","1007","1008","1009","1010","1011","1012","1013","1014","1015","1016","1017","1018","1019","1020","1021","1022","1023","1024","1025","1026","1027","1028","1029","1030","1031","1101","1102","1103","1104","1105","1106","1107","1108","1109","1110","1111","1112","1113","1114","1115","1116","1117","1118","1119","1120","1121","1122","1123","1124","1125","1126","1127","1128","1129","1130","1201","1202","1203","1204","1205","1206","1207","1208","1209","1210","1211","1212","1213","1214","1215","1216","1217","1218","1219","1220","1221","1222","1223","1224","1225","1226","1227","1228","1229","1230","1231"};
	
	public static String[] days = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	public static String[] months={"January","February","March","April","May","June","July","August","September","October","November","December"};
	
	public Calendar today = Calendar.getInstance();
	
	public static String getDayOfWeek(int n){ //1=Sunday, 2=Monday, etc.
		return days[n-1];
	}
	
	public static String getDayOfWeek(String date){
		String wkDay;
		
		Calendar thisDay = Calendar.getInstance();
		thisDay.set(Calendar.DAY_OF_YEAR,getDayOfYear(date));
		wkDay=getDayOfWeek(thisDay.get(Calendar.DAY_OF_WEEK));	
		
		return wkDay;
	}
	
	public static String getDayOfWeek(Date date){
		Calendar thisDay = Calendar.getInstance();
		thisDay.setTime(date);
		return getDayOfWeek(thisDay.get(Calendar.DAY_OF_WEEK));	
	}

//reverse of previous process
	public int getNWeek(String wkday){
		int n=99;
		
		for(int i=0;i<days.length;i++){
			if(days[i].equals(wkday))
				n=i;
		}
		
		return n;	
	}
//Returns month given an integer from 1-12
	public static String getMonth(int n)
	{
		return months[n];
	}
	
	public static String getMonth(Date date){
		Calendar thisDay = Calendar.getInstance();
		thisDay.setTime(date);
		return getMonth(thisDay.get(Calendar.MONTH));	
	}

//reverse of previous process
	public static int getNMonth(String month){
		int n=99;
		
		for(int i=0;i<months.length;i++){
			if(months[i].equals(month))
				n=i+1;
		}
		
		return n;	
	}

	public static boolean isLeap(int year){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR,year);
		
		if(c.getActualMaximum(Calendar.DAY_OF_YEAR)==365)
		return false;
		else
		return true;
	}	

//Gets the day of the year (from 1-366) depending if its a leap year
	public static int getDayOfYear(String date){
		int num=-1;
		boolean leap=isLeap(Integer.valueOf(date.substring(4,8)));
		String mmdd=date.substring(0,4);
		
		if(leap){
			for(int i=0;i<daysWithLeap.length;i++){
				if(mmdd.equals(daysWithLeap[i]))
					num = i+1;
			}
		}
		else{
			for(int i=0;i<daysNoLeap.length;i++){
				if(mmdd.equals(daysNoLeap[i]))
					num = i+1;
			}
		}
		
		if(num==-1){	
			throw new IllegalArgumentException();
		}
		else		
			return num;
	}


//Gets the day of the year (from 1-366) depending if its a leap year
	public static int getDayOfYear(Date date){
		SimpleDateFormat dFormat=new SimpleDateFormat("MMddyyyy");
		String d = dFormat.format(date);
		return getDayOfYear(d);
	}
	
	public static int getDaysInMonth(int month){
		if(month==9 || month==4 || month==6 || month==11)
			return 30;
		else if(month==2)
			return 29;
		else
			return 31;
	}
	
	public static Date toDate(String date){
		Date d=null;
		DateFormat dFormat = new SimpleDateFormat("MM/dd/yyyy");
		if(!date.equals("")){
			try {
				d = dFormat.parse(date);
			} catch (ParseException e) {e.printStackTrace();}	
		}
		return d;
	}
	
	//Convert a Date to a String of form MM/DD/YYYY
	public static String dateToString(java.util.Date date){
		DateFormat dFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		if(date != null)
			return dFormat.format(date);
		else
			return null;
	}
	
	//Convert a Date to a String of form MM/DD/YYYY
	public static String getDate(java.util.Date date){
		DateFormat dFormat = new SimpleDateFormat("MM/dd");
		
		if(date != null)
			return dFormat.format(date);
		else
			return null;
	}
	
	public static String parse(String str){
		if(str.equals(""))
			return null;
		else
			return str;
	}
	/*
	public static void sort(java.util.List<Contact> list){
		for(int i=0;i<list.size();i++){
			for(int j=i;j>0;j--){
				String[] names = new String[2];
				for(int k=0;k<2;k++){
					if(list.get(j+k-1).getLastName()==null)
						names[k]=list.get(j+k-1).getFirstName();
					else
						names[k]=list.get(j+k-1).getLastName();
				}
				
				if(names[0].compareTo(names[1])>0)
					Collections.swap(list,j-1,j);
			}
				
		}
	}
	*/
	
	public static double ID(Date date){
		Calendar now=Calendar.getInstance();
		Calendar then=Calendar.getInstance();
		then.setTime(date);
		return now.YEAR+(.001*then.DAY_OF_YEAR);
	}
	
	public static void clearTime(Calendar cal){
		cal.clear(Calendar.HOUR_OF_DAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
	}
	
//Add a checkDate() to verify bday entries
	
//compares two dates, determines if the first is less than or equal to the second (MMDDYYYY)	
			//if same date, returns false!!!
	public boolean isOld(String date1, String date2){
		int year1 = Integer.valueOf(date1.substring(4,8));
		int year2 = Integer.valueOf(date2.substring(4,8));
	
		int index1 = year1*10000;
		int index2 = year2*10000;
	
		index1=index1+getDayOfYear(date1);
		index2=index2+getDayOfYear(date2);
		
		if(index1<index2)
			return true;
		else
			return false;
	}

//Deletes old one-time appointments (all others must be deleted manually)		
	public void update(String date){
		File dir = new File("sched/once");
		String[] files = dir.list();
		
		System.out.println(date);
		for(int i=0;i<files.length;i++){
			if(isOld(files[i],date)){
				File old = new File("sched/once/"+files[i]);
				old.delete();
			}
		
		}
	}	

//Determines if a date is legal	MMDDYYYY

	public boolean isDate(String date){
		boolean exists=false;
		
		int year = Integer.valueOf(date.substring(4,8));
		String mmdd = date.substring(0,4);
		if(isLeap(year)){
			for(int i=0;i<daysWithLeap.length;i++){
				if(mmdd.equals(daysWithLeap[i]))
					exists=true;
			}
		}
		else{
			for(int i=0;i<daysNoLeap.length;i++){
				if(mmdd.equals(daysNoLeap[i]))
					exists=true;
			}
		}
			
		return exists;
	}

//Compares two appointment objects (of the same type!!!) and determines if the date is different

//Simple process that sets up JComboes for Schedule program

	public JComboBox schedBox (String time){
		JComboBox box = new JComboBox();
		box.addItem("");
		
		if(time.equals("hr")){
			for(int x=1;x<13;x++){
				box.addItem(intToStr(x));
			}
		}
		if(time.equals("min")){
			for(int y=0;y<61;y++){
				box.addItem(intToStr(y));
			}
		}
		if(time.equals("XX")){
				box.addItem("AM");
				box.addItem("PM");		
		}
		if(time.equals("day")){
			for(int k=1;k<32;k++){
				box.addItem(intToStr(k));
			}
		}
		if(time.equals("week")){
			for(int i=1;i<8;i++)
			box.addItem(getDayOfWeek(i));
		}	

		if(time.equals("month")){
			for(int j=0;j<12;j++)
			box.addItem(getMonth(j));
		}
		
		if(time.equals("year")){
			for(int m=today.get(Calendar.YEAR)+3;m>1890;m--)
			box.addItem(""+m);
		}
		
	return box;
	}


//retrieve holidays with weird rules (nth week of mth month)

	//date is in terms of MMwDD where MM is month, w is day of week, and DD is day
	
	public String holiday(String date){
		int len=date.length();
		String record;
		String holiday = "null";
		int count=0;
		int dMin=0,dMax=0,DD=0;
	
		DD=Integer.valueOf(date.substring(3,5));
	 	Scanner s = null;
	 
		try{
			java.net.URL url = this.getClass().getResource("sched/holidays.txt");
			InputStream in = url.openStream();	
			s = new Scanner(in);
			s.useDelimiter("\r\n");
			
			while(s.hasNext()){
				record=s.next();
				
				//range of possible dates
				dMin=Integer.valueOf(record.substring(3,5));
				dMax=Integer.valueOf(record.substring(6,8));
		
				if(record.substring(0,3).equals(date.substring(0,3)) && DD>dMin && DD<dMax)
					holiday=record.substring(8,record.length());		//better way to return proper strings of names		
						
			}
		
		      // dispose all the resources after using them.
   	 	if(s == null)
			s.close(); 	
		}
		
		catch (IOException e){
			System.out.println(e);
		}
	
		return holiday;
	}


//Formats a string to be used in BDayApp
	
	public String dateStr(int month, int date) {
		String mo = intToStr(month);
		String dt = intToStr(date);
		return mo+dt;
	}
	
/*
	public static void sortAppts(ArrayList<Appt> a){
	
		for(int i=0;i<a.size();i++){
			for(int j=i;j>0 && milTime(a.get(j-1).startTime) > milTime(a.get(j).startTime);j--){
				Appt temp = a.get(j-1);
				a.set(j-1,a.get(j));
				a.set(j,temp);
			}
		}
	}
	*/
//Converts standard time XX:XX(AM/PM) into military time (for use in ordering appts from earliest to latest)

	//BE SURE that time is entered in this format (i.e. - 03:00PM, include leading 0)
	
	public static int milTime (String stdTime)
	{
		int mil = 9999;
		String temp;
	
		//Catches any entries that are not constrained by time(all day)
		try{	
			temp = stdTime.substring(0,2)+stdTime.substring(3,5);	
			mil = Integer.parseInt(temp);
			
			//noon (and after)
			if(stdTime.substring(5,7).equals("PM") && stdTime.substring(0,2).equals("12")==false)
			mil=mil+1200;
		
			//midnight
			if(stdTime.substring(5,7).equals("AM") && stdTime.substring(0,2).equals("12"))
			mil=mil-1200;
		
		}
		catch (Exception e){

		}
		
	return mil;
	}	
	
	public static String ordinal(int value){
		value = Math.abs( value );
		final int lastDigit = value % 10;
		final int last2Digits = value % 100;
		switch ( lastDigit ){
		  case 1 :
		     return  last2Digits == 11 ? "th" : "st";
		
		  case 2:
		     return  last2Digits == 12 ? "th" : "nd";
		
		  case 3:
		     return  last2Digits == 13 ? "th" : "rd";
		
		  default:
		     return "th";
		  }
	   }

//Creates a string from an integer, adding 0 to any single-digit integers	
	public String intToStr(int n){
		if(n<10 && n>-10)
			return "0"+Integer.toString(n);
		else
			return Integer.toString(n);
	}
	
/*	
	public String isBday(String d){
		Scanner s = null;
		String word;
		String file="mellon/Contacts.txt";
		String date = d.substring(0,2)+"/"+d.substring(2,4);
		String name=null;
				
		try{
			java.net.URL url = this.getClass().getResource(file);
			InputStream in = url.openStream();	
			s = new Scanner(in);
			s.useDelimiter("\r\n");
						
  			while(s.hasNext()){
				word=s.next();
				Contact person = new Contact(word);
					if(person.bday.substring(0,5).equals(date))
					name=person.name;
			}
				if(s == null)
				s.close();
		}
	
		catch(IOException e){
		}
		
		return name;
	}
	
*/
	
}