package joejava.pensieve.util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import joejava.ibatis.bean.EmailProperties;
import joejava.ibatis.DAO;
import joejava.util.EmailUtility;

public class SendReminder {
	public static void main(String[] args) throws Exception{
		EmailProperties eprops = (EmailProperties)
						DAO.client.queryForObject("getEmailProperties","Gmail");
		
		Properties props = System.getProperties();
	    props.put("mail.smtp.auth", "true");
	    
	    Session session = Session.getInstance(props, null);
	    Transport t = null;
	    
	    List<String> reminders = (List<String>)DAO.client.queryForList("getReminders",null);
	    EmailUtility.sendSimpleEmail("6104163219@txt.att.net", "pensieve@jolty.com", 
    											"Reminder", reminders.toString());
	}
}
