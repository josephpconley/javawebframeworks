/** 
 *  Copyright BDP International, Inc.
 */
package joejava.pensieve.common;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

import joejava.ibatis.DAO;

/**
 * Class: ErrorHandler
 *
 * Description:  Any excption that is thrown and not caught in the application
 * will be given to this class.  It sends the user to the error page and logs the error in 
 * the database.
 * 
 * Created on Aug 29, 2008
 *
 * @author Adam Stokar
 * @version 
 */
public class ErrorHandler extends ExceptionHandler {
	
	public ActionForward execute(Exception ex, ExceptionConfig eConfig,
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		
		try {
			String requestURI = request.getRequestURI();
						
			StringWriter sw = new StringWriter();
			ex.printStackTrace(new PrintWriter(sw));
			String reason = sw.toString();
			System.out.println(reason);
		
			if(reason.length() > 2000) {
				reason = reason.substring(0,1999);
			}
			
			request.setAttribute("globalException", "Unable to handle " + requestURI + " due to the following:<br /><br /><b>" + reason + "</b>");
		} catch(Exception e) {
			e.printStackTrace(System.out);
		}
		
		return super.execute(ex, eConfig, mapping, form, request, response);
	}
}
