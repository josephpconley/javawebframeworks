/** 
 *  Copyright BDP International, Inc.
 */
package joejava.pensieve.tags;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import joejava.ibatis.bean.Note;
import joejava.ibatis.DAO;

public class OptionsTag extends TagSupport {

	private String type;
	private int noteID;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}		
	public int getNoteID() {
		return noteID;
	}
	public void setNoteID(int noteID) {
		this.noteID = noteID;
	}
	public int doStartTag() throws JspException {
				
		HttpSession session = pageContext.getSession();
		
		try {
			if("DAYS".equals(type)) {
				List<String> days = new ArrayList<String>();
				for(int i=1; i<=31; i++) {
					days.add(i+"");
				}
				session.setAttribute("days", days);
			}
			if("YEARS".equals(type)) {
				List<String> years = new ArrayList<String>();
				for(int i=2010; i>1899; i--) {
					years.add(i+"");
				}
				session.setAttribute("years", years);
			}
			if("HOURS".equals(type)) {
				List<String> hours = new ArrayList<String>();
				for(int i=1; i<13; i++) {
					hours.add(i+"");
				}
				session.setAttribute("hours", hours);
			}
			if("MINUTES".equals(type)) {
				List<String> minutes = new ArrayList<String>();
				for(int i=0; i<61; i++) {
					if(i<10)
						minutes.add("0"+i);
					else
						minutes.add(""+i);
				}
				session.setAttribute("minutes", minutes);
			}
			if("dayLabel".equals(type)){
				Calendar today = Calendar.getInstance();
				DateFormat dFormat = new SimpleDateFormat("EEEE, MMMM d");
				
				Note note = (Note) DAO.client.queryForObject("getNote",noteID);
				today.setTime(note.getStartDate());
				String dayLabel = dFormat.format(today.getTime());
				System.out.println(dayLabel);
				session.setAttribute("dayLabel", dayLabel);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
}
