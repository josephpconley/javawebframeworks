<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/pensieve" prefix="pensieve" %>

<bean:parameter name="id" id="idRef"/>

<jsp:include page="header.jsp" />

	<div class="editContact">
	<table>
		<tr ><td>
			<table>
			<html:form action="saveContact.do" method="POST">
			<pensieve:options type="DAYS"/>
			<pensieve:options type="YEARS"/>
				<tr><td>Name: <html:text property="id" value="${idRef}" /></td></tr>
				<tr>
					<div id="bdayCal" class="calendarDiv"></div>						
					<td colspan="4">Bday: 
					<html:select property="birthDay" styleId="birthDay">
					    <html:option value="">Day</html:option>
					    <html:options name="days"/> 
					</html:select>
					<html:select property="birthMonth" styleId="birthMonth">
					    <html:option value="">Month</html:option>
					    <html:option value="JAN">January</html:option>
					    <html:option value="FEB">February</html:option>
					    <html:option value="MAR">March</html:option>
					    <html:option value="APR">April</html:option>
					    <html:option value="MAY">May</html:option>
					    <html:option value="JUN">June</html:option>
					    <html:option value="JUL">July</html:option>
					    <html:option value="AUG">August</html:option>
					    <html:option value="SEP">September</html:option>
					    <html:option value="OCT">October</html:option>
					    <html:option value="NOV">November</html:option>
					    <html:option value="DEC">December</html:option>
					</html:select>
					<html:select property="birthYear" styleId="birthYear">
						<html:option value="">Year</html:option>
						<html:options name="years"/>
					</html:select>
					<a style="cursor:pointer"><img class="button" src="/pensieve/images/calendar/calendar.gif" id="bdayButton"/></a>
				</td></tr>
				<tr><td>Address 1: <html:text property="address1" /></td>
					<td>Address 2: <html:text property="address2" /></td>
					<td>Address 3: <html:text property="address3" /></td>
					<td>Address 4: <html:text property="address4" /></td></tr>
				<tr><td>Phone 1: <html:text property="phone1" /></td>
					<td>Phone 2: <html:text property="phone2" /></td>
					<td>Phone 3: <html:text property="phone3" /></td>
					<td>Phone 4: <html:text property="phone4" /></td></tr>
				<tr><td>Email 1: <html:text property="email1" /></td>
					<td>Email 2: <html:text property="email2" /></td>
					<td>Email 3: <html:text property="email3" /></td>
					<td>Email 4: <html:text property="email4" /></td></tr>
				<tr><td>Cell: <html:text property="cellService" /></td>
				<td><html:submit styleClass="button" value="Save"/>
				<input type="button" class="button" onclick="javascript:deleteContact('${idRef}')" value="Delete"/></td></tr>
			</html:form>
			</table>
		</td></tr>
	</table>
	</div>
	<script type="text/javascript">
		initContacts();
	</script>
<jsp:include page="footer.jsp" />