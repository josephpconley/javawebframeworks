<%@ taglib uri="/struts-bean" prefix="bean" %>

<jsp:include page="header.jsp" />

	<bean:parameter name="type" id="type" />

	<div class="editNote">
		Your transaction was completed successfully.
		<table>
			<tr><td>
				<a href="javascript:getLink('${type}')">Add</a></td>
				<td><a href="javascript:refreshParent()">Close</a></td></tr>		
		</table>
	</div>
	
<jsp:include page="footer.jsp" />