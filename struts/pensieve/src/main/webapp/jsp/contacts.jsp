<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/pensieve" prefix="pensieve" %>

<jsp:include page="header.jsp" />
<jsp:include page="menu.jsp" />
	<div class="panel">
		<input type="button" class="button" value="Add Contact" onclick="javascript:editContact('')"/>
	</div>
	<div class="left">	
		<table class="note">
			<logic:present name="contacts">
			<logic:iterate name="contacts" id="contact">
			<tr >
				<logic:equal name="contact" property="picFlag" value="true">
				<td><a href="javascript:editContact('${contact.id}')" style="cursor:pointer"><img src="/pensieve/images/contacts/${contact.id}.jpg"/></a></td>
				</logic:equal>
				<logic:equal name="contact" property="picFlag" value="false">
				<td><a href="javascript:editContact('${contact.id}')" style="cursor:pointer"><img src="/pensieve/images/contacts/no pic.jpg"/></a></td>
				</logic:equal>
				<td>
					<table id="${contact.id}" class="contact">
						<tr><td>Name: ${contact.id}</td></tr>
						<logic:present name="contact" property="bday">
							<tr><td>Bday: <bean:write name="contact" property="bday" format="MMM-dd-yyyy"/></td></tr>
						</logic:present>
						<logic:present name="contact" property="address1">
							<tr><td>Address 1: ${contact.address1}</td></tr>
						</logic:present>
						<logic:present name="contact" property="address2">
							<tr><td>Address 2: ${contact.address2}</td></tr>
						</logic:present>
						<logic:present name="contact" property="address3">
							<tr><td>Address 3: ${contact.address3}</td></tr>
						</logic:present>
						<logic:present name="contact" property="address4">
							<tr><td>Address 4: ${contact.address4}</td></tr>
						</logic:present>
				
						<logic:present name="contact" property="email1">
							<tr><td>Email 1:<a style="cursor:pointer;" href="javascript:sendEmail('${contact.email1}')">${contact.email1}
								</a></td></tr>
						</logic:present>
						<logic:present name="contact" property="email2">
							<tr><td>Email 2: <a style="cursor:pointer;" href="javascript:sendEmail('${contact.email2}')">${contact.email2}
								</a></td></tr>
						</logic:present>
						<logic:present name="contact" property="email3">
							<tr><td>Email 3: <a style="cursor:pointer;" href="javascript:sendEmail('${contact.email3}')">${contact.email3}
								</a></td></tr>
						</logic:present>
						<logic:present name="contact" property="email4">
							<tr><td>Email 4: <a style="cursor:pointer;" href="javascript:sendEmail('${contact.email4}')">${contact.email4}
								</a></td></tr>
						</logic:present>
					
						<logic:present name="contact" property="phone1">
							<tr><td>Phone 1: <a style="cursor:pointer;" href="javascript:sendEmail(getEmail('${contact.phone1}','${contact.cellService}'))">
							${contact.phone1}</a></td></tr>
						</logic:present>
						<logic:present name="contact" property="phone2">
							<tr><td>Phone 2: ${contact.phone2}</td></tr>
						</logic:present>
						<logic:present name="contact" property="phone3">
							<tr><td>Phone 3: ${contact.phone3}</td></tr>
						</logic:present>
						<logic:present name="contact" property="phone4">
							<tr><td>Phone 4: ${contact.phone4}</td></tr>
						</logic:present>
						
						<logic:present name="contact" property="cellService">
							<tr><td>Cell: ${contact.cellService}</td></tr>
						</logic:present>
					</table>
				</td></tr>
		</logic:iterate>
		</logic:present>
		</table>
	</div>
<jsp:include page="footer.jsp" />