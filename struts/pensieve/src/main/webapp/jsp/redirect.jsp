<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>

<jsp:include page="header.jsp"/>

<input type="hidden" id="page" value="${url}${params}"/>

<br />
<p class="labelhd">Please wait while page loads...</p>

<script>
	EVENT.onDOMReady(goToPage);
</script>

<jsp:include page="footer.jsp"/>