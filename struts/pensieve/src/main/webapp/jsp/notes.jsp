<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-nested" prefix="nested"%>
<%@ taglib uri="/pensieve" prefix="pensieve" %>

<jsp:include page="header.jsp" />
<jsp:include page="menu.jsp" />

		<div class="panel">
			<input type="button" class="button" onclick="javascript:editNote('0')" value="Add Note" />
		</div>			
		<div class="left">
			<table class="note">
				<logic:present name="days">
				<logic:iterate name="days" id="day">			
				<tr><td>${day.label}</td></tr>
				<logic:iterate collection="${day.notes}" id="note">			
					<tr><td>
						<logic:notMatch name="note" property="content" value="Birthday">
						<a style="margin-left: 50px;" href="javascript:editNote('${note.id}')">				
							<logic:notEmpty name="note" property="startTime">
								<bean:write name="note" property="startTime" format="h:mm a"/>
							</logic:notEmpty>
							<logic:notEmpty name="note" property="endTime">
								- <bean:write name="note" property="endTime" format="h:mm a"/>
							</logic:notEmpty>
							&nbsp;&nbsp;&nbsp;${note.content}</a>
						</logic:notMatch>
						<logic:match name="note" property="content" value="Birthday">
						<li style="margin-left: 100px; list-style-type: none;">
							<logic:notEmpty name="note" property="startTime">
								<bean:write name="note" property="startTime" format="h:mm a"/>
							</logic:notEmpty>
							&nbsp;&nbsp;&nbsp;${note.content}
						</li>
						</logic:match>
					</td></tr>
					</logic:iterate>
				</logic:iterate>
				</logic:present>
			</table>
		</div>
		<div class="right">			
			<table class="note">
				<logic:present name="categories">
				<logic:iterate name="categories" id="category">			
				<tr><td>${category.label}</td></tr>
					<logic:iterate collection="${category.notes}" id="note">	
						<tr><td>
							<a style="margin-left: 50px;" href="javascript:editNote('${note.id}')">${note.content}</a>
						</td></tr>
					</logic:iterate>
				</logic:iterate>
				</logic:present>
			</table>
		</div>
				
<jsp:include page="footer.jsp" />