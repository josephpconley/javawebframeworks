<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="-1" /> 
		<meta name="author" content="Jolty Inc."/>
		<meta name="copyrght" content="Jolty Inc."/>
		
		<title>Joe's Pensieve</title>	
		
	    <style type="text/css" media="screen">
			@import url("/pensieve/css/main.css");
			@import url("/pensieve/css/calendar.css");
	    </style>
	 
	<!-- YUI Scripts -->
	<script type="text/javascript" src="/pensieve/js/yui/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="/pensieve/js/yui/utilities.js"></script>
	<script type="text/javascript" src="/pensieve/js/yui/connection-min.js"></script>
	<script type="text/javascript" src="/pensieve/js/yui/calendar-min.js"></script>
	<script type="text/javascript" src="/pensieve/js/yui/json-min.js"></script>
	<script type="text/javascript" src="/pensieve/js/yui/animation-min.js"></script>

	<!-- TreeView source file -->  
	<script src = "http://yui.yahooapis.com/2.7.0/build/treeview/treeview-min.js" ></script>

	<!-- MooTools Scripts -->
	<script type="text/javascript" src="/pensieve/js/mootools/mootools.js"></script>

	<!-- JavaScripts -->    
	<script type="text/javascript" src="/pensieve/js/main.js"></script>
	<script type="text/javascript" src="/pensieve/js/contacts.js"></script>
	<script type="text/javascript" src="/pensieve/js/email.js"></script>
	<script type="text/javascript" src="/pensieve/js/calendar.js"></script>
	<script type="text/javascript" src="/pensieve/js/notes.js"></script>

	    <!-- DWR Scripts - Call back-end Java processes remotely 
	<script type='text/javascript' src='/ISF/dwr/interface/ContactFilter.js'></script>
	<script type='text/javascript' src='/ISF/dwr/engine.js'></script>
-->

		<!--  Call function directly from taglib (include custom tag too)  -->
	</head> 
	
	<body>