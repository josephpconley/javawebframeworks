<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/pensieve" prefix="pensieve" %>

<bean:parameter name="id" id="idRefNo"/>

<jsp:include page="header.jsp" />
	<div class="editNote">
		<html:form action="saveNote.do" method="POST">
			<pensieve:options type="DAYS"/>
			<pensieve:options type="YEARS"/>
			<pensieve:options type="HOURS"/>
			<pensieve:options type="MINUTES"/>
								
			<html:hidden property="id" value="${idRefNo}"/>
			
			<table>
				<tr>
					<div id="startDateCal" class="calendarDiv"></div>
					<td colspan="4">Start Date: 
					<html:select property="startMonth" styleId="startMonth">
					    <html:option value="">Month</html:option>
					    <html:option value="JAN">January</html:option>
					    <html:option value="FEB">February</html:option>
					    <html:option value="MAR">March</html:option>
					    <html:option value="APR">April</html:option>
					    <html:option value="MAY">May</html:option>
					    <html:option value="JUN">June</html:option>
					    <html:option value="JUL">July</html:option>
					    <html:option value="AUG">August</html:option>
					    <html:option value="SEP">September</html:option>
					    <html:option value="OCT">October</html:option>
					    <html:option value="NOV">November</html:option>
					    <html:option value="DEC">December</html:option>
					</html:select>				
					<html:select property="startDay" styleId="startDay">
					    <html:option value="">Day</html:option>
					    <html:options name="days"/> 
					</html:select>
					<html:select property="startYear" styleId="startYear">
						<html:option value="">Year</html:option>
						<html:options name="years"/>
					</html:select>
					<a style="cursor:pointer"><img class="button" src="/pensieve/images/calendar/calendar.gif" id="startDateButton"/></a>
				</td></tr>
				<tr>
					<div id="endDateCal" class="calendarDiv"></div>
					<td colspan="4">End Date: 
					<html:select property="endMonth" styleId="endMonth">
					    <html:option value="">Month</html:option>
					    <html:option value="JAN">January</html:option>
					    <html:option value="FEB">February</html:option>
					    <html:option value="MAR">March</html:option>
					    <html:option value="APR">April</html:option>
					    <html:option value="MAY">May</html:option>
					    <html:option value="JUN">June</html:option>
					    <html:option value="JUL">July</html:option>
					    <html:option value="AUG">August</html:option>
					    <html:option value="SEP">September</html:option>
					    <html:option value="OCT">October</html:option>
					    <html:option value="NOV">November</html:option>
					    <html:option value="DEC">December</html:option>
					</html:select>
					<html:select property="endDay" styleId="endDay">
					    <html:option value="">Day</html:option>
					    <html:options name="days"/> 
					</html:select>
					<html:select property="endYear" styleId="endYear">
						<html:option value="">Year</html:option>
						<html:options name="years"/>
					</html:select>
					<a style="cursor:pointer"><img class="button" src="/pensieve/images/calendar/calendar.gif" id="endDateButton"/></a>
				</td></tr>			
				<tr><td colspan="4">Start Time:
					<html:select property="startHours">
					    <html:option value="">Hr</html:option>
					    <html:options name="hours"/>
					</html:select>
					<html:select property="startMinutes">
					    <html:option value="">Min</html:option>
					    <html:options name="minutes"/>
					</html:select>
					<html:select property="startMeridiem">
						<html:option value="">-</html:option>
						<html:option value="AM">AM</html:option>
						<html:option value="PM">PM</html:option>
					</html:select>
				</td></tr>
				<tr><td colspan="4">End Time:
					<html:select property="endHours">
					    <html:option value="">Hr</html:option>
					    <html:options name="hours"/>
					</html:select>
					<html:select property="endMinutes">
					    <html:option value="">Min</html:option>
					    <html:options name="minutes"/>
					</html:select>
					<html:select property="endMeridiem">
						<html:option value="">-</html:option>
						<html:option value="AM">AM</html:option>
						<html:option value="PM">PM</html:option>
					</html:select>
				</td></tr>			
				<tr><td>
					Category: <html:text property="category" styleId="category" onkeyup="lookupCategory(event)" onfocus="lookupCategory(event)" onblur="delayHideSuggest('category-suggest')"/>
					<div class="suggest hide" id="category-suggest" onfocus="canHideSuggest = false;" onblur="canHideSuggest = true; delayHideSuggest('category-suggest')"></div>
				</td></tr>
				<tr><td>
					Frequency: <html:text property="frequency" styleId="frequency" onkeyup="lookupFrequency(event)" onfocus="lookupFrequency(event)" onblur="delayHideSuggest('frequency-suggest')"/>
					<div class="suggest hide" id="frequency-suggest" onfocus="canHideSuggest = false;" onblur="canHideSuggest = true; delayHideSuggest('frequency-suggest')"></div>
				</td></tr>
				<tr><td>
					<html:textarea property="content" cols="30" rows="10"/>
				</td></tr>
				<tr><td>
					Reminder?<html:checkbox property="reminder"/></td></tr>
				<tr><td><html:submit styleClass="button" value="Save"/>
					<input type="button" class="button" onclick="javascript:deleteNote(${note.id})" value="Delete"/>
				</td></tr>
			</table>
		</html:form>
	</div>
	<script type="text/javascript">
		initNotes();
	</script>
	
<jsp:include page="footer.jsp" />