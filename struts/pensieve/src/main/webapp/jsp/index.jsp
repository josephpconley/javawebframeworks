<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>

<jsp:include page="header.jsp" />
<jsp:include page="menu.jsp" />

	<html:form action="login.do" method="post">
		<table class="editNote">
			<tr>
				<td>Username</td>
				<td>
					<html:text property="username" style="width: 150px;"/>
					<span class="error"><html:errors property="username"/></span>
					<logic:present name="message">
					<span class="error">${message}</span>
					</logic:present>
				</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>
					<html:password property="password" style="width: 150px;"/>
					<span class="error"><html:errors property="password-"/></span>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<html:submit value="Log In" styleClass="button" />
					<span class="error"><html:errors property="invalid"/></span>
				</td>
			</tr>
		</table>
	</html:form>
	
<jsp:include page="footer.jsp" />