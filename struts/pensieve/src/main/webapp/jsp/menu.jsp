<%@ taglib uri="/pensieve" prefix="pensieve" %>

<div class="header">
	<a href="/pensieve/jsp/index.jsp">Joe's Pensieve</a>
</div>
<div class="tab">
	<a class="menu" href="/pensieve/showContacts.do" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)">Contacts</a>
	<a class="menu" href="/pensieve/showNotes.do" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)">Notes</a>
	<a class="menu" href="javascript:sendEmail()" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)">Email</a>
</div>