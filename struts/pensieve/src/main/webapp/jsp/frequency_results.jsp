<%@ taglib uri="/struts-logic" prefix="logic" %>

<logic:iterate name="frequencies" id="frequency">
	<div class="result" style="cursor:pointer" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)" onclick="loadFrequency('${frequency}')" >
		${frequency}
	</div>	
</logic:iterate>
