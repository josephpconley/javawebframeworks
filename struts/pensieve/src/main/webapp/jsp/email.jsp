<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>

<bean:parameter name="to" id="recipient"/>

<jsp:include page="header.jsp" />
	<div class="editNote">
	<html:form action="sendEmail.do" method="POST">
		<table id="email">
			<logic:notEmpty name="recipient">
				<tr><td colspan="2">To: <html:text property="to" value="${recipient}"/></td></tr>
			</logic:notEmpty>
			<logic:empty name="recipient">
				<tr><td colspan="3">To: <html:text property="to" styleId="to" onkeyup="lookupEmail(event)" onfocus="lookupEmail(event)" onblur="delayHideSuggest('email-suggest')"/>
					<div class="suggest hide" id="email-suggest" onfocus="canHideSuggest = false;" onblur="canHideSuggest = true; delayHideSuggest('email-suggest')"></div>
				</td></tr>
			</logic:empty>
			<tr><td colspan="2">From: <html:text property="from" value="josephpconley@gmail.com"/></td></tr>
			<tr><td colspan="2">Subject: <html:text property="subject"/></td></tr>
			<tr><td colspan="2"><html:textarea property="text" cols="30" rows="10"/></td></tr>
			<tr><td><html:submit value="Send" styleClass="button" onclick="javascript:loading()"/></td></tr>
		</table>
	</html:form>
	</div>
	<div class="loading hide" id="loading">
		Sending...<img id="loading-image" src="/pensieve/images/loading.gif" />
	</div>
<jsp:include page="footer.jsp" />