<%@ taglib uri="/struts-logic" prefix="logic" %>

<logic:iterate name="categories" id="category">
	<div class="result" style="cursor:pointer" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)" onclick="loadCategory('${category}')" >
		${category}
	</div>	
</logic:iterate>
