<jsp:include page="header.jsp" />
			
	<div style="padding: 10px;">
		<p style="color: #336699; font-family: arial; font-size: 18pt; margin-top: 10px;">The page you requested is unavailable.</p>
		<br />
		<span class="error">${globalException}</span>
		<br /><br />
	</div>
			
<jsp:include page="footer.jsp" />