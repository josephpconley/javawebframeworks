<%@ taglib uri="/struts-logic" prefix="logic" %>

<logic:iterate name="emails" id="email">
	<div class="result" style="cursor:pointer" onmouseover="highlightEl(this)" onmouseout="unhighlightEl(this)" onclick="loadEmail('${email.address}')" >
		${email.ID} - ${email.address}
	</div>	
</logic:iterate>