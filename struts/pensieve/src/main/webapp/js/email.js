function sendEmail(to){
	if(to){
		window.open("/pensieve/jsp/email.jsp?to="+to,"email","height="+popupHeight+
						",width="+popupWidth+",top="+top+",left="+left);
	}else{
		window.open("/pensieve/jsp/email.jsp?to=","email","height="+popupHeight+
				",width="+popupWidth+",top="+top+",left="+left);	
	}
}

function showResults(o) {
	var div = DOM.get(o.argument[0]);
	if(o) {
		div.innerHTML = o.responseText;
	}
}

function lookupEmail(e){
	var keyCode = e.keyCode;
	if(!keyCode || validKeyCode(keyCode)) {
		var div = DOM.get('email-suggest');
		var value = DOM.get('to').value.trim();
		
		if(value != "" && isAlphaNumeric(value)) {
			var args = ['email-suggest'];
			var emailHandler = {
				success: showResults, 
				failure: handleError, 
				argument: args
			};
	
			var request = YAHOO.util.Connect.asyncRequest('POST',
									'/pensieve/ajax.do?type=EMAIL&value='+value,emailHandler,null); 	
			DOM.removeClass(div,'hide');
		}
	}
}

function loadEmail(email){
	DOM.get('to').value=email;
	
	var div = DOM.get('email-suggest');
	DOM.addClass(div,'hide');
}

function loading(){
	var div = DOM.get('loading');
	DOM.removeClass(div,'hide');
}