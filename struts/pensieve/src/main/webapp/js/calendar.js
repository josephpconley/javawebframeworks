function getMonthString(n) {
	switch(n) {
		case 1: return 'JAN';
		case 2: return 'FEB';
		case 3: return 'MAR';
		case 4: return 'APR';
		case 5: return 'MAY';
		case 6: return 'JUN';
		case 7: return 'JUL';
		case 8: return 'AUG';
		case 9: return 'SEP';
		case 10: return 'OCT';
		case 11: return 'NOV';
		case 12: return 'DEC';
		default: return null;
	}
}

function getMonthNumber(str) {
	if(str) {
		switch(str.toUpperCase()) {
			case 'JAN': return 1;
			case 'FEB': return 2;
			case 'MAR': return 3;
			case 'APR': return 4;
			case 'MAY': return 5;
			case 'JUN': return 6;
			case 'JUL': return 7;
			case 'AUG': return 8;
			case 'SEP': return 9;
			case 'OCT': return 10;
			case 'NOV': return 11;
			case 'DEC': return 12;
			default: return null;
		}
	} else {
		return null;
	}
}


function cal(){

	function loadDate() {
	    
	    var selDay = DOM.get(selDayId); 
	    var selMonth = DOM.get(selMonthId); 
	    var selYear = DOM.get(selYearId); 
	    
		var input = "";
		if(date != "") {
			if(selMonth.value && getMonthNumber(selMonth.value) != null) {
				input += getMonthNumber(selMonth.value) + "/";
			} else {
				return;
			}
			if(selDay.value) {
				input += selDay.value + "/";
			} else {
				return;
			}
			if(selYear.value) {
				input += selYear.value;
			} else {
				return;
			}
			calendar.select(input);
			calendar.cfg.setProperty("pagedate",getMonthNumber(data[1])+"/"+data[2]);
			calendar.render();
		}
	}
}

function initCalendar(div,dayId,monthId,yearId,btn) {
	var calendar = new YAHOO.widget.Calendar(div,{ 
		title:"Choose a date:", 
		close:true,
		mindate:"01/01/2009"
	});
	
	function handleSelect(type,args,obj) { 
	    var dates = args[0]; 
	    var date = dates[0]; 
	    var year = date[0], month = date[1], day = date[2]; 
	 
	    var selDay = DOM.get(dayId); 
	    var selMonth = DOM.get(monthId); 
	    var selYear = DOM.get(yearId); 
	    
	    selDay.value = day;
	    selMonth.value = getMonthString(month);
	    selYear.value = year;
	    
	    calendar.hide();
	} 
	
	function loadDate() {
	    
	    var selDay = DOM.get(selDayId); 
	    var selMonth = DOM.get(selMonthId); 
	    var selYear = DOM.get(selYearId); 
	    
		var input = "";
		if(date != "") {
			if(selMonth.value && getMonthNumber(selMonth.value) != null) {
				input += getMonthNumber(selMonth.value) + "/";
			} else {
				return;
			}
			if(selDay.value) {
				input += selDay.value + "/";
			} else {
				return;
			}
			if(selYear.value) {
				input += selYear.value;
			} else {
				return;
			}
			calendar.select(input);
			calendar.cfg.setProperty("pagedate",getMonthNumber(data[1])+"/"+data[2]);
			calendar.render();
		}
	}
	
	calendar.selectEvent.subscribe(handleSelect, calendar, true);
	calendar.beforeShowEvent.subscribe(loadDate,calendar,true);
	YAHOO.util.Event.addListener(btn,"click",calendar.show,calendar,true);
	
	calendar.render();
}

function resetDate(td){
	var dates = td.childNodes;
	for(var i = 0; i < dates.length; i++){
		if(dates.item(i).tagName == 'SELECT'){
			dates.item(i).value = '';
		}
	}
}
