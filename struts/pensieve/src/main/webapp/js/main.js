var DOM = YAHOO.util.Dom;
var EVENT = YAHOO.util.Event;
var autosaving = false;
var date = new Date();

var alphaNumeric = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
var validSearch = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";
var canHideSuggest = true;

var popupHeight = 500; 
var popupWidth = 750;

var top = (screen.height - popupHeight)/2;
var left = (screen.width - popupWidth)/2;

var badChar = "()-";

function isAlphaNumeric(str) {
	for(var i=0; i<str.length; i++) {
		if(alphaNumeric.indexOf(str.charAt(i)) < 0) {
			return false;
		}
	}
	return true;
}

function setCookie(c_name,value,expiredays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=c_name+ "=" +escape(value)+
	((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function getCookie(c_name){
	if (document.cookie.length>0){
	  c_start=document.cookie.indexOf(c_name + "=");
	  
	  	if (c_start!=-1){
	    c_start=c_start + c_name.length+1;
	    c_end=document.cookie.indexOf(";",c_start);
	    
	    if (c_end==-1) c_end=document.cookie.length;
	    return unescape(document.cookie.substring(c_start,c_end));
	    }
	}
	return "";
}

function validKeyCode(keyCode) {
	if((keyCode >= 48 && keyCode <= 90) || 
	   (keyCode >= 96 && keyCode<= 105) ||
	    keyCode == 8 ||
	    keyCode == 46) {
		return true;
	}
	return false;
}

function goToPage() {
	showLoadingImage();
	var page = DOM.get('page').value;
	var img = DOM.get('loading-image');
	location.href = page;
	img.src = img.src;
}

function delayHideSuggest(div) {
	var hide = function() {
		if(canHideSuggest) {
			DOM.addClass(div,'hide');
		}
	}
	setTimeout(hide,500);
}

function refreshParent() {
	  window.opener.location.href = window.opener.location.href;

	  if (window.opener.progressWindow){
	    window.opener.progressWindow.close()
	  }
	  window.close();
}

function getLink(type){
	var link ='';
	if(type == 'contact'){
		link = '/pensieve/jsp/edit_contact.jsp?id=';
	}else if(type == 'note'){
		link = '/pensieve/jsp/edit_note.jsp?id=0';
	}else if (type == 'email'){
		link = '/pensieve/jsp/email.jsp?to=';
	}
	location.href = link;
}

function showLoadingImage() {
	var img = DOM.get('loading-image');
	DOM.removeClass(img,'hide');
}

function hideLoadingImage() {
	var img = DOM.get('loading-image');
	DOM.addClass(img,'hide');
}

function handleError(o) {
	alert('AJAX ERROR');
}

function highlightEl(el) {
	DOM.addClass(el,'highlight');
}

function unhighlightEl(el) {
	DOM.removeClass(el,'highlight');
}

function initContacts(){
	initCalendar('bdayCal','birthDay','birthMonth','birthYear','bdayButton');
}
function initNotes(){
	initCalendar('startDateCal','startDay','startMonth','startYear','startDateButton');
	initCalendar('endDateCal','endDay','endMonth','endYear','endDateButton');
}