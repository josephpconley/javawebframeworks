function editContact(id){
	window.open("/pensieve/jsp/edit_contact.jsp?id="+id,"Edit Contact","height="+popupHeight+
			",width="+popupWidth+",top="+top+",left="+left);
}

function deleteContact(id){
	if(confirm('Delete this Contact?')) {
		location.href = '/pensieve/deleteContact.do?id=' + id;
	}
}

function filterSaveSuccess(o) {
	if(o) {
		location.href = o.argument[0];
	}
}

function filterSaveError(o) {
	if(o) {
		location.href = o.argument[0];
	}
}

function filter(){
	var query = DOM.get('filter').value;
	
	var args = [location];
	var saveHandler = {
		success: filterSaveSuccess, 
		failure: filterSaveError, 
		argument: args
	};
	
	var request = YAHOO.util.Connect.asyncRequest('POST','showContacts.do?query='+query,saveHandler,null); 	
}

function isBad(str){
	for(var i=0; i<str.length; i++) {
		if(badChar.indexOf(str.charAt(i)) < 0) {
			return false;
		}
	}
	return true;
}

function getEmail(str,cell){
	var newStr='';
	for(var i =0; i<str.length;i++){
		if(!isBad(str.charAt(i)))
			newStr += str.charAt(i);
	}
	
	if(cell == 'Verizon'){
		newStr += '@vtext.com';
	}
	if(cell == 'AT&T'){
		newStr += '@txt.att.net';
	}
	
	return newStr;
}