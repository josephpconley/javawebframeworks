
function editNote(id){
	if(id){
		window.open("/pensieve/jsp/edit_note.jsp?id="+id,"Edit Note","height="+popupHeight+
					",width="+popupWidth+",top="+top+",left="+left);
	}
}

function deleteNote(id){
	if(confirm('Delete this Note?')) {
		location.href = '/pensieve/deleteNote.do?id='+id;
	}
}

function showResults(o) {
	var div = DOM.get(o.argument[0]);
	if(o) {
		div.innerHTML = o.responseText;
	}
}

function lookupCategory(e){
	var keyCode = e.keyCode;
	if(!keyCode || validKeyCode(keyCode)) {
		var div = DOM.get('category-suggest');
		var value = DOM.get('category').value.trim();
		
		if(value != "" && isAlphaNumeric(value)) {
			var args = ['category-suggest'];
			var categoryHandler = {
				success: showResults, 
				failure: handleError, 
				argument: args
			};
	
			var request = YAHOO.util.Connect.asyncRequest('POST',
									'/pensieve/ajax.do?type=CATEGORY&value='+value,
									categoryHandler,null); 	
			DOM.removeClass(div,'hide');
		}
	}
}

function loadCategory(category){
	DOM.get('category').value=category;
	
	var div = DOM.get('category-suggest');
	DOM.addClass(div,'hide');
}

function lookupFrequency(e){
	var keyCode = e.keyCode;
	if(!keyCode || validKeyCode(keyCode)) {
		var div = DOM.get('frequency-suggest');
		var value = DOM.get('frequency').value.trim();
		
		if(value != "" && isAlphaNumeric(value)) {
			var args = ['frequency-suggest'];
			var frequencyHandler = {
				success: showResults, 
				failure: handleError, 
				argument: args
			};
	
			var request = YAHOO.util.Connect.asyncRequest('POST',
									'/pensieve/ajax.do?type=FREQUENCY&value='+value,
									frequencyHandler,null); 	
			DOM.removeClass(div,'hide');
		}
	}
}

function loadFrequency(frequency){
	DOM.get('frequency').value=frequency;
	
	var div = DOM.get('frequency-suggest');
	DOM.addClass(div,'hide');
}