create function get_dow(double precision) returns varchar as $$
declare 
	dayOfWeek varchar;
begin	
	if ($1 = 0) then
		dayOfWeek := 'Sunday';		
	end if;
	if ($1 = 1) then
		dayOfWeek := 'Monday';		
	end if;
	if ($1 = 2) then
		dayOfWeek := 'Tuesday';		
	end if;	
	if ($1 = 3) then
		dayOfWeek := 'Wednesday';		
	end if;	
	if ($1 = 4) then
		dayOfWeek := 'Thursday';		
	end if;	
	if ($1 = 5) then
		dayOfWeek := 'Friday';		
	end if;
	if ($1 = 6) then
		dayOfWeek := 'Saturday';		
	end if;	

return dayOfWeek;
end;
$$
language 'plpgsql';