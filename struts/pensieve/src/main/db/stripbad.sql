create function stripbad(varchar, varchar) returns varchar as $$
declare 
	str1 varchar;
	str2 varchar;
	str3 varchar;
begin
	str1 := replace($1,'(','');
	str2 := replace(str1,')','');
	str3 := replace(str2,'-','');	
	
	if $2 = 'Verizon' then
		str3 := str3 || '@vtext.com';
	end if;
	
	if $2 = 'AT&T' then
		str3 := str3 || '@txt.att.net';
	end if;
return str3;
end;
$$
language 'plpgsql';
