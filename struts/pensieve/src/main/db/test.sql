SELECT * from pensieve.notes
WHERE date_part('month',start_date) = date_part('month',current_date)
				AND date_part('day',start_date) = date_part('day',current_date)
				AND frequency = 'Annual'