--\i C:/tomcat/webapps/pensieve/db/create_tables.sql

DROP TABLE pensieve.contacts;
DROP TABLE pensieve.email;
DROP TABLE pensieve.holidays;
DROP TABLE pensieve.notes;	

CREATE TABLE pensieve.contacts()
  id character varying(30) NOT NULL,
  firstname character varying(15) NOT NULL,
  lastname character varying(20),
  bday date,
  phone1 character(13),
  phone2 character(13),
  phone3 character(13),
  phone4 character(13),
  address1 character varying(50),
  address2 character varying(50),
  address3 character varying(50),
  address4 character varying(50),
  email1 character varying(40),
  email2 character varying(30),
  email3 character varying(30),
  email4 character varying(30),
  pic_flag boolean DEFAULT false,
  cell_service character varying(20),
  age double precision,
  CONSTRAINT contacts_pkey PRIMARY KEY (id)
);

CREATE TABLE pensieve.email(
  provider character varying(30),
  transport character varying(20),
  username character varying(30),
  "password" character varying(40),
  inbox character varying(20),
  sent character varying(30),
  imap_host character varying(30),
  smtp_host character varying(30),
  pop3_host character varying(30)
);

CREATE TABLE pensieve.holidays(
  holiday_desc character varying(30) NOT NULL,
  "month" integer,
  dow integer,
  start_day integer,
  end_day integer,
  CONSTRAINT holidays_pkey PRIMARY KEY (holiday_desc)
);

CREATE TABLE pensieve.notes(
  id serial NOT NULL,
  "content" character varying(500),
  category character varying(30),
  start_date date,
  end_date date,
  start_time time without time zone,
  end_time time without time zone,
  frequency character varying(30),
  reminder boolean DEFAULT false
);