create or replace function get_dow(double precision) returns varchar as $$
declare 
	dayOfWeek varchar;
begin	
	if ($1 = 0) then
		dayOfWeek := 'Sunday';		
	end if;
	if ($1 = 1) then
		dayOfWeek := 'Monday';		
	end if;
	if ($1 = 2) then
		dayOfWeek := 'Tuesday';		
	end if;	
	if ($1 = 3) then
		dayOfWeek := 'Wednesday';		
	end if;	
	if ($1 = 4) then
		dayOfWeek := 'Thursday';		
	end if;	
	if ($1 = 5) then
		dayOfWeek := 'Friday';		
	end if;
	if ($1 = 6) then
		dayOfWeek := 'Saturday';		
	end if;	

return dayOfWeek;
end;
$$
language 'plpgsql';

create or replace function write_bday(varchar) returns varchar as $$
declare 
	str1 varchar;
	yrs double precision;
	bday date;
	rec record;
begin
	bday := current_date;
	for rec in select * from pensieve.contacts
			where id = $1 
	loop
		bday := rec.bday;
	end loop;
	
	if (date_part('month',bday) = date_part('month',current_date) and 
		date_part('day',bday) = date_part('day',current_date)) then
		yrs := date_part('year',age(bday));
	else
		yrs := date_part('year',age(bday)) + 1;
	end if;
		
	str1 := $1 ||  '''s Birthday(' || yrs || ')' ;
return str1;
end;
$$
language 'plpgsql';


create or replace function stripbad(varchar, varchar) returns varchar as $$
declare 
	str1 varchar;
	str2 varchar;
	str3 varchar;
begin
	str1 := replace($1,'(','');
	str2 := replace(str1,')','');
	str3 := replace(str2,'-','');	
	
	if $2 = 'Verizon' then
		str3 := str3 || '@vtext.com';
	end if;
	
	if $2 = 'AT&T' then
		str3 := str3 || '@txt.att.net';
	end if;
return str3;
end;
$$
language 'plpgsql';
