create function write_bday(varchar) returns varchar as $$
declare 
	str1 varchar;
	yrs double precision;
	bday date;
	rec record;
begin
	bday := current_date;
	for rec in select * from pensieve.contacts
			where id = $1 
	loop
		bday := rec.bday;
	end loop;
	
	if (date_part('month',bday) = date_part('month',current_date) and 
		date_part('day',bday) = date_part('day',current_date)) then
		yrs := date_part('year',age(bday));
	else
		yrs := date_part('year',age(bday)) + 1;
	end if;
		
	str1 := $1 ||  '''s Birthday(' || yrs || ')' ;
return str1;
end;
$$
language 'plpgsql';
