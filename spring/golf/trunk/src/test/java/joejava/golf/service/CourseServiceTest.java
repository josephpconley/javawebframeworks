package joejava.golf.service;

import static org.junit.Assert.assertNotNull;
import joejava.golf.domain.Course;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Tee;
import joejava.golf.util.MockJndiUtility;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:META-INF/spring/applicationContext*"})
public class CourseServiceTest {

	Logger log = Logger.getLogger(this.getClass());
	
    @Autowired
    private CourseService courseService;

    @Autowired
    private TeeService teeService;    
    
    @Before
    public void setUp() throws Exception {
        MockJndiUtility.setup();
    }

    @After
    public void tearDown() throws Exception {
    }

	@Test
	@Transactional
    public final void testCourse(){
		Course course = new Course();
		course.setName("TEST");
		
		//Integer holeNo, Integer par, Integer yardage, Integer handicap, Tee tee
		Tee whites = new Tee("White",66.2,121);
		whites.getHole().add(new Hole(1, 4, 319, 14));
		whites.getHole().add(new Hole(2, 3, 119, 18));
		whites.getHole().add(new Hole(3, 5, 480, 6));
		whites.getHole().add(new Hole(4, 3, 135, 16));
		whites.getHole().add(new Hole(5, 4, 317, 12));
		whites.getHole().add(new Hole(6, 4, 261, 4));
		whites.getHole().add(new Hole(7, 5, 441, 10));
		whites.getHole().add(new Hole(8, 3, 158, 8));
		whites.getHole().add(new Hole(9, 4, 363, 2));
		
		whites.getHole().add(new Hole(10, 4, 250, 15));
		whites.getHole().add(new Hole(11, 5, 430, 5));
		whites.getHole().add(new Hole(12, 4, 350, 9));
		whites.getHole().add(new Hole(13, 4, 340, 3));
		whites.getHole().add(new Hole(14, 4, 370, 7));
		whites.getHole().add(new Hole(15, 3, 150, 11));
		whites.getHole().add(new Hole(16, 4, 300, 13));
		whites.getHole().add(new Hole(17, 3, 115, 17));
		whites.getHole().add(new Hole(18, 5, 519, 1));
		
		course.getTee().add(whites);
		courseService.addCourse(course);
		assertNotNull(course.getId());
		
		courseService.deleteCourse(course);
		
		//Course paxon = courseService.findCourseByCode("PAXON");
	}
	
	/*
	@Test
	@Transactional
	public final void testDeletes(){
		String[] code = {"A","B","C","D"};
		for(int i=0;i<4;i++){
			Course c = new Course();
			c.setCode(code[i]);
			c.setName(code[i] + " Country Club");
			courseService.addCourse(c);
		}
		
		assertTrue(courseService.listCourse().size() > 3);
		
		courseService.deleteAllCourses();
		assertTrue(courseService.listCourse().size() == 0);
	}
	*/
}
