package joejava.golf.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import joejava.golf.domain.Pick;
import joejava.golf.util.MockJndiUtility;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WaitingRefreshHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

/**
 * @author jconley
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/applicationContext*" })
@TransactionConfiguration(defaultRollback=false)
public class PgaServiceTest {

	Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private PgaService pgaService;

	@Before
	public void setUp() throws Exception {
		MockJndiUtility.setup();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Transactional
	@SuppressWarnings("unchecked")
	public final void updateESPNIds() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		final String ESPN_1 = "http://scores.espn.go.com/golf/leaderboard";	//XPATH: //table[@class='tablehead']
		final String ESPN_2 = "http://sports.espn.go.com/golf/leaderboard";
		final WebClient webClient = new WebClient();
	    final HtmlPage page = (HtmlPage) webClient.getPage(ESPN_2);
	    //assertEquals("HtmlUnit - Welcome to HtmlUnit", page.getTitleText());

	    final String pageAsXml = page.asXml();
	    System.out.println(pageAsXml);
	    
	    List<HtmlAnchor> anchors = (List<HtmlAnchor>)page.getByXPath("//a[@player_id]");
	    
	    for(HtmlAnchor a : anchors){
	    	String golfer = a.asText();
	    	log.info(golfer);
	    	Pick pick = pgaService.getPickByName(golfer);
	    	if(pick != null){
	    		log.info(pick.getId());
		    	pick.setEspnId(Integer.valueOf(a.getAttribute("player_id")));
	    	}
	    }
	}
	
	@Test
	@Transactional
	@Ignore
	public final void updatePGAIds() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		//LINKS
		final String PGA_1 = "http://www.majorschampionships.com/masters/2011/scoring/index.cfm";	//table[@class='leaderMain']
		
		final WebClient webClient = new WebClient();
		webClient.setRefreshHandler(new WaitingRefreshHandler());
	    final HtmlPage page = (HtmlPage) webClient.getPage(PGA_1);
	    //assertEquals("HtmlUnit - Welcome to HtmlUnit", page.getTitleText());

	    //final String pageAsXml = page.asXml();
	    //System.out.println(pageAsXml);
	    
	    HtmlTable table = (HtmlTable)page.getByXPath("//table[@summary='Leaderboard Table']").get(0);
	    
	    ArrayList<String> manual = new ArrayList<String>();
	    
	    int n = 0;
	    for(HtmlTableRow tr : table.getBodies().get(2).getRows()){
	    	
	    	Pick pick = new Pick();
	    	Integer id = Integer.valueOf(tr.getAttribute("id").substring(3));
	    	pick.setPgaId(id);
	    	pick.setPosition(tr.getCells().get(0).asText());
	    	pick.setStartPosition(tr.getCells().get(1).asText());
	    	pick.setGolfer(tr.getCells().get(2).asText());
	    	pick.setTotal(pgaService.getScore(tr.getCells().get(3).asText()));
	    	pick.setThru(tr.getCells().get(4).asText());
	    	pick.setToday(pgaService.getScore(tr.getCells().get(5).asText()));
	    	pick.setRound1(pgaService.getScore(tr.getCells().get(6).asText()));
	    	pick.setRound2(pgaService.getScore(tr.getCells().get(7).asText()));
	    	pick.setRound3(pgaService.getScore(tr.getCells().get(8).asText()));
	    	pick.setRound4(pgaService.getScore(tr.getCells().get(9).asText()));
	    	pick.setTotalScore(pgaService.getScore(tr.getCells().get(10).asText()));
	    	
	    	System.out.println(pick);
	    	/*
	    	for(HtmlTableCell td : tr.getCells()){
	    		System.out.print(td.asText() + "[" + m + "] ");
	    		m++;
	    	}
	    	*/
	    	Pick p = pgaService.getPickByName(pick.getGolfer());
	    	
	    	if(p != null){
	    		log.info(p.getId());
	    		p.setPgaId(pick.getPgaId());
	    	}else{
	    		manual.add(pick.getGolfer() + "-" + pick.getPgaId());
	    	}
	    	n++;
	    }		
	    
	    System.out.println(manual);
	    System.out.println(n);		
	}

	@Test
	@Transactional
	@Ignore
	public final void updatePGAScores() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		pgaService.updatePgaScores();
	}
}