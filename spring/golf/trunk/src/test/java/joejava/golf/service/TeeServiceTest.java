package joejava.golf.service;

import static org.junit.Assert.assertTrue;
import joejava.golf.util.MockJndiUtility;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:META-INF/spring/applicationContext*"})
public class TeeServiceTest {

	Logger log = Logger.getLogger(this.getClass());
	
    @Autowired
    private TeeService teeService;
    
    @Before
    public void setUp() throws Exception {
        MockJndiUtility.setup();
    }

    @After
    public void tearDown() throws Exception {
    }

	@Test
	@Transactional
	public final void testTees(){
		assertTrue(1 > 0);
	}
}
