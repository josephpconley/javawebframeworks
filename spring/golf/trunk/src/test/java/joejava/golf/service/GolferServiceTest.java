package joejava.golf.service;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;

import joejava.golf.domain.Golfer;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.gdata.client.Query;
import com.google.gdata.client.Service.GDataRequest;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.Link;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.util.ServiceException;

/**
 * @author jconley
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/applicationContext*" })
//@TransactionConfiguration(defaultRollback=false)
public class GolferServiceTest {

	Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private GolferService golferService;
	
	@Test
	@Transactional
	@Ignore
	public final void testGolfer() {
		Golfer golfer = new Golfer();
		golfer.setName("Joe Conley");
		golfer.setNickname("Jolty");
		
		//violations
		golfer.setUsername("jpc");
		golfer.setPassword("abcd123");
		golfer.setAge(10);
		golfer.setBirthDate(new Date());
		golfer.setEmail("asjdfalkjdf");

		/*
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();		
		Set<ConstraintViolation<Golfer>> errors = validator.validate(golfer);		
		
		for(ConstraintViolation<Golfer> e : errors){
			System.out.println(e.getInvalidValue() + " " + e.getMessage());
		}
		*/
		
		golferService.addGolfer(golfer);
		assertNotNull(golfer.getId());

		Golfer joe = golferService.findGolferById(golfer.getId());
		golferService.deleteGolfer(joe);
	}
	
	@Test
	@Transactional
	@Ignore
	public final void testHandicap(){
		Golfer golfer = golferService.findFirstGolfer();
		log.info(golfer.getHandicap().doubleValue());
		log.info(golfer.getRound().size());
	}
	
	@Test
	@Transactional
	@Ignore
	public final void testAllGolfers(){
		List<Golfer> golfers = golferService.listGolfer();
		for(Golfer g : golfers){
			System.out.println(g.getName());
		}
	}
	
	@Test
	@Transactional
	public final void uploadGooglePhoto() throws IOException, ServiceException{
		Golfer joe = golferService.findGolferById(22);
		
		ContactsService contactService = new ContactsService("Joe");
        contactService.setUserCredentials("josephpconley@gmail.com", "theone08");
        
        URL contactUrl = new URL("https://www.google.com/m8/feeds/contacts/default/full");
        
        Query contactQuery = new Query(contactUrl);
        //make sure to give me ALL contacts, not the first 25
        contactQuery.setMaxResults(1000);
        
        //I only care about Contacts added to My Contacts
        contactQuery.setStringCustomParameter("group", "http://www.google.com/m8/feeds/groups/josephpconley%40gmail.com/base/6");
        String q = "Jolty";
        
        contactQuery.setFullTextQuery(q);
        
        ContactFeed contactFeed = contactService.query(contactQuery, ContactFeed.class);
        ContactEntry entry = contactFeed.getEntries().get(0);
        
	  	  Link photoLink = entry.getContactPhotoLink();
		  if (photoLink != null && photoLink.getEtag() != null) {
			  //GDataRequest request = service.createRequest(GDataRequest.RequestType.UPDATE, new URL(photoLink.getHref()), new ContentType("image/jpeg"));
			  GDataRequest request = contactService.createLinkQueryRequest(photoLink);
			  request.execute();
			  
			  InputStream in = request.getResponseStream();
			  joe.setPhoto(IOUtils.toByteArray(in));
			  request.end();
		  }        
	}
}