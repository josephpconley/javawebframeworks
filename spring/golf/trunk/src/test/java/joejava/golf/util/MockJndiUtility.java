package joejava.golf.util;

import java.sql.SQLException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.apache.log4j.Logger;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

/**
 * Class to create a mock JNDI object in which we add a direct connection to the
 * Oracle jdbc. Needed so that the standalone Junit tests run and don't fail on
 * the creation of the dataSource bean within applicationContext.xml. This class
 * acts as if a datasource would be added to an application/web server's JNDI
 * tree, under the name java:comp/env/jdbc/XXXX. Has to be invoked from within
 * each one of the Junit tests.
 *
 * @author Joris Vleminckx
 *
 */
public class MockJndiUtility
{

    //TODO: consider writing this to utilize a common properties file that might be the fall back
    //should the JNDI lookup fail
    private static final String url = "jdbc:oracle:thin:@joec.no-ip.org:1521:xe";
    private static final String username = "golf";
    private static final String password = "golf";
    private static final String dsJndiName = "Oracle";
    private static final String dsJndiUrl = "java:comp/env/jdbc/" + dsJndiName;
    private static final String smtpJndiUrl = "java:comp/env/mail/Session";
    private static Logger log = Logger.getLogger( MockJndiUtility.class );

    public static void setup(  )
    {
        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder(  );

        // Create JNDI datasource
        OracleConnectionPoolDataSource ds;

        try
        {
            ds = new OracleConnectionPoolDataSource();
            ds.setURL( url );
            ds.setUser( username );
            ds.setPassword( password );
            builder.bind( dsJndiUrl, ds );
        } catch ( SQLException e )
        {
            log.error( "Failed to create JNDI datasource for " + dsJndiName );
            e.printStackTrace(  );
        }

        // Create JNDI mail
        /*
        Properties props = System.getProperties();
        props.put( "mail.smtp.host", "localhost" );

        Session session = Session.getDefaultInstance( props, null );
        builder.bind( smtpJndiUrl, session );

        try
        {
            builder.activate(  );
        } catch ( IllegalStateException e )
        {
            log.error( "IllegalStateException when activating SimpeNamingContextBuilder object", e );
            e.printStackTrace(  );
        } catch ( NamingException e )
        {
            log.error( "NamingException when activating SimpeNamingContextBuilder object", e );
            e.printStackTrace(  );
        }
		*/
        log.debug( "Successfully created JNDI datasource: " + smtpJndiUrl );
    }
}
