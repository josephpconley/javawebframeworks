package joejava.golf.service;

import joejava.golf.domain.Round;
import joejava.golf.util.MockJndiUtility;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/applicationContext*" })
public class RoundServiceTest {

	Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private RoundService roundService;
	
	@Before
	public void setUp() throws Exception {
		MockJndiUtility.setup();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Transactional
	public final void testRound() {
		Round round = new Round();
		
		//verify this first course is an 18-hole course, score matches par
		/*
		for(Tee t : course.getTee()){
			assertTrue(t.getHole().size() == 18);
			
			for(Hole h : t.getHole()){
				round.getScores().add(new Score(h, h.getPar()));
			}
			
			assertTrue(t.getPar() == round.getTotalScore());
		}
		*/
	}
}
