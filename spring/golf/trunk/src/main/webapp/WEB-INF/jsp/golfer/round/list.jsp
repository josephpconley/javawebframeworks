<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<title>Upload your CSV file of rounds</title>
	<style type="text/css" media="screen">
		@import url("/golf/css/styles.css");
    </style>
</head>
<body>
	<h2>Upload File</h2>
	<form method="post" action="upload" enctype="multipart/form-data">
		<input type="file" name="file"/>
		<input type="submit" name="button" value="Upload"/>
	</form>
	
	<h3>Rounds</h3>
	<c:if  test="${!empty roundList}">
	<table class="data">
	<tr>
		<th>Id</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${roundList}" var="round">
		<tr>
			<td>${round.id}</td>
			<td><a href="delete/${round.id}">delete</a></td>
		</tr>
	</c:forEach>
	</table>
	</c:if>	
</body>
</html>
