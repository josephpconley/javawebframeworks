<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<div id="pick">
	<h3>2011 Masters Picks (as of ${lastUpdated})</h3>

	<c:if  test="${!empty pickList}">
	<table class="data">
		<tr>
			<th>Position</th>
			<th>Owner</th>
			<th>Golfer</th>
			<th>Total</th>
			<th>Thru</th>
			<th>Today</th>
			<th>Rd. 1</th>
			<th>Rd. 2</th>
			<th>Rd. 3</th>
			<th>Rd. 4</th>
			<th>Total Score</th>
		</tr>
	<c:forEach items="${pickList}" var="pick">
		<tr>
			<td>${pick.position}</td>
			<td>${pick.owner.name}</td>
			<td>${pick.golfer}</td>
			<td>${pick.total}</td>
			<td>${pick.thru}</td>
			<td>${pick.today}</td>
			<td>${pick.round1}</td>
			<td>${pick.round2}</td>
			<td>${pick.round3}</td>
			<td>${pick.round4}</td>
			<td>${pick.totalScore}</td>
		</tr>
	</c:forEach>	
	
		<c:if test="${!empty cutList }">
			<tr><td colspan="11" align="center">CUT</td></tr>
			<c:forEach items="${cutList}" var="cut">
				<tr>
					<td>${cut.position}</td>
					<td>${cut.owner.name}</td>
					<td>${cut.golfer}</td>
					<td>${cut.total}</td>
					<td>${cut.thru}</td>
					<td>${cut.today}</td>
					<td>${cut.round1}</td>
					<td>${cut.round2}</td>
					<td>${cut.round3}</td>
					<td>${cut.round4}</td>
					<td>${cut.totalScore}</td>
				</tr>
			</c:forEach>
		</c:if>
		
	</table>
	</c:if>
</div>	
