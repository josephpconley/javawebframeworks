<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Golf</title>
    <style type="text/css" media="screen">
		@import url("/golf/css/styles.css");
    </style>
</head>
<body>

<h2>Golf</h2>

<!-- use href for add just like delete below? -->
<form:form method="post" action="add" commandName="golfer">

	<table>
	<tr>
		<td><form:label path="firstName"><spring:message code="label.firstName"/></form:label></td>
		<td><form:input path="firstName" /></td> 
	</tr>
	<tr>
		<td><form:label path="lastName"><spring:message code="label.lastName"/></form:label></td>
		<td><form:input path="lastName" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="<spring:message code="label.addgolfer"/>"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Golfers</h3>
	<c:if  test="${!empty golferList}">
	<table class="data">
		<tr>
			<th>Name</th>
			<th>&nbsp;</th>
		</tr>
	<c:forEach items="${golferList}" var="golfer">
		<tr>
			<td><a href="${golfer.id}">${golfer.lastName}, ${golfer.firstName}</a></td>
			<td><a href="delete/${golfer.id}">delete</a></td>
		</tr>
	</c:forEach>		
	</table>
	</c:if>
</body>
</html>
