<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<h3>Show Golfer</h3>

	<c:if  test="${!empty golfer}">
		<table class="data">
			<tr>
				<th>&nbsp;</th>
				<th>Name</th>
				<th>Handicap</th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td><img src="/golf/golfer/${golfer.id}.img"/></td>
				<td>${golfer.name}</td>
				<td><fmt:formatNumber value="${golfer.handicap}" maxFractionDigits="5"/></td>
				<td><a href="delete/${contact.id}">delete</a></td>
			</tr>
		</table>
	</c:if>

	<h3>Upload Rounds</h3>
	<form method="post" action="/golf/golfer/${golfer.id}/round/upload" enctype="multipart/form-data">
		<input type="file" name="round"/>
		<input type="submit" name="button" value="Upload"/>
	</form>

	<h3>Rounds</h3>
	<c:if  test="${!empty golfer.round}">
	<table class="data">
		<tr>
			<th>Id</th>
			<th>Date</th>
			<th>Course</th>
			<th>Tee</th>
			<th>Rating</th>
			<th>Slope</th>
			<th>Score</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${golfer.round}" var="round">
			<tr>
				<td>${round.id }</td>
				<td>${round.roundDate }</td>
				<td>${round.tee.course.name }</td>
				<td>${round.tee.name }</td>
				<td>${round.tee.rating }</td>
				<td>${round.tee.slope }</td>
				<td>${round.scoreTotal }</td>
				<td><a href="delete/${round.id}">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	</c:if>