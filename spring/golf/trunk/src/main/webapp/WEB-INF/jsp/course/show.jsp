<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Golf!</title>
	<style type="text/css" media="screen">
		@import url("/golf/css/styles.css");
    </style>
</head>
<body>

<h2>Show Course</h2>

	<c:if  test="${!empty course}">
	<table class="data">
		<tr>
			<th>Id</th>
			<th>Code</th>
			<th>Name</th>
			<th>Phone</th>
			<th>Address</th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<td>${course.id }</td>
			<td>${course.code }</td>
			<td>${course.name }</td>
			<td>${course.phone }</td>
			<td>${course.address }</td>
			<td><a href="delete/${course.id}">delete</a></td>
		</tr>
	</table>
	</c:if>

<!-- 
	<h2>Upload Tees</h2>
	<form method="post" action="/golf/course/tee/${golfer.id}/round/upload" enctype="multipart/form-data">
		<input type="file" name="round"/>
		<input type="submit" name="button" value="Upload"/>
	</form>
 -->

	<h3>Tees</h3>
	<c:if  test="${!empty golfer.round}">
	<table class="data">
		<tr>
			<th>Course</th>
			<th>Tee</th>
			<th>Score</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${golfer.round}" var="round">
			<tr>
				<td>${round.tee.course.name }</td>
				<td>${round.tee.name }</td>
				<td>${round.scoreTotal }</td>
				<td><a href="delete/${round.id}">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	</c:if>	

</body>
</html>
