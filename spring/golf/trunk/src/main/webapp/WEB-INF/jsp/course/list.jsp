<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<title>Upload your CSV file of courses</title>
	<style type="text/css" media="screen">
		@import url("/golf/css/styles.css");
    </style>
</head>
<body>

	<h2>Add Course</h2>

	<!-- use href for add just like delete below? -->
	<form:form method="post" action="add" commandName="course">
	
		<table>
		<tr>
			<td><form:label path="name">Name</form:label></td>
			<td><form:input path="name" /></td> 
		</tr>
		<tr>
			<td><form:label path="code">Code</form:label></td>
			<td><form:input path="code" /></td> 
		</tr>
		<tr>
			<td><form:label path="phoneNumber">Phone Number</form:label></td>
			<td><form:input path="phoneNumber" /></td> 
		</tr>
		<tr>
			<td><form:label path="address.street">Street</form:label></td>
			<td><form:input path="address.street" /></td> 
		</tr>
		<tr>
			<td><form:label path="address.city">City</form:label></td>
			<td><form:input path="address.city" /></td> 
		</tr>
		<tr>
			<td><form:label path="address.stateProv">State/Prov.</form:label></td>
			<td><form:input path="address.stateProv" /></td> 
		</tr>
		<tr>
			<td><form:label path="address.zip">Zip</form:label></td>
			<td><form:input path="address.zip" /></td> 
		</tr>				
		<tr>
			<td colspan="2">
				<input type="submit" value="Add Course"/>
			</td>
		</tr>
	</table>	
	</form:form>	

	<h2>Upload Course</h2>
	<form method="post" action="upload" enctype="multipart/form-data">
		<input type="file" name="course"/>
		<input type="submit" name="button" value="Upload"/>		
	</form>

	<h2>Upload Tee</h2>
	<form method="post" action="tee/upload" enctype="multipart/form-data">
		<input type="file" name="tee"/>
		<input type="submit" name="button" value="Upload"/>		
	</form>
	
	<h3>Courses</h3>
	<c:if  test="${!empty courseList}">
	<table class="data">
	<tr>
		<th>Id</th>
		<th>Code</th>
		<th>Name</th>
		<th>Phone</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${courseList}" var="course">
		<tr>
			<td>${course.id}</td>
			<td>${course.code}</td>
			<td>${course.name }</td>
			<td>${course.phoneNumber }</td>
			<td><a href="delete/${course.id}">delete</a></td>
		</tr>
		
		<c:if test="${!empty course.tee }">
		<tr>
			<table class="data">
				<tr>
					<th>Id</th>
					<th>Tee</th>
					<th>Slope</th>
					<th>Rating</th>
					<th>&nbsp;</th>
				</tr>		
			<c:forEach items="${course.tee}" var="tee">
				<tr>
					<td>${tee.id}</td>
					<td>${tee.name }</td>
					<td>${tee.slope}</td>
					<td>${tee.rating}</td>
					<td><a href="delete/${tee.id}">delete</a></td>
				</tr>
			</c:forEach>
			</table>
		</tr>
		</c:if>
	</c:forEach>
	</table>
	</c:if>	
</body>
</html>
