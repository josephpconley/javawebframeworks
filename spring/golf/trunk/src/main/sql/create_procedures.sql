--@ c:\workspace\golf\src\main\sql\create_procedures.sql;

--@ c:\workspace\golf\src\main\sql\create_functions.sql

CREATE OR REPLACE FUNCTION GET_ROUND_COUNT(golferId INTEGER) RETURN integer AS
	numRounds integer := 0;
	totRounds integer := 0;
BEGIN
	SELECT COUNT(*) INTO totRounds FROM GOLF.ROUND WHERE GOLFER_ID = golferId;

	IF totRounds = 5 OR totRounds = 6 THEN
		numRounds := 1;
	ELSIF totRounds = 7 OR totRounds = 8 THEN
		numRounds := 2;
	ELSIF totRounds = 9 OR totRounds = 10 THEN
		numRounds := 3;
	ELSIF totRounds = 11 OR totRounds = 12 THEN
		numRounds := 4;
	ELSIF totRounds = 13 OR totRounds = 14 THEN
		numRounds := 5;
	ELSIF totRounds = 15 OR totRounds = 16 THEN
		numRounds := 6;
	ELSIF totRounds > 16 AND totRounds < 21 THEN
		numRounds := totRounds - 10;
	ELSIF totRounds > 20 THEN
		numRounds := 10;
	END IF;
	
	RETURN numRounds;
END;
/

CREATE OR REPLACE PROCEDURE SET_DIFFERENTIAL(roundId INTEGER) AS 
	round GOLF.ROUND%ROWTYPE;
	tee GOLF.TEE%ROWTYPE;
	diff NUMBER;
	roundScore NUMBER;
BEGIN
	SELECT * INTO round FROM GOLF.ROUND WHERE ID = roundId;	
	SELECT * INTO tee FROM GOLF.TEE WHERE ID = round.TEE_ID;
	
	SELECT SUM(SCORE) INTO roundScore FROM SCORE WHERE ROUND_ID = roundId;
	
	UPDATE ROUND
	SET DIFFERENTIAL = (roundScore - tee.RATING) * (113/tee.SLOPE)
	WHERE ID = roundId;
END SET_DIFFERENTIAL;
/

CREATE OR REPLACE PROCEDURE SET_HANDICAP(golferId INTEGER) AS 
	totDiff NUMBER := 0;
	hCap NUMBER := 0;
	numDiff integer := 0;
	
	golfer varchar(20);
BEGIN
	numDiff := GET_ROUND_COUNT(golferId);
	
	FOR round IN (SELECT * FROM ROUND WHERE GOLFER_ID = golferId ORDER BY differential) LOOP
		totDiff := totDiff + round.differential;
	END LOOP;
	
	IF numDiff > 0 THEN
		hCap := (totDiff / numDiff) * 0.96;
	ELSE
		hCap := null;
	END IF;
	
	UPDATE GOLFER SET HANDICAP = hCap WHERE ID = golferId;
END SET_HANDICAP;
/
