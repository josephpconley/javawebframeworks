package joejava.golf.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import joejava.golf.dao.PgaDAO;
import joejava.golf.domain.Pick;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WaitingRefreshHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

@Service
public class PgaServiceImpl implements PgaService{

	@Autowired
	private PgaDAO pgaDAO;
	
	//update scores every 5 minutes during the timeframe of a standard golf tournament
	@Scheduled(cron="0 0/5 7-19 * * THU-SAT,SUN")
	@Transactional
	public void updatePgaScores() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		//LINKS
		final String PGA_1 = "http://www.majorschampionships.com/masters/2011/scoring/index.cfm";	//table[@class='leaderMain']
		
		final WebClient webClient = new WebClient();
		webClient.setRefreshHandler(new WaitingRefreshHandler());
	    final HtmlPage page = (HtmlPage) webClient.getPage(PGA_1);
	    //assertEquals("HtmlUnit - Welcome to HtmlUnit", page.getTitleText());

	    //final String pageAsXml = page.asXml();
	    //System.out.println(pageAsXml);
	    
	    HtmlTable table = (HtmlTable)page.getByXPath("//table[@summary='Leaderboard Table']").get(0);
	    
	    int n = 0;
	    for(HtmlTableRow tr : table.getBodies().get(2).getRows()){
	    	
	    	if(tr.getCells().size() == 11){
	    	
		    	Integer id = Integer.valueOf(tr.getAttribute("id").substring(3));
		    	
		    	Pick pick = getPickByPgaId(id);
		    	pick.setPosition(tr.getCells().get(0).asText());
		    	pick.setStartPosition(tr.getCells().get(1).asText());
	//	    	pick.setGolfer(tr.getCells().get(2).asText());
		    	pick.setTotal(getScore(tr.getCells().get(3).asText()));
		    	pick.setThru(tr.getCells().get(4).asText());
		    	pick.setToday(getScore(tr.getCells().get(5).asText()));
	
		    	pick.setRound1(getRoundScore(tr.getCells().get(6).asText()));
		    	pick.setRound2(getRoundScore(tr.getCells().get(7).asText()));
		    	pick.setRound3(getRoundScore(tr.getCells().get(8).asText()));
		    	pick.setRound4(getRoundScore(tr.getCells().get(9).asText()));
		    	pick.setTotalScore(getRoundScore(tr.getCells().get(10).asText()));
		    	pick.setTimeUpdated(new java.util.Date());
		    	
		    	//System.out.println(tr.asText());
		    	//System.out.println(pick);
		    	n++;
	    	}
	    }		
	    
	    System.out.println(n + " golfers updated");	
	}
	
	@Transactional
	public List<Pick> listPicks() {
		return pgaDAO.listPicks();
	}

	@Transactional
	public List<Pick> listCutPicks() {
		return pgaDAO.listCutPicks();
	}		
	
	@Transactional
	public Pick getPickByName(String golfer) {
		return pgaDAO.getPickByName(golfer);
	}

	@Transactional
	public void savePick(Pick pick) {
		pgaDAO.save(pick);
	}

	@Transactional
	public Pick getPickByPgaId(Integer id) {
		return pgaDAO.getPickByPgaId(id);
	}

	public Integer getScore(String score) {
		if(score.contains("+")){
			return Integer.valueOf(score.substring(1));
		}else if(score.equals("E")){
			return 0;
		}else if(score.contains("-") && score.length() > 1){
			return Integer.valueOf(score);
		}else{
			return null;
		}
	}

	public Integer getRoundScore(String score) {
		if(score.contains("-")){
			return null;
		}
		
		return Integer.valueOf(score);
	}
}
