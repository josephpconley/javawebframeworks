package joejava.golf.dao;

import java.io.Serializable;
import java.util.List;

public interface DAO<E> {
	public void save(E entity);
	public void delete(E entity);
	public E findById(Serializable id);
	public List<E> list();
}
