package joejava.golf.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Course {
	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	@Column(unique=true)
	private String code;
	
	private String phoneNumber;
	
	@OneToOne(cascade=CascadeType.ALL)
	private Address address;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="course", orphanRemoval=true)
	private Set<Tee> tee = new HashSet<Tee>();

	public Course(){}
	
	@Override
	public String toString() {
		return "Course [address=" + address + ", code=" + code + ", id=" + id
				+ ", name=" + name + ", phoneNumber=" + phoneNumber + ", tee="
				+ tee + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<Tee> getTee() {
		return tee;
	}

	public void setTee(Set<Tee> tee) {
		this.tee = tee;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
