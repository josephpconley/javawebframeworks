/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.dao.CourseDAO;
import joejava.golf.domain.Course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseDAO courseDAO;
	
	@Transactional
	public void addCourse(Course course) {
		courseDAO.save(course);
	}

	@Transactional
	public List<Course> listCourse() {
		return courseDAO.listCourse();
	}

	@Transactional
	public void deleteCourse(Course course) {
		courseDAO.delete(course);
	}
	
	@Transactional
	public Course findCourseById(Integer id){
		return (Course) courseDAO.findById(id);
	}

	@Transactional
	public Course findFirstCourse() {
		return courseDAO.findFirstCourse();
	}

	@Transactional
	public Course findCourseByNameContaining(String name) {
		return courseDAO.findCourseByNameContaining(name);
	}

	@Transactional
	public void deleteAllCourses() {
		courseDAO.deleteAllCourses();
	}

	@Transactional
	public Course findCourseByCode(String code) {
		return courseDAO.findCourseByCode(code);
	}
}
