package joejava.golf.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import joejava.golf.util.GolfConstants;
@Entity
public class Round {
	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	private Golfer golfer;
	
	@ManyToOne(optional=false)
	private Tee tee;
	
	@OneToMany(cascade={CascadeType.ALL}, mappedBy="round", orphanRemoval=true)
	private List<Score> score = new ArrayList<Score>();

	@Temporal(TemporalType.TIMESTAMP)
	private Date roundDate;
	
	public Round(){}

	public Round(Tee tee, List<Score> score){
		this.tee = tee;
		this.score = score;
	}

	@Transient
	public Integer getScoreTotal(){
		int total = 0;
		for(Score s : this.score){
			total += s.getScore();
		}
		
		return total;
	}
	
	@Transient
	public Integer getScoreOut(){
		int out = 0;
		for(Score s : this.score){
			if(s.getHole().getHoleNo() < 10){
				out += s.getScore();
			}
		}
		
		return out;
	}
	
	@Transient
	public Integer getScoreIn(){
		int in = 0;
		for(Score s : this.score){
			if(s.getHole().getHoleNo() > 9){
				in += s.getScore();
			}
		}		
		
		return in;
	}
	
	@Transient
	public Integer getScoreByPar(){
		return this.getScoreTotal() - this.getTee().getPar();
	}
	
	@Transient
	public Double getDifferential(){
		double diff = (this.getScoreTotal() - this.getTee().getRating()) * (GolfConstants.SLOPE_NUMERATOR/this.getTee().getSlope()); 
		return diff;
	}
	
	@Override
	public String toString() {
		return "Round [id=" + id + "score=" + this.getScoreTotal() + "]";
	}

	public Golfer getGolfer() {
		return golfer;
	}

	public void setGolfer(Golfer golfer) {
		this.golfer = golfer;
	}

	public List<Score> getScore() {
		return score;
	}

	public void setScore(List<Score> score) {
		this.score = score;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tee getTee() {
		return tee;
	}

	public void setTee(Tee tee) {
		this.tee = tee;
	}

	public List<Score> getScores() {
		return score;
	}

	public void setScores(List<Score> score) {
		this.score = score;
	}

	public Date getRoundDate() {
		return roundDate;
	}

	public void setRoundDate(Date roundDate) {
		this.roundDate = roundDate;
	}
}
