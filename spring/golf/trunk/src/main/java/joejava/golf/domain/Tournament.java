package joejava.golf.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Tournament {
	@Id
	@GeneratedValue
	public Integer id;
	
	@OneToOne
	public Course course;
	
	public String name;
	
	public Date startDate;
	
	public Date endDate;

	@OneToMany(cascade={CascadeType.ALL}, mappedBy="tournament", orphanRemoval=true)
	public List<Pick> pick;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<Pick> getPick() {
		return pick;
	}

	public void setPick(List<Pick> pick) {
		this.pick = pick;
	}
}
