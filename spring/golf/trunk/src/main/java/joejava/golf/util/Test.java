package joejava.golf.util;

import joejava.golf.domain.Golfer;
import joejava.golf.service.GolferService;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("TEST");
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext.xml");
	
		/*
		GolferService golferService = (GolferService)con.getBean("golferServiceImpl");
		Golfer golfer = golferService.findGolferById(22);
		
		System.out.println(golfer.getHandicap());
		*/
	}
}
