/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.dao.GolferDAO;
import joejava.golf.domain.Golfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@Service
public class GolferServiceImpl implements GolferService {

	@Autowired
	private GolferDAO golferDAO;
	
	@Transactional
	public void addGolfer(Golfer golfer) {
		golferDAO.save(golfer);
	}

	@Transactional
	public List<Golfer> listGolfer() {
		return golferDAO.list();
	}

	@Transactional
	public void deleteGolfer(Golfer golfer) {
		golferDAO.delete(golfer);
	}
	
	@Transactional
	public Golfer findGolferById(Integer id){
		return (Golfer) golferDAO.findById(id);
	}

	@Transactional
	public Golfer findFirstGolfer() {
		return golferDAO.findFirstGolfer();
	}
}
