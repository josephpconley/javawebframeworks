/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.dao.RoundDAO;
import joejava.golf.domain.Round;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@Service
public class RoundServiceImpl implements RoundService {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private RoundDAO roundDAO;
	
	@Transactional
	public void addRound(Round round) {
		roundDAO.save(round);
	}

	@Transactional
	public List<Round> listRound() {
		return roundDAO.listRound();
	}

	@Transactional
	public void deleteRound(Integer id) {
		roundDAO.delete(id);
	}
	
	@Transactional
	public Round findRoundById(Integer id){
		return (Round) roundDAO.findById(id);
	}
}
