/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Pick;

/**
 * @author jconley
 *
 */
public interface PgaDAO extends DAO{
	public List<Pick> listPicks();
	public Pick getPickByName(String golfer);
	public Pick getPickByPgaId(Integer id);
	public List<Pick> listCutPicks();
}
