/**
 * 
 */
package joejava.golf.dao;

import joejava.golf.domain.Golfer;

/**
 * @author jconley
 *
 */
public interface GolferDAO extends DAO<Golfer>{
    public Golfer findFirstGolfer();
}
