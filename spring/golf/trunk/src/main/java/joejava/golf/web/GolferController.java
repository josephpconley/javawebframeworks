package joejava.golf.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;


import joejava.golf.domain.Golfer;
import joejava.golf.domain.Pick;
import joejava.golf.service.GolferService;
import joejava.golf.service.PgaService;
import joejava.golf.service.UploadService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

@Controller
@RequestMapping("/golfer")
public class GolferController {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private GolferService golferService;

	@Autowired
	private PgaService pgaService;
	
	@Autowired
	private UploadService uploadService;

	@RequestMapping("/list")
	public String listGolfers(Map<String, Object> map) {
		map.put("golfer", new Golfer());
		map.put("golferList", golferService.listGolfer());
		return "golfer/list";
	}

	@RequestMapping("/{id}")
	public String getGolfer(@PathVariable("id") Integer golferId,Map<String, Object> map) {
		Golfer golfer = golferService.findGolferById(golferId);
		map.put("golfer",golfer);
		return "golfer/show";
	}

	@RequestMapping("/{id}.img")
	public void getPhoto(@PathVariable("id") Integer golferId, HttpServletResponse response) throws IOException {
		Golfer golfer = golferService.findGolferById(golferId);
		response.getOutputStream().write(golfer.getPhoto());
	}	
	
	//ALWAYS redirect after DELETES/UPDATES
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addGolfer(@ModelAttribute("golfer") Golfer golfer, BindingResult result) {
		System.out.println("Adding via web");
		golferService.addGolfer(golfer);
		return "redirect:/golfer/list";
	}

	@RequestMapping(value = "/add/xml", method = RequestMethod.POST)
	public void addGolfer(@RequestBody Golfer golfer) {
		System.out.println("Adding via REST POST");
		System.out.println(golfer.toString());
		golferService.addGolfer(golfer);
	}	
	
	@RequestMapping("/delete/{id}")
	public String deleteGolfer(@PathVariable("id")	Integer golferId) {
		golferService.deleteGolfer(golferService.findGolferById(golferId));
		return "redirect:/golfer/list";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "content-type=application/pdf")
	public String addGolfer(@RequestParam("name") String name, @RequestBody MultipartFile file){
		System.out.println(name);
		System.out.println(file.getName());
		return "";
	}
	
	@RequestMapping(value = "/{id}/round/upload", method = RequestMethod.POST)
	public String addRounds(@PathVariable("id")	Integer id, @RequestParam("round") MultipartFile file) throws IOException, Exception{
		Golfer golfer = golferService.findGolferById(id);
		uploadService.convertDelimitedListToRoundList(golfer, file.getInputStream(), ",");
		return "redirect:/golfer/" + id;
	}
	
	@RequestMapping(value="/rest", method = RequestMethod.POST)
	public void addRest(@RequestBody String body){
		System.out.println(body);
	}
	
	@RequestMapping(value="/pga")
	public String listPicks(Map<String, Object> map) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		List<Pick> picks = pgaService.listPicks();
		map.put("lastUpdated", picks.get(0).getTimeUpdated());
		map.put("pickList", picks);
		
		List<Pick> cut = pgaService.listCutPicks();
		map.put("cutList", cut);
		return "golfer/pga";
	}
}
