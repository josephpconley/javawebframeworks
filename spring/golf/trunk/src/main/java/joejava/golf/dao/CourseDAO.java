/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Course;

/**
 * @author jconley
 *
 */
public interface CourseDAO extends DAO<Course> {
    public List<Course> listCourse();
    public Course findFirstCourse();
	public Course findCourseByNameContaining(String name);
	public void deleteAllCourses();
	public Course findCourseByCode(String code);

}
