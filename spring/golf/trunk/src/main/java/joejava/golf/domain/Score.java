package joejava.golf.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Score{
	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	private Round round;
	
	@ManyToOne
	private Hole hole;
	
	private Integer score;

	public Score(){}
	
	public Score(Hole hole, Integer score) {
		this.hole = hole;
		this.score = score;
	}

	public Integer getScoreByPar(){
		return this.score - this.hole.getPar();
	}
	
	public Round getRound() {
		return round;
	}

	public void setRound(Round round) {
		this.round = round;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Hole getHole() {
		return hole;
	}
	public void setHole(Hole hole) {
		this.hole = hole;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
}