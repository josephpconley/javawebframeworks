/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.domain.Round;

/**
 * @author jconley
 *
 */
public interface RoundService {
	 
    public void addRound(Round round);
    public List<Round> listRound();
    public void deleteRound(Integer id);
    public Round findRoundById(Integer id);
}
