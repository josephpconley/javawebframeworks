package joejava.golf.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import joejava.golf.domain.Pick;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

public interface PgaService {
    //Fantasy
    public Pick getPickByName(String golfer);
    public Pick getPickByPgaId(Integer id);
    public void savePick(Pick pick);
	public List<Pick> listPicks();
	public List<Pick> listCutPicks();
	public Integer getScore(String score);
	public Integer getRoundScore(String score);
	public void updatePgaScores() throws FailingHttpStatusCodeException, MalformedURLException, IOException;
}
