package joejava.golf.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Pick {
	
	@Id
	private Integer id;
	private String golfer;
	private String rank;
	private String position;
	private String startPosition;
	private String thru;
	private Integer total;
	private Integer today;
	private Integer round1;
	private Integer round2;
	private Integer round3;
	private Integer round4;
	private Integer totalScore;
	private Integer espnId;
	private Integer pgaId;

	@ManyToOne
	private Golfer owner;
	
	@ManyToOne
	private Tournament tournament;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeUpdated;
	
	@Override
	public String toString() {
		return "Pick [espnId=" + espnId + ", golfer=" + golfer + ", id=" + id
				+ ", owner=" + owner + ", pgaId=" + pgaId + ", position="
				+ position + ", rank=" + rank + ", round1=" + round1
				+ ", round2=" + round2 + ", round3=" + round3 + ", round4="
				+ round4 + ", startPosition=" + startPosition + ", thru="
				+ thru + ", today=" + today + ", total=" + total
				+ ", totalScore=" + totalScore + "]";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGolfer() {
		return golfer;
	}
	public void setGolfer(String golfer) {
		this.golfer = golfer;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public Golfer getOwner() {
		return owner;
	}
	public void setOwner(Golfer owner) {
		this.owner = owner;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getStartPosition() {
		return startPosition;
	}
	public void setStartPosition(String startPosition) {
		this.startPosition = startPosition;
	}
	public String getThru() {
		return thru;
	}
	public void setThru(String thru) {
		this.thru = thru;
	}
		public Integer getEspnId() {
		return espnId;
	}
	public void setEspnId(Integer espnId) {
		this.espnId = espnId;
	}
	public Integer getPgaId() {
		return pgaId;
	}
	public void setPgaId(Integer pgaId) {
		this.pgaId = pgaId;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getToday() {
		return today;
	}
	public void setToday(Integer today) {
		this.today = today;
	}
	public Integer getRound1() {
		return round1;
	}
	public void setRound1(Integer round1) {
		this.round1 = round1;
	}
	public Integer getRound2() {
		return round2;
	}
	public void setRound2(Integer round2) {
		this.round2 = round2;
	}
	public Integer getRound3() {
		return round3;
	}
	public void setRound3(Integer round3) {
		this.round3 = round3;
	}
	public Integer getRound4() {
		return round4;
	}
	public void setRound4(Integer round4) {
		this.round4 = round4;
	}
	public Integer getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	public Date getTimeUpdated() {
		return timeUpdated;
	}
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	public Tournament getTournament() {
		return tournament;
	}
	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}
}