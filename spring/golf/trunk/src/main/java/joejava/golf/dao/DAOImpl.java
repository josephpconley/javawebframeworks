package joejava.golf.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DAOImpl<E> implements DAO<E>{

	@Autowired
    private SessionFactory sessionFactory;
	
	protected Class<?> entityClass;
	
    public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public DAOImpl(){
    	Type type = getClass().getGenericSuperclass();
    	Class<?> c = null;
    	if(type instanceof ParameterizedType){
    		ParameterizedType pType = (ParameterizedType)type;
    		c =(Class<?>)pType.getActualTypeArguments()[0];
    	}else{
    		c =(Class<?>)type;
    	}
    	
    	System.out.println(c);
    	this.setEntityClass(c);
    }
    
    
	public void delete(E entity) {
		sessionFactory.getCurrentSession().delete(entity);
	}

	public void save(E entity) {
		sessionFactory.getCurrentSession().persist(entity);
	}

	public E findById(Serializable id) {
		return (E) sessionFactory.getCurrentSession().get(entityClass, id);
	}
	
	public List<E> list() {
		return (List<E>) sessionFactory.getCurrentSession().createQuery("from " + entityClass.getSimpleName()).list();
	}
	
	
}
