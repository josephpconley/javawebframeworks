package joejava.golf.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Tee {
	
	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	private Double rating;
	
	private Integer slope;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="tee", orphanRemoval=true)
	private Set<Hole> hole = new HashSet<Hole>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="tee")
	private Set<Round> round = new HashSet<Round>();
	
	@ManyToOne
	private Course course;
	
	public Tee(String name, Double rating, Integer slope) {
		this.name = name;
		this.rating = rating;
		this.slope = slope;
	}

	public Tee(){}
	
	public Integer getPar(){
		Integer par = 0;
		
		for(Hole h : hole){
			par += h.getPar();
		}
		
		return par;
	}
	
	public Integer getParOut(){
		int out = 0;
		for(Hole h : this.hole){
			if(h.getHoleNo() < 10){
				out += h.getPar();
			}
		}
		
		return out;
	}
	
	public Integer getParIn(){
		int in = 0;
		for(Hole h : this.hole){
			if(h.getHoleNo() > 9){
				in += h.getPar();
			}
		}	
		
		return in;
	}	
	
	public Set<Round> getRound() {
		return round;
	}

	public void setRound(Set<Round> round) {
		this.round = round;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSlope() {
		return slope;
	}

	public void setSlope(Integer slope) {
		this.slope = slope;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Hole> getHole() {
		return hole;
	}

	public void setHole(Set<Hole> hole) {
		this.hole = hole;
	}
}
