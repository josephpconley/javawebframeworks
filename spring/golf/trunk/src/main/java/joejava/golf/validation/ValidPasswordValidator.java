package joejava.golf.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidPasswordValidator implements ConstraintValidator<ValidPassword,String> {

	public void initialize(ValidPassword arg0) {
		
	}

	public boolean isValid(String password, ConstraintValidatorContext ctx) {
		boolean isValid;

		if(password == null) return false;
		isValid = password.matches("[a-zA-Z]+[0-9]+");
		
		return isValid;
	}

}
