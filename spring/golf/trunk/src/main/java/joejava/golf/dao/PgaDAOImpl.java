/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Pick;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author jconley
 * 
 */
@Repository
public class PgaDAOImpl extends DAOImpl implements PgaDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public List<Pick> listPicks() {
		return sessionFactory.getCurrentSession().createQuery("from Pick where position != 'CUT' order by total, golfer").list();
	}

	public List<Pick> listCutPicks() {
		return sessionFactory.getCurrentSession().createQuery("from Pick where position = 'CUT' order by total, golfer").list();
	}

	public Pick getPickByName(String golfer) {
		Query query = (Query) sessionFactory.getCurrentSession().createQuery("from Pick where golfer like :golfer ");
		query.setParameter("golfer", golfer);

		return (Pick) query.uniqueResult();
	}

	public Pick getPickByPgaId(Integer id) {
		return (Pick) sessionFactory.getCurrentSession().createQuery(
				"from Pick where pgaId = " + id).uniqueResult();
	}
}
