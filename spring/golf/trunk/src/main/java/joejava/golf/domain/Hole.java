package joejava.golf.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Hole {
	@Id
	@GeneratedValue
	private Integer id;
	
	private Integer holeNo;
	
	private Integer par;
	
	private Integer yardage;
	
	private Integer handicap;
	
	@ManyToOne
	private Tee tee;
	
	@OneToMany(cascade={CascadeType.ALL}, mappedBy="hole", orphanRemoval=true)
	private Set<Score> score = new HashSet<Score>();
	
	public Hole(Integer holeNo, Integer par, Integer yardage, Integer handicap){
		this.holeNo = holeNo;
		this.par = par;
		this.yardage = yardage;
		this.handicap = handicap;
	}
	
	public Hole(){}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHoleNo() {
		return holeNo;
	}

	public void setHoleNo(Integer holeNo) {
		this.holeNo = holeNo;
	}

	public Integer getPar() {
		return par;
	}

	public void setPar(Integer par) {
		this.par = par;
	}

	public Integer getYardage() {
		return yardage;
	}

	public void setYardage(Integer yardage) {
		this.yardage = yardage;
	}

	public Integer getHandicap() {
		return handicap;
	}

	public void setHandicap(Integer handicap) {
		this.handicap = handicap;
	}

	public Tee getTee() {
		return tee;
	}

	public void setTee(Tee tee) {
		this.tee = tee;
	}

	public Set<Score> getScore() {
		return score;
	}

	public void setScore(Set<Score> score) {
		this.score = score;
	}
}
