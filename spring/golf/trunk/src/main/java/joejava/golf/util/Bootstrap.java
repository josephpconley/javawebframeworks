package joejava.golf.util;

import joejava.golf.domain.Course;
import joejava.golf.domain.Golfer;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Tee;
import joejava.golf.service.CourseService;
import joejava.golf.service.GolferService;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class Bootstrap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("bootstrappin'");
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext*");
		
		GolferService golferService = (GolferService)con.getBean("golferServiceImpl");
		Golfer golfer = new Golfer();
		golfer.setName("Joe Conley");
		golfer.setNickname("Jolty");

		golferService.addGolfer(golfer);
		
		CourseService courseService = (CourseService)con.getBean("courseServiceImpl");
		Course course = new Course();
		course.setName("Paxon Hollow Country Club");
		
		//Integer holeNo, Integer par, Integer yardage, Integer handicap, Tee tee
		Tee whites = new Tee("White",66.2,121);
		whites.getHole().add(new Hole(1, 4, 319, 14));
		whites.getHole().add(new Hole(2, 3, 119, 18));
		whites.getHole().add(new Hole(3, 5, 480, 6));
		whites.getHole().add(new Hole(4, 3, 135, 16));
		whites.getHole().add(new Hole(5, 4, 317, 12));
		whites.getHole().add(new Hole(6, 4, 261, 4));
		whites.getHole().add(new Hole(7, 5, 441, 10));
		whites.getHole().add(new Hole(8, 3, 158, 8));
		whites.getHole().add(new Hole(9, 4, 363, 2));
		
		whites.getHole().add(new Hole(10, 4, 250, 15));
		whites.getHole().add(new Hole(11, 5, 430, 5));
		whites.getHole().add(new Hole(12, 4, 350, 9));
		whites.getHole().add(new Hole(13, 4, 340, 3));
		whites.getHole().add(new Hole(14, 4, 370, 7));
		whites.getHole().add(new Hole(15, 3, 150, 11));
		whites.getHole().add(new Hole(16, 4, 300, 13));
		whites.getHole().add(new Hole(17, 3, 115, 17));
		whites.getHole().add(new Hole(18, 5, 519, 1));
		
		course.getTee().add(whites);
		courseService.addCourse(course);		
	}

}
