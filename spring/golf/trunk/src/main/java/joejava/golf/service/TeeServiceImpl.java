package joejava.golf.service;

import java.util.List;

import joejava.golf.dao.TeeDAO;
import joejava.golf.domain.Tee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author jconley
 *
 */
@Service
public class TeeServiceImpl implements TeeService {

	@Autowired
	private TeeDAO teeDAO;
	
	@Transactional
	public void addTee(Tee tee) {
		teeDAO.addTee(tee);
	}

	@Transactional
	public List<Tee> listTee() {
		return teeDAO.listTee();
	}

	@Transactional
	public void deleteTee(Integer id) {
		teeDAO.deleteTee(id);
	}
	
	@Transactional
	public Tee findTeeById(Integer id){
		return teeDAO.findTeeById(id);
	}

	@Transactional
	public Tee findTeeByNameAndCourseCode(String name, String courseCode) {
		return teeDAO.findTeeByNameAndCourseCode(name,courseCode);
	}
}
