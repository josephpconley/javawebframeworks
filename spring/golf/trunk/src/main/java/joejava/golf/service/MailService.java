package joejava.golf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;

/**
 * @author jconley
 *
 */
@Service
public class MailService {
	
	@Autowired
    private transient MailSender mailTemplate;

    /**
      * Sends an email message
      * 
      * @param mailFrom	
      * @param mailTo
      * @param subject
      * @param message
     */
    public void sendMessage(String mailFrom, String mailTo, String subject, String message) {
        org.springframework.mail.SimpleMailMessage simpleMailMessage = new org.springframework.mail.SimpleMailMessage();
        simpleMailMessage.setFrom(mailFrom);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setTo(mailTo);
        simpleMailMessage.setText(message);
        
        try{
        	System.out.println("sending message ");
        	mailTemplate.send(simpleMailMessage);
        	System.out.println("message sent");
        }catch(Exception e){
        	e.printStackTrace();
        }
    }
}
