package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Tee;

/**
 * @author jconley
 *
 */
public interface TeeDAO {
	public void addTee(Tee tee);
    public List<Tee> listTee();
    public void deleteTee(Integer id);
    public Tee findTeeById(Integer id);
	public Tee findTeeByNameAndCourseCode(String name, String courseCode);

}
