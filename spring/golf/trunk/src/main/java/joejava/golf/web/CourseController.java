package joejava.golf.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import joejava.golf.domain.Course;
import joejava.golf.service.CourseService;
import joejava.golf.service.UploadService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/course")
public class CourseController {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private UploadService uploadService;	

	@RequestMapping("/list")
	public String listCourses(Map<String, Object> map) {
		log.info("Listing courses");
		List<Course> courses = courseService.listCourse();
		log.info(courses.size());

		map.put("course", new Course());
		map.put("courseList", courses);
		return "course/list";
	}

	@RequestMapping("/{id}")
	public String getCourse(@PathVariable("id") Integer courseId,Map<String, Object> map) {
		Course course = courseService.findCourseById(courseId);
		map.put("course",course);
		return "course/show";
	}
	
	//ALWAYS redirect after DELETES/UPDATES
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCourse(@ModelAttribute("course") Course course, BindingResult result) {
		System.out.println("Adding via web");
		courseService.addCourse(course);
		return "redirect:/course/list";
	}

	@RequestMapping(value = "/add/xml", method = RequestMethod.POST)
	public void addCourse(@RequestBody Course course) {
		System.out.println("Adding via REST POST");
		System.out.println(course.toString());
		courseService.addCourse(course);
	}	
	
	@RequestMapping("/delete/{id}")
	public String deleteCourse(@PathVariable("id")	Integer courseId) {
		courseService.deleteCourse(courseService.findCourseById(courseId));
		return "redirect:/course/list";
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String addCourse(@RequestParam("course") MultipartFile file) throws IOException, Exception{
		uploadService.convertDelimitedListToCourseList(file.getInputStream(), ",");
		return "redirect:/course/list";
	}
	
	@RequestMapping(value = "/tee/upload", method = RequestMethod.POST)
	public String addTee(@RequestParam("tee") MultipartFile file) throws IOException, Exception{
		uploadService.convertDelimitedListToTeeList(file.getInputStream(), ",");
		return "redirect:/course/list";
	}	
}
