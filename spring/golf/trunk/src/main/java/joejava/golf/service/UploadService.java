package joejava.golf.service;

import java.io.InputStream;

import joejava.golf.domain.Golfer;

public interface UploadService {
	/**
	  * 
	  * @param golferId
	  * @param inputStream
	  * @param delimiter
	  * @throws Exception
	 */
	public void convertDelimitedListToRoundList(Golfer golfer,InputStream inputStream, String delimiter) throws Exception;
	
	/**
	 * @param inputStream
	 * @param delimiter
	 * @throws Exception
	 */
	public void convertDelimitedListToCourseList(InputStream inputStream, String delimiter) throws Exception;

	/**
	 * @param inputStream
	 * @param string
	 * @throws Exception
	 */
	public void convertDelimitedListToTeeList(InputStream inputStream,String string) throws Exception;
}
