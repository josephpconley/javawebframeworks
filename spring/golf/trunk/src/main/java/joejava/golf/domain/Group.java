package joejava.golf.domain;

import java.util.Set;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class Group {
	@Id
	@GeneratedValue
	private Integer id;
	
	private Set<Round> rounds;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Round> getRounds() {
		return rounds;
	}

	public void setRounds(Set<Round> rounds) {
		this.rounds = rounds;
	}
	
	
}
