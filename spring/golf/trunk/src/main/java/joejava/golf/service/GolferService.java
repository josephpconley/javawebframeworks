/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.domain.Golfer;

/**
 * @author jconley
 *
 */
public interface GolferService {
	 
    public void addGolfer(Golfer golfer);
    public List<Golfer> listGolfer();
    public void deleteGolfer(Golfer golfer);
    public Golfer findGolferById(Integer id);
    public Golfer findFirstGolfer();

}
