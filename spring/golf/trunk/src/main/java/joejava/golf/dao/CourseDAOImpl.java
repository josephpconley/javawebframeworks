/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Course;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author jconley
 *
 */
@Repository
public class CourseDAOImpl extends DAOImpl<Course> implements CourseDAO { //extends HibernateDaoSupport
	
    @Autowired
    private SessionFactory sessionFactory;
 
    @SuppressWarnings("unchecked")
	public List<Course> listCourse() {
        return sessionFactory.getCurrentSession().createQuery("from Course").list();
    }
 
	public Course findFirstCourse() {
		return (Course)sessionFactory.getCurrentSession().createQuery("from Course where ID = (select min(id) from Course)").uniqueResult();
	}

	public Course findCourseByNameContaining(String name) {
		name = "'%" + name.toUpperCase() + "%'";
		return (Course)sessionFactory.getCurrentSession().createQuery("from Course where upper(name) like " + name).uniqueResult();
	}

	public void deleteAllCourses() {
		Query q = (Query) sessionFactory.getCurrentSession().createQuery("delete Course");
		q.executeUpdate();
	}

	public Course findCourseByCode(String code) {
		code = "'" + code + "'";
		return (Course)sessionFactory.getCurrentSession().createQuery("from Course where code = " + code).uniqueResult();
	}
	
}