/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Round;

/**
 * @author jconley
 *
 */
public interface RoundDAO extends DAO{
    public List<Round> listRound();
}
