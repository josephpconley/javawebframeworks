/**
 * 
 */
package joejava.golf.service;

import java.util.List;

import joejava.golf.domain.Course;

/**
 * @author jconley
 *
 */
public interface CourseService {
	 
    public void addCourse(Course course);
    public List<Course> listCourse();
    public void deleteCourse(Course course);
    public Course findCourseById(Integer id);
    public Course findFirstCourse();
	public Course findCourseByNameContaining(String name);
	public void deleteAllCourses();
	public Course findCourseByCode(String code);
}
