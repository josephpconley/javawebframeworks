package joejava.golf.util;

import java.util.Comparator;

import joejava.golf.domain.Round;

public class RoundComparator implements Comparator<Round>{
	public int compare(Round o1, Round o2) {
		Double diff1 = o1.getDifferential();
		Double diff2 = o2.getDifferential();
			
		return diff1.compareTo(diff2);			
	}
}
