package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Tee;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author jconley
 *
 */
@Repository
public class TeeDAOImpl implements TeeDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public void addTee(Tee tee) {
        sessionFactory.getCurrentSession().persist(tee);
    }
 
    public List<Tee> listTee() {
        return sessionFactory.getCurrentSession().createQuery("from Tee").list();
    }
 
    public void deleteTee(Integer id) {
        Tee tee = (Tee) sessionFactory.getCurrentSession().load(Tee.class, id);
        if (null != tee) {
            sessionFactory.getCurrentSession().delete(tee);
        }
    }

	public Tee findTeeById(Integer id) {
		return (Tee)sessionFactory.getCurrentSession().createQuery("from Tee where ID = " + id).uniqueResult();
	}

	public Tee findTeeByNameAndCourseCode(String name, String courseCode) {
		Query q = (Query) sessionFactory.getCurrentSession().createQuery("select t from Tee t where t.name = ? and t.course.code = ?");
		q.setString(0, name);
		q.setString(1,courseCode);
		return (Tee) q.uniqueResult();
	}

}
