package joejava.golf.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import joejava.golf.domain.Address;
import joejava.golf.domain.Course;
import joejava.golf.domain.Golfer;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Round;
import joejava.golf.domain.Score;
import joejava.golf.domain.Tee;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UploadServiceImpl implements UploadService {
	
	@Autowired
	private RoundService roundService;	
	
	@Autowired
	private TeeService teeService;	
	
	@Autowired
	private CourseService courseService;
	
	Logger log = Logger.getLogger(this.getClass());

	@Transactional
	public void convertDelimitedListToCourseList(InputStream inputStream, String delimiter) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		String[] headerRow = null;
		
		try {
			int count = 0;
			while ((line = br.readLine()) != null) {
				if(count == 0){
					headerRow = line.split(delimiter);
				}else{
					String[] detailRow = line.split(delimiter);
					boolean insert = false;
					
					Course course = courseService.findCourseByCode(detailRow[0]);
					if(course == null){
						course = new Course();
						insert = true;
					}
					
					course.setCode(detailRow[0]);
					course.setName(detailRow[1]);
					course.setAddress(new Address(detailRow[2],detailRow[3],detailRow[4],detailRow[5]));
					course.setPhoneNumber(detailRow[6]);
					
					if(insert){
						courseService.addCourse(course);
					}
				}
				count++;
			}
			
			inputStream.close();
		} catch (Exception e) {// Catch exception if any
			log.error("Error: " + e.getMessage());
			throw new Exception(e);
		}
	}	
	
	@Transactional
	public void convertDelimitedListToRoundList(Golfer golfer, InputStream inputStream, String delimiter) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		String[] headerRow = null;
		
		try {
			int count = 0;
			golfer.getRound().clear();
			
			while ((line = br.readLine()) != null) {
				if(count == 0){
					headerRow = line.split(delimiter);
				}else{
					String[] detailRow = line.split(delimiter);
					Round round = new Round();
					Tee tee = teeService.findTeeByNameAndCourseCode(detailRow[1],detailRow[0]);
					log.info(tee.getId());
					
					int h = 2;
					for(Hole hole : tee.getHole()){
						Score s = new Score(hole, Integer.valueOf(detailRow[h]));
						s.setRound(round);
						round.getScores().add(s);
						h++;
					}
					round.setTee(tee);
					round.setGolfer(golfer);
					
					golfer.getRound().add(round);
				}
				count++;
			}
			
			inputStream.close();
		} catch (Exception e) {// Catch exception if any
			log.error("Error: " + e.getMessage());
			throw new Exception(e);
		}
	}
	@Transactional
	public void convertDelimitedListToTeeList(InputStream inputStream,String delimiter) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		String[] headerRow = null;
		Tee tee = null;
		Course course = null;
		Hole[] hole = new Hole[18];
		boolean insert = false;
		
		try {
			int count = 0;
			while ((line = br.readLine()) != null) {
				if(count == 0){
					headerRow = line.split(delimiter);
				}else{
					String[] detailRow = line.split(delimiter);
					
					if(count % 3 == 1){
						tee = null;
						insert = false;
						course = courseService.findCourseByCode(detailRow[0]);
						for(Tee t : course.getTee()){
							if(t.getName().toUpperCase().equals(detailRow[1])){
								tee = t;
								course.getTee().remove(t);
							}
						}
						
						if(tee == null){
							tee = new Tee();
						}
						
						tee.setName(detailRow[1]);
						tee.setRating(Double.valueOf(detailRow[2]));
						tee.setSlope(Integer.valueOf(detailRow[3]));
						
						tee.getHole().clear();
						for(int h=0;h<18;h++){
							hole[h] = new Hole();
							hole[h].setYardage(Integer.valueOf(detailRow[h+4]));
							hole[h].setHoleNo(h+1);
						}
						
					}
					else if(count % 3 == 2){
						for(int h=0;h<18;h++){
							hole[h].setHandicap(Integer.valueOf(detailRow[h+4]));
						}						
					}
					else if(count % 3 == 0){
						for(int h=0;h<18;h++){
							hole[h].setPar(Integer.valueOf(detailRow[h+4]));
							
							hole[h].setTee(tee);
							tee.getHole().add(hole[h]);
						}

						tee.setCourse(course);
						course.getTee().add(tee);
					}
				}
				count++;
			}
			
			inputStream.close();
		} catch (Exception e) {// Catch exception if any
			log.error("Error: " + e.getMessage());
			throw new Exception(e);
		}

	}
}
