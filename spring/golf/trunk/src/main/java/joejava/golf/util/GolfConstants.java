package joejava.golf.util;

public class GolfConstants {
	public static final Double SLOPE_NUMERATOR = 113.0;
	public static final Double HANDICAP_FACTOR = 0.96;
}
