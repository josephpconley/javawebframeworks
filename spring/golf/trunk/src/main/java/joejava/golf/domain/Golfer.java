package joejava.golf.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import joejava.golf.util.GolfConstants;
import joejava.golf.util.RoundComparator;

import org.hibernate.validator.constraints.Email;

@Entity
public class Golfer {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@Min(18)
	private Integer age;

	@Size(min=6)
	private String username;
	
	//@ValidPassword
	private String password;
	
	@NotNull
	private String name;
	
	private String nickname;
	
	@Email
	private String email;

	@Past
	private Date birthDate;
	
	@OneToMany(cascade={CascadeType.ALL}, mappedBy="golfer", orphanRemoval=true)
	private Set<Round> round = new HashSet<Round>();
	
	private byte[] photo;
	
	public Golfer(){}
	
	public boolean equals(Golfer golfer){
		if(this.id == golfer.getId()){
			return true;
		}
		return false;
	}
	
	@Transient
	public Double getHandicap(){
		Double handicap = null;
		double diff = 0;
		int minRounds = this.getDiffCount().intValue();
		
		ArrayList<Round> roundList = new ArrayList<Round>(round); 
		Collections.sort(roundList, new RoundComparator() );
		
		if(this.round.size() > 4){
			int i = 0;
			while(minRounds > 0){
				Round r = (Round)roundList.get(i);
				diff += r.getDifferential();
				minRounds--;
				i++;
			}
			
			handicap = (diff/this.getDiffCount()) * GolfConstants.HANDICAP_FACTOR;
		}
		
		return handicap;
	}

	@Transient
	public Integer getDiffCount(){
		int minCount = -1;
		
		if(this.round.size() == 5 || this.round.size() == 6){
			minCount = 1;
		}
		else if(this.round.size() == 7 || this.round.size() == 8){
			minCount = 2;
		}		
		else if(this.round.size() == 9 || this.round.size() == 10){
			minCount = 3;
		}		
		else if(this.round.size() == 11 || this.round.size() == 12){
			minCount = 4;
		}		
		else if(this.round.size() == 13 || this.round.size() == 14){
			minCount = 5;
		}		
		else if(this.round.size() == 15 || this.round.size() == 16){
			minCount = 6;
		}		
		else if(this.round.size() > 16 || this.round.size() < 21){
			minCount = this.round.size() - 10;
		}				
		else if(this.round.size() > 20){
			minCount = 10;
		}
		
		return minCount;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Round> getRound() {
		return round;
	}

	public void setRound(Set<Round> round) {
		this.round = round;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
}
