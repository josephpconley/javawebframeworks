/**
 * 
 */
package joejava.golf.dao;

import joejava.golf.domain.Golfer;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author jconley
 *
 */
@Repository
public class GolferDAOImpl extends DAOImpl<Golfer> implements GolferDAO {
	
    @Autowired
    private SessionFactory sessionFactory;
 
	public Golfer findFirstGolfer() {
		return (Golfer)sessionFactory.getCurrentSession().createQuery("from Golfer where ID = (select min(id) from Golfer)").uniqueResult();
	}
}
