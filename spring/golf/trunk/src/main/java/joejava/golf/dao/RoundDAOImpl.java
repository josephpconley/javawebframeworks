/**
 * 
 */
package joejava.golf.dao;

import java.util.List;

import joejava.golf.domain.Round;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author jconley
 *
 */
@Repository
public class RoundDAOImpl extends DAOImpl implements RoundDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public List<Round> listRound() {
        return sessionFactory.getCurrentSession().createQuery("from Round order by id desc").list();
    }
}
