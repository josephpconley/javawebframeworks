package joejava.golf.web;

import java.util.List;
import java.util.Map;

import joejava.golf.domain.Round;
import joejava.golf.service.RoundService;
import joejava.golf.service.UploadService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/round")
public class RoundController {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private RoundService roundService;
	
	@Autowired
	private UploadService uploadService;	

	@RequestMapping("/list")
	public String listRounds(Map<String, Object> map) {
		log.info("Listing rounds");
		List<Round> rounds = roundService.listRound();
		log.info(rounds.size());

		map.put("round", new Round());
		map.put("roundList", rounds);
		return "round/list";
	}

	@RequestMapping("/{id}")
	public String getRound(@PathVariable("id") Integer roundId,Map<String, Object> map) {
		Round round = roundService.findRoundById(roundId);
		map.put("round",round);
		return "round/show";
	}
	
	//ALWAYS redirect after DELETES/UPDATES
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addRound(@ModelAttribute("round") Round round, BindingResult result) {
		System.out.println("Adding via web");
		roundService.addRound(round);
		return "redirect:/round/list";
	}

	@RequestMapping(value = "/add/xml", method = RequestMethod.POST)
	public void addRound(@RequestBody Round round) {
		System.out.println("Adding via REST POST");
		System.out.println(round.toString());
		roundService.addRound(round);
	}	
	
	@RequestMapping("/delete/{id}")
	public String deleteRound(@PathVariable("id")	Integer roundId) {
		roundService.deleteRound(roundId);
		return "redirect:/round/list";
	}
}
