TODO
	fix the profile page to add photo, more professional css style
	functionality to add a round (other than CSV destructive update, which should stay hidden)
	android app to keep score for you while you play
	figure out how to sort children of an entity (namely, sort a golfer's rounds by date)
	fix session bug
	setup chat/pick functionality for everyone to simply logon to chat/pick instead of fbook/manual
	build out rest of fantasy app with a page explaining rules
	download individual scores/course info for every pga tournament, do metrics
		WHAT METRICS?
			New World Rank calculation
			compare hard stats to betting odds, weed out discrepancies
			forecasting for marketing copmanies, pick out up-and-comers			
	
DONE
	domain model to hold course information for multiple tees and store a round of scores
	calculating one's handicap based on these scores
	
	storing picks for a major championship in a database
	downloading information from a majorchampionship website to update scores