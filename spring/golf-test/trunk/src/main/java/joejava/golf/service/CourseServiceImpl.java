package joejava.golf.service;

import java.util.Set;

import joejava.golf.dao.CourseDAO;
import joejava.golf.dao.TeeDAO;

import joejava.golf.domain.Course;
import joejava.golf.domain.Tee;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Course entities
 * 
 */

@Service("CourseService")
@Transactional
public class CourseServiceImpl implements CourseService {

	/**
	 * DAO injected by Spring that manages Course entities
	 * 
	 */
	@Autowired
	private CourseDAO courseDAO;

	/**
	 * DAO injected by Spring that manages Tee entities
	 * 
	 */
	@Autowired
	private TeeDAO teeDAO;

	/**
	 * Instantiates a new CourseServiceImpl.
	 *
	 */
	public CourseServiceImpl() {
	}

	/**
	 * Delete an existing Course entity
	 * 
	 */
	@Transactional
	public void deleteCourse(Course course) {
		courseDAO.remove(course);
		courseDAO.flush();
	}

	/**
	 * Save an existing Course entity
	 * 
	 */
	@Transactional
	public void saveCourse(Course course) {
		Course existingCourse = courseDAO.findCourseByPrimaryKey(course.getId());

		if (existingCourse != null) {
			existingCourse.setId(course.getId());
			existingCourse.setName(course.getName());
			course = courseDAO.store(existingCourse);
		} else {
			course = courseDAO.store(course);
		}
		courseDAO.flush();
	}

	/**
	 * Save an existing Tee entity
	 * 
	 */
	@Transactional
	public Course saveCourseTees(Integer id, Tee tee) {
		Course course = courseDAO.findCourseByPrimaryKey(id, -1, -1);
		Tee existingTee = teeDAO.findTeeByPrimaryKey(tee.getId());

		// copy into the existing record to preserve existing relationships
		if (existingTee != null) {
			existingTee.setId(tee.getId());
			existingTee.setTeeType(tee.getTeeType());
			existingTee.setRating(tee.getRating());
			existingTee.setSlope(tee.getSlope());
			tee = existingTee;
		} else {
			tee = teeDAO.store(tee);
			teeDAO.flush();
		}

		tee.setCourse(course);
		course.getTees().add(tee);
		tee = teeDAO.store(tee);
		teeDAO.flush();

		course = courseDAO.store(course);
		courseDAO.flush();

		return course;
	}

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	@Transactional
	public Course deleteCourseTees(Integer tee_id, Integer course_id) {
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);
		Course course = courseDAO.findCourseByPrimaryKey(course_id, -1, -1);

		tee.setCourse(null);
		course.getTees().remove(tee);
		tee = teeDAO.store(tee);
		teeDAO.flush();

		course = courseDAO.store(course);
		courseDAO.flush();

		teeDAO.remove(tee);
		teeDAO.flush();

		return course;
	}

	/**
	 * Load an existing Course entity
	 * 
	 */
	@Transactional
	public Set<Course> loadCourses() {
		return courseDAO.findAllCourses();
	}
}
