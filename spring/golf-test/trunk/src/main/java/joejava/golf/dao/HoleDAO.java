package joejava.golf.dao;

import java.util.Set;

import joejava.golf.domain.Hole;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Hole entities.
 * 
 */
public interface HoleDAO extends JpaDao {

	/**
	 * JPQL Query - findAllHoles
	 *
	 */
	public Set<Hole> findAllHoles() throws DataAccessException;

	/**
	 * JPQL Query - findAllHoles
	 *
	 */
	public Set<Hole> findAllHoles(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findHoleByPrimaryKey
	 *
	 */
	public Hole findHoleByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findHoleByPrimaryKey
	 *
	 */
	public Hole findHoleByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

}