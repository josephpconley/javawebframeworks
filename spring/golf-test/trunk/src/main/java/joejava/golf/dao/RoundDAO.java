package joejava.golf.dao;

import java.util.Set;

import joejava.golf.domain.Round;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Round entities.
 * 
 */
public interface RoundDAO extends JpaDao {

	/**
	 * JPQL Query - findRoundByPrimaryKey
	 *
	 */
	public Round findRoundByPrimaryKey(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findRoundByPrimaryKey
	 *
	 */
	public Round findRoundByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllRounds
	 *
	 */
	public Set<Round> findAllRounds() throws DataAccessException;

	/**
	 * JPQL Query - findAllRounds
	 *
	 */
	public Set<Round> findAllRounds(int startResult, int maxRows) throws DataAccessException;

}