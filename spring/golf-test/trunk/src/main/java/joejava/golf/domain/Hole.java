package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries( {
		@NamedQuery(name = "findAllHoles", query = "select myHole from Hole myHole"),
		@NamedQuery(name = "findHoleByPrimaryKey", query = "select myHole from Hole myHole where myHole.id = ?1") })
@Table(schema = "GOLF", name = "HOLE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Hole")
@XmlRootElement(namespace = "golf/joejava/golf/domain")
public class Hole implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ID", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "HOLE_NO")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer holeNo;
	/**
	 */

	@Column(name = "PAR")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer par;
	/**
	 */

	@Column(name = "YARDAGE")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer yardage;
	/**
	 */

	@Column(name = "HANDICAP")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer handicap;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "TEE_ID", referencedColumnName = "ID") })
	@XmlTransient
	Tee tee;
	/**
	 */
	@OneToMany(mappedBy = "hole", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Score> scores;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setHoleNo(Integer holeNo) {
		this.holeNo = holeNo;
	}

	/**
	 */
	public Integer getHoleNo() {
		return this.holeNo;
	}

	/**
	 */
	public void setPar(Integer par) {
		this.par = par;
	}

	/**
	 */
	public Integer getPar() {
		return this.par;
	}

	/**
	 */
	public void setYardage(Integer yardage) {
		this.yardage = yardage;
	}

	/**
	 */
	public Integer getYardage() {
		return this.yardage;
	}

	/**
	 */
	public void setHandicap(Integer handicap) {
		this.handicap = handicap;
	}

	/**
	 */
	public Integer getHandicap() {
		return this.handicap;
	}

	/**
	 */
	public void setTee(Tee tee) {
		this.tee = tee;
	}

	/**
	 */
	public Tee getTee() {
		return tee;
	}

	/**
	 */
	public void setScores(Set<Score> scores) {
		this.scores = scores;
	}

	/**
	 */
	public Set<Score> getScores() {
		if (scores == null) {
			scores = new java.util.LinkedHashSet<joejava.golf.domain.Score>();
		}
		return scores;
	}

	/**
	 */
	public Hole() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Hole that) {
		setId(that.getId());
		setHoleNo(that.getHoleNo());
		setPar(that.getPar());
		setYardage(that.getYardage());
		setHandicap(that.getHandicap());
		setTee(that.getTee());
		setScores(new java.util.LinkedHashSet<joejava.golf.domain.Score>(that.getScores()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("holeNo=[").append(holeNo).append("] ");
		buffer.append("par=[").append(par).append("] ");
		buffer.append("yardage=[").append(yardage).append("] ");
		buffer.append("handicap=[").append(handicap).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Hole))
			return false;
		Hole equalCheck = (Hole) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
