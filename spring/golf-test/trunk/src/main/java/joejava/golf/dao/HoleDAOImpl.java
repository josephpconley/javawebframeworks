package joejava.golf.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import joejava.golf.domain.Hole;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Hole entities.
 * 
 */
@Repository("HoleDAO")
@Transactional
public class HoleDAOImpl extends AbstractJpaDao implements HoleDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Hole.class }));

	/**
	 * EntityManager injected by Spring for persistence unit joetomcat_no_ip_org
	 *
	 */
	@PersistenceContext(unitName = "joetomcat_no_ip_org")
	private EntityManager entityManager;

	/**
	 * Instantiates a new HoleDAOImpl
	 *
	 */
	public HoleDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit joetomcat_no_ip_org
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllHoles
	 *
	 */
	@Transactional
	public Set<Hole> findAllHoles() throws DataAccessException {

		return findAllHoles(-1, -1);
	}

	/**
	 * JPQL Query - findAllHoles
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Hole> findAllHoles(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllHoles", startResult, maxRows);
		return new LinkedHashSet<Hole>(query.getResultList());
	}

	/**
	 * JPQL Query - findHoleByPrimaryKey
	 *
	 */
	@Transactional
	public Hole findHoleByPrimaryKey(Integer id) throws DataAccessException {

		return findHoleByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findHoleByPrimaryKey
	 *
	 */

	@Transactional
	public Hole findHoleByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findHoleByPrimaryKey", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

}
