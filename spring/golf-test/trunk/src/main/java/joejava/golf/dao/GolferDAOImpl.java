package joejava.golf.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import joejava.golf.domain.Golfer;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Golfer entities.
 * 
 */
@Repository("GolferDAO")
@Transactional
public class GolferDAOImpl extends AbstractJpaDao implements GolferDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Golfer.class }));

	/**
	 * EntityManager injected by Spring for persistence unit joetomcat_no_ip_org
	 *
	 */
	@PersistenceContext(unitName = "joetomcat_no_ip_org")
	private EntityManager entityManager;

	/**
	 * Instantiates a new GolferDAOImpl
	 *
	 */
	public GolferDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit joetomcat_no_ip_org
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

}
