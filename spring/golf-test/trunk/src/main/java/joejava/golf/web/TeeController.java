package joejava.golf.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import joejava.golf.dao.CourseDAO;
import joejava.golf.dao.HoleDAO;
import joejava.golf.dao.RoundDAO;
import joejava.golf.dao.TeeDAO;

import joejava.golf.domain.Course;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Round;
import joejava.golf.domain.Tee;

import joejava.golf.service.TeeService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.ModelAndView;

/**
 * Spring MVC controller that handles CRUD requests for Tee entities
 * 
 */

@Controller("TeeController")
public class TeeController {

	/**
	 * DAO injected by Spring that manages Course entities
	 * 
	 */
	@Autowired
	private CourseDAO courseDAO;

	/**
	 * DAO injected by Spring that manages Hole entities
	 * 
	 */
	@Autowired
	private HoleDAO holeDAO;

	/**
	 * DAO injected by Spring that manages Round entities
	 * 
	 */
	@Autowired
	private RoundDAO roundDAO;

	/**
	 * DAO injected by Spring that manages Tee entities
	 * 
	 */
	@Autowired
	private TeeDAO teeDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Tee entities
	 * 
	 */
	@Autowired
	private TeeService teeService;

	/**
	 * Select the child Round entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteTeeRounds")
	public ModelAndView confirmDeleteTeeRounds(@RequestParam Integer tee_id, @RequestParam Integer round_id) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("round", roundDAO.findRoundByPrimaryKey(round_id));
		mav.addObject("tee_id", tee_id);
		mav.setViewName("tee/rounds/deleteRounds.jsp");

		return mav;
	}

	/**
	 * Delete an existing Hole entity
	 * 
	 */
	@RequestMapping("/deleteTeeHoles")
	public ModelAndView deleteTeeHoles(@RequestParam Integer tee_id, @RequestParam Integer hole_id) {
		ModelAndView mav = new ModelAndView();

		Tee tee = teeService.deleteTeeHoles(hole_id, tee_id);

		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * Edit an existing Course entity
	 * 
	 */
	@RequestMapping("/editTeeCourse")
	public ModelAndView editTeeCourse(@RequestParam Integer tee_id, @RequestParam Integer course_id) {
		Course course = courseDAO.findCourseByPrimaryKey(course_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("course", course);
		mav.setViewName("tee/course/editCourse.jsp");

		return mav;
	}

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	 * Create a new Round entity
	 * 
	 */
	@RequestMapping("/newTeeRounds")
	public ModelAndView newTeeRounds(@RequestParam Integer tee_id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("round", new Round());
		mav.addObject("newFlag", true);
		mav.setViewName("tee/rounds/editRounds.jsp");

		return mav;
	}

	/**
	 * View an existing Hole entity
	 * 
	 */
	@RequestMapping("/selectTeeHoles")
	public ModelAndView selectTeeHoles(@RequestParam Integer tee_id, @RequestParam Integer hole_id) {
		Hole hole = holeDAO.findHoleByPrimaryKey(hole_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("hole", hole);
		mav.setViewName("tee/holes/viewHoles.jsp");

		return mav;
	}

	/**
	 * Create a new Course entity
	 * 
	 */
	@RequestMapping("/newTeeCourse")
	public ModelAndView newTeeCourse(@RequestParam Integer tee_id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("course", new Course());
		mav.addObject("newFlag", true);
		mav.setViewName("tee/course/editCourse.jsp");

		return mav;
	}

	/**
	 * Create a new Hole entity
	 * 
	 */
	@RequestMapping("/newTeeHoles")
	public ModelAndView newTeeHoles(@RequestParam Integer tee_id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("hole", new Hole());
		mav.addObject("newFlag", true);
		mav.setViewName("tee/holes/editHoles.jsp");

		return mav;
	}

	/**
	 * Save an existing Hole entity
	 * 
	 */
	@RequestMapping("/saveTeeHoles")
	public ModelAndView saveTeeHoles(@RequestParam Integer tee_id, @ModelAttribute Hole hole) {
		Tee tee = teeService.saveTeeHoles(tee_id, hole);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * View an existing Course entity
	 * 
	 */
	@RequestMapping("/selectTeeCourse")
	public ModelAndView selectTeeCourse(@RequestParam Integer tee_id, @RequestParam Integer course_id) {
		Course course = courseDAO.findCourseByPrimaryKey(course_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("course", course);
		mav.setViewName("tee/course/viewCourse.jsp");

		return mav;
	}

	/**
	 * Edit an existing Tee entity
	 * 
	 */
	@RequestMapping("/editTee")
	public ModelAndView editTee(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/editTee.jsp");

		return mav;
	}

	/**
	 * Delete an existing Course entity
	 * 
	 */
	@RequestMapping("/deleteTeeCourse")
	public ModelAndView deleteTeeCourse(@RequestParam Integer tee_id, @RequestParam Integer course_id) {
		ModelAndView mav = new ModelAndView();

		Tee tee = teeService.deleteTeeCourse(tee_id, course_id);

		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * Select an existing Tee entity
	 * 
	 */
	@RequestMapping("/selectTee")
	public ModelAndView selectTee(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * Edit an existing Hole entity
	 * 
	 */
	@RequestMapping("/editTeeHoles")
	public ModelAndView editTeeHoles(@RequestParam Integer tee_id, @RequestParam Integer hole_id) {
		Hole hole = holeDAO.findHoleByPrimaryKey(hole_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("hole", hole);
		mav.setViewName("tee/holes/editHoles.jsp");

		return mav;
	}

	/**
	 * Edit an existing Round entity
	 * 
	 */
	@RequestMapping("/editTeeRounds")
	public ModelAndView editTeeRounds(@RequestParam Integer tee_id, @RequestParam Integer round_id) {
		Round round = roundDAO.findRoundByPrimaryKey(round_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("round", round);
		mav.setViewName("tee/rounds/editRounds.jsp");

		return mav;
	}

	/**
	 * Create a new Tee entity
	 * 
	 */
	@RequestMapping("/newTee")
	public ModelAndView newTee() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", new Tee());
		mav.addObject("newFlag", true);
		mav.setViewName("tee/editTee.jsp");

		return mav;
	}

	/**
	 * Save an existing Tee entity
	 * 
	 */
	@RequestMapping("/saveTee")
	public String saveTee(@ModelAttribute Tee tee) {
		teeService.saveTee(tee);
		return "forward:/indexTee";
	}

	/**
	 * Delete an existing Round entity
	 * 
	 */
	@RequestMapping("/deleteTeeRounds")
	public ModelAndView deleteTeeRounds(@RequestParam Integer tee_id, @RequestParam Integer round_id) {
		ModelAndView mav = new ModelAndView();

		Tee tee = teeService.deleteTeeRounds(round_id, tee_id);

		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * Entry point to show all Tee entities
	 * 
	 */
	public String indexTee() {
		return "redirect:/indexTee";
	}

	/**
	 * View an existing Round entity
	 * 
	 */
	@RequestMapping("/selectTeeRounds")
	public ModelAndView selectTeeRounds(@RequestParam Integer tee_id, @RequestParam Integer round_id) {
		Round round = roundDAO.findRoundByPrimaryKey(round_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("round", round);
		mav.setViewName("tee/rounds/viewRounds.jsp");

		return mav;
	}

	/**
	 * Show all Course entities by Tee
	 * 
	 */
	@RequestMapping("/listTeeCourse")
	public ModelAndView listTeeCourse(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/course/listCourse.jsp");

		return mav;
	}

	/**
	 * Save an existing Round entity
	 * 
	 */
	@RequestMapping("/saveTeeRounds")
	public ModelAndView saveTeeRounds(@RequestParam Integer tee_id, @ModelAttribute Round round) {
		Tee tee = teeService.saveTeeRounds(tee_id, round);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 * Select the child Hole entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteTeeHoles")
	public ModelAndView confirmDeleteTeeHoles(@RequestParam Integer tee_id, @RequestParam Integer hole_id) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("hole", holeDAO.findHoleByPrimaryKey(hole_id));
		mav.addObject("tee_id", tee_id);
		mav.setViewName("tee/holes/deleteHoles.jsp");

		return mav;
	}

	/**
	 * Select the child Course entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteTeeCourse")
	public ModelAndView confirmDeleteTeeCourse(@RequestParam Integer tee_id, @RequestParam Integer course_id) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", courseDAO.findCourseByPrimaryKey(course_id));
		mav.addObject("tee_id", tee_id);
		mav.setViewName("tee/course/deleteCourse.jsp");

		return mav;
	}

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	@RequestMapping("/deleteTee")
	public String deleteTee(@RequestParam Integer idKey) {
		Tee tee = teeDAO.findTeeByPrimaryKey(idKey);
		teeService.deleteTee(tee);
		return "forward:/indexTee";
	}

	/**
	 * Show all Round entities by Tee
	 * 
	 */
	@RequestMapping("/listTeeRounds")
	public ModelAndView listTeeRounds(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/rounds/listRounds.jsp");

		return mav;
	}

	/**
	 * Show all Hole entities by Tee
	 * 
	 */
	@RequestMapping("/listTeeHoles")
	public ModelAndView listTeeHoles(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/holes/listHoles.jsp");

		return mav;
	}

	/**
	 * Select the Tee entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteTee")
	public ModelAndView confirmDeleteTee(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(idKey));
		mav.setViewName("tee/deleteTee.jsp");

		return mav;
	}

	/**
	 * Save an existing Course entity
	 * 
	 */
	@RequestMapping("/saveTeeCourse")
	public ModelAndView saveTeeCourse(@RequestParam Integer tee_id, @ModelAttribute Course course) {
		Tee tee = teeService.saveTeeCourse(tee_id, course);

		ModelAndView mav = new ModelAndView();
		mav.addObject("tee_id", tee_id);
		mav.addObject("tee", tee);
		mav.setViewName("tee/viewTee.jsp");

		return mav;
	}

	/**
	 */
	@RequestMapping("/teeController/binary.action")
	public ModelAndView streamBinary(@ModelAttribute HttpServletRequest request, @ModelAttribute HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("streamedBinaryContentView");
		return mav;

	}

	/**
	 * Show all Tee entities
	 * 
	 */
	@RequestMapping("/indexTee")
	public ModelAndView listTees() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tees", teeService.loadTees());

		mav.setViewName("tee/listTees.jsp");

		return mav;
	}
}