package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries( {
		@NamedQuery(name = "findAllRounds", query = "select myRound from Round myRound"),
		@NamedQuery(name = "findRoundByPrimaryKey", query = "select myRound from Round myRound where myRound.id = ?1") })
@Table(schema = "GOLF", name = "ROUND")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Round")
@XmlRootElement(namespace = "golf/joejava/golf/domain")
public class Round implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ID", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "DIFFERENTIAL")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer differential;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE_PLAYED")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Calendar datePlayed;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "GOLFER_ID", referencedColumnName = "ID") })
	@XmlTransient
	Golfer golfer;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "TEE_ID", referencedColumnName = "ID") })
	@XmlTransient
	Tee tee;
	/**
	 */
	@OneToMany(mappedBy = "round", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Score> scores;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setDifferential(Integer differential) {
		this.differential = differential;
	}

	/**
	 */
	public Integer getDifferential() {
		return this.differential;
	}

	/**
	 */
	public void setDatePlayed(Calendar datePlayed) {
		this.datePlayed = datePlayed;
	}

	/**
	 */
	public Calendar getDatePlayed() {
		return this.datePlayed;
	}

	/**
	 */
	public void setGolfer(Golfer golfer) {
		this.golfer = golfer;
	}

	/**
	 */
	public Golfer getGolfer() {
		return golfer;
	}

	/**
	 */
	public void setTee(Tee tee) {
		this.tee = tee;
	}

	/**
	 */
	public Tee getTee() {
		return tee;
	}

	/**
	 */
	public void setScores(Set<Score> scores) {
		this.scores = scores;
	}

	/**
	 */
	public Set<Score> getScores() {
		if (scores == null) {
			scores = new java.util.LinkedHashSet<joejava.golf.domain.Score>();
		}
		return scores;
	}

	/**
	 */
	public Round() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Round that) {
		setId(that.getId());
		setDifferential(that.getDifferential());
		setDatePlayed(that.getDatePlayed());
		setGolfer(that.getGolfer());
		setTee(that.getTee());
		setScores(new java.util.LinkedHashSet<joejava.golf.domain.Score>(that.getScores()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("differential=[").append(differential).append("] ");
		buffer.append("datePlayed=[").append(datePlayed).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Round))
			return false;
		Round equalCheck = (Round) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
