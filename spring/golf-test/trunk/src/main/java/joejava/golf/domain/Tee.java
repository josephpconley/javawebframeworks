package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries( {
		@NamedQuery(name = "findAllTees", query = "select myTee from Tee myTee"),
		@NamedQuery(name = "findTeeById", query = "select myTee from Tee myTee where myTee.id = ?1"),
		@NamedQuery(name = "findTeeByPrimaryKey", query = "select myTee from Tee myTee where myTee.id = ?1"),
		@NamedQuery(name = "findTeeByRating", query = "select myTee from Tee myTee where myTee.rating = ?1"),
		@NamedQuery(name = "findTeeBySlope", query = "select myTee from Tee myTee where myTee.slope = ?1"),
		@NamedQuery(name = "findTeeByTeeType", query = "select myTee from Tee myTee where myTee.teeType = ?1"),
		@NamedQuery(name = "findTeeByTeeTypeContaining", query = "select myTee from Tee myTee where myTee.teeType like ?1") })
@Table(schema = "GOLF", name = "TEE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Tee")
@XmlRootElement(namespace = "golf/joejava/golf/domain")
public class Tee implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ID", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "TEE_TYPE", length = 20)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String teeType;
	/**
	 */

	@Column(name = "RATING")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer rating;
	/**
	 */

	@Column(name = "SLOPE")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer slope;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "COURSE_ID", referencedColumnName = "ID") })
	@XmlTransient
	Course course;
	/**
	 */
	@OneToMany(mappedBy = "tee", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Round> rounds;
	/**
	 */
	@OneToMany(mappedBy = "tee", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Hole> holes;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setTeeType(String teeType) {
		this.teeType = teeType;
	}

	/**
	 */
	public String getTeeType() {
		return this.teeType;
	}

	/**
	 */
	public void setRating(Integer rating) {
		this.rating = rating;
	}

	/**
	 */
	public Integer getRating() {
		return this.rating;
	}

	/**
	 */
	public void setSlope(Integer slope) {
		this.slope = slope;
	}

	/**
	 */
	public Integer getSlope() {
		return this.slope;
	}

	/**
	 */
	public void setCourse(Course course) {
		this.course = course;
	}

	/**
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 */
	public void setRounds(Set<Round> rounds) {
		this.rounds = rounds;
	}

	/**
	 */
	public Set<Round> getRounds() {
		if (rounds == null) {
			rounds = new java.util.LinkedHashSet<joejava.golf.domain.Round>();
		}
		return rounds;
	}

	/**
	 */
	public void setHoles(Set<Hole> holes) {
		this.holes = holes;
	}

	/**
	 */
	public Set<Hole> getHoles() {
		if (holes == null) {
			holes = new java.util.LinkedHashSet<joejava.golf.domain.Hole>();
		}
		return holes;
	}

	/**
	 */
	public Tee() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Tee that) {
		setId(that.getId());
		setTeeType(that.getTeeType());
		setRating(that.getRating());
		setSlope(that.getSlope());
		setCourse(that.getCourse());
		setRounds(new java.util.LinkedHashSet<joejava.golf.domain.Round>(that.getRounds()));
		setHoles(new java.util.LinkedHashSet<joejava.golf.domain.Hole>(that.getHoles()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("teeType=[").append(teeType).append("] ");
		buffer.append("rating=[").append(rating).append("] ");
		buffer.append("slope=[").append(slope).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Tee))
			return false;
		Tee equalCheck = (Tee) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
