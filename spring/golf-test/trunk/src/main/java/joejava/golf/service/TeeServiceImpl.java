package joejava.golf.service;

import java.util.Set;

import joejava.golf.dao.CourseDAO;
import joejava.golf.dao.HoleDAO;
import joejava.golf.dao.RoundDAO;
import joejava.golf.dao.TeeDAO;

import joejava.golf.domain.Course;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Round;
import joejava.golf.domain.Tee;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * Spring service that handles CRUD requests for Tee entities
 * 
 */

@Service("TeeService")
@Transactional
public class TeeServiceImpl implements TeeService {

	/**
	 * DAO injected by Spring that manages Course entities
	 * 
	 */
	@Autowired
	private CourseDAO courseDAO;

	/**
	 * DAO injected by Spring that manages Hole entities
	 * 
	 */
	@Autowired
	private HoleDAO holeDAO;

	/**
	 * DAO injected by Spring that manages Round entities
	 * 
	 */
	@Autowired
	private RoundDAO roundDAO;

	/**
	 * DAO injected by Spring that manages Tee entities
	 * 
	 */
	@Autowired
	private TeeDAO teeDAO;

	/**
	 * Instantiates a new TeeServiceImpl.
	 *
	 */
	public TeeServiceImpl() {
	}

	/**
	 * Save an existing Round entity
	 * 
	 */
	@Transactional
	public Tee saveTeeRounds(Integer id, Round round) {
		Tee tee = teeDAO.findTeeByPrimaryKey(id, -1, -1);
		Round existingRound = roundDAO.findRoundByPrimaryKey(round.getId());

		// copy into the existing record to preserve existing relationships
		if (existingRound != null) {
			existingRound.setId(round.getId());
			existingRound.setDifferential(round.getDifferential());
			existingRound.setDatePlayed(round.getDatePlayed());
			round = existingRound;
		} else {
			round = roundDAO.store(round);
			roundDAO.flush();
		}

		round.setTee(tee);
		tee.getRounds().add(round);
		round = roundDAO.store(round);
		roundDAO.flush();

		tee = teeDAO.store(tee);
		teeDAO.flush();

		return tee;
	}

	/**
	 * Save an existing Tee entity
	 * 
	 */
	@Transactional
	public void saveTee(Tee tee) {
		Tee existingTee = teeDAO.findTeeByPrimaryKey(tee.getId());

		if (existingTee != null) {
			existingTee.setId(tee.getId());
			existingTee.setTeeType(tee.getTeeType());
			existingTee.setRating(tee.getRating());
			existingTee.setSlope(tee.getSlope());
			tee = teeDAO.store(existingTee);
		} else {
			tee = teeDAO.store(tee);
		}
		teeDAO.flush();
	}

	/**
	 * Delete an existing Hole entity
	 * 
	 */
	@Transactional
	public Tee deleteTeeHoles(Integer hole_id, Integer tee_id) {
		Hole hole = holeDAO.findHoleByPrimaryKey(hole_id, -1, -1);
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);

		hole.setTee(null);
		tee.getHoles().remove(hole);
		hole = holeDAO.store(hole);
		holeDAO.flush();

		tee = teeDAO.store(tee);
		teeDAO.flush();

		holeDAO.remove(hole);
		holeDAO.flush();

		return tee;
	}

	/**
	 * Save an existing Hole entity
	 * 
	 */
	@Transactional
	public Tee saveTeeHoles(Integer id, Hole hole) {
		Tee tee = teeDAO.findTeeByPrimaryKey(id, -1, -1);
		Hole existingHole = holeDAO.findHoleByPrimaryKey(hole.getId());

		// copy into the existing record to preserve existing relationships
		if (existingHole != null) {
			existingHole.setId(hole.getId());
			existingHole.setHoleNo(hole.getHoleNo());
			existingHole.setPar(hole.getPar());
			existingHole.setYardage(hole.getYardage());
			existingHole.setHandicap(hole.getHandicap());
			hole = existingHole;
		} else {
			hole = holeDAO.store(hole);
			holeDAO.flush();
		}

		hole.setTee(tee);
		tee.getHoles().add(hole);
		hole = holeDAO.store(hole);
		holeDAO.flush();

		tee = teeDAO.store(tee);
		teeDAO.flush();

		return tee;
	}

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	@Transactional
	public void deleteTee(Tee tee) {
		teeDAO.remove(tee);
		teeDAO.flush();
	}

	/**
	 * Save an existing Course entity
	 * 
	 */
	@Transactional
	public Tee saveTeeCourse(Integer id, Course course) {
		Tee tee = teeDAO.findTeeByPrimaryKey(id, -1, -1);
		Course existingCourse = courseDAO.findCourseByPrimaryKey(course.getId());

		// copy into the existing record to preserve existing relationships
		if (existingCourse != null) {
			existingCourse.setId(course.getId());
			existingCourse.setName(course.getName());
			course = existingCourse;
		} else {
			course = courseDAO.store(course);
			courseDAO.flush();
		}

		tee.setCourse(course);
		course.getTees().add(tee);
		tee = teeDAO.store(tee);
		teeDAO.flush();

		course = courseDAO.store(course);
		courseDAO.flush();

		return tee;
	}

	/**
	 * Delete an existing Course entity
	 * 
	 */
	@Transactional
	public Tee deleteTeeCourse(Integer tee_id, Integer course_id) {
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);
		Course course = courseDAO.findCourseByPrimaryKey(course_id, -1, -1);

		tee.setCourse(null);
		course.getTees().remove(tee);
		tee = teeDAO.store(tee);
		teeDAO.flush();

		course = courseDAO.store(course);
		courseDAO.flush();

		courseDAO.remove(course);
		courseDAO.flush();

		return tee;
	}

	/**
	 * Load an existing Tee entity
	 * 
	 */
	@Transactional
	public Set<Tee> loadTees() {
		return teeDAO.findAllTees();
	}

	/**
	 * Delete an existing Round entity
	 * 
	 */
	@Transactional
	public Tee deleteTeeRounds(Integer round_id, Integer tee_id) {
		Round round = roundDAO.findRoundByPrimaryKey(round_id, -1, -1);
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);

		round.setTee(null);
		tee.getRounds().remove(round);
		round = roundDAO.store(round);
		roundDAO.flush();

		tee = teeDAO.store(tee);
		teeDAO.flush();

		roundDAO.remove(round);
		roundDAO.flush();

		return tee;
	}
}
