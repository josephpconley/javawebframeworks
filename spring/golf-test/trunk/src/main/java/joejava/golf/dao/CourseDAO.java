package joejava.golf.dao;

import java.util.Set;

import joejava.golf.domain.Course;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Course entities.
 * 
 */
public interface CourseDAO extends JpaDao {

	/**
	 * JPQL Query - findCourseById
	 *
	 */
	public Course findCourseById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findCourseById
	 *
	 */
	public Course findCourseById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findAllCourses
	 *
	 */
	public Set<Course> findAllCourses() throws DataAccessException;

	/**
	 * JPQL Query - findAllCourses
	 *
	 */
	public Set<Course> findAllCourses(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByNameContaining
	 *
	 */
	public Set<Course> findCourseByNameContaining(String name) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByNameContaining
	 *
	 */
	public Set<Course> findCourseByNameContaining(String name, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByName
	 *
	 */
	public Set<Course> findCourseByName(String name_1) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByName
	 *
	 */
	public Set<Course> findCourseByName(String name_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByPrimaryKey
	 *
	 */
	public Course findCourseByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findCourseByPrimaryKey
	 *
	 */
	public Course findCourseByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

}