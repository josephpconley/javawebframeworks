package joejava.golf.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import joejava.golf.domain.Round;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Round entities.
 * 
 */
@Repository("RoundDAO")
@Transactional
public class RoundDAOImpl extends AbstractJpaDao implements RoundDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Round.class }));

	/**
	 * EntityManager injected by Spring for persistence unit joetomcat_no_ip_org
	 *
	 */
	@PersistenceContext(unitName = "joetomcat_no_ip_org")
	private EntityManager entityManager;

	/**
	 * Instantiates a new RoundDAOImpl
	 *
	 */
	public RoundDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit joetomcat_no_ip_org
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findRoundByPrimaryKey
	 *
	 */
	@Transactional
	public Round findRoundByPrimaryKey(Integer id) throws DataAccessException {

		return findRoundByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findRoundByPrimaryKey
	 *
	 */

	@Transactional
	public Round findRoundByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findRoundByPrimaryKey", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllRounds
	 *
	 */
	@Transactional
	public Set<Round> findAllRounds() throws DataAccessException {

		return findAllRounds(-1, -1);
	}

	/**
	 * JPQL Query - findAllRounds
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Round> findAllRounds(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllRounds", startResult, maxRows);
		return new LinkedHashSet<Round>(query.getResultList());
	}

}
