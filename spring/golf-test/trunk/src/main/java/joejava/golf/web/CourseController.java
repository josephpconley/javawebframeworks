package joejava.golf.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import joejava.golf.dao.CourseDAO;
import joejava.golf.dao.TeeDAO;

import joejava.golf.domain.Course;
import joejava.golf.domain.Tee;

import joejava.golf.service.CourseService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.WebDataBinder;

import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.servlet.ModelAndView;

/**
 * Spring MVC controller that handles CRUD requests for Course entities
 * 
 */

@Controller("CourseController")
public class CourseController {

	/**
	 * DAO injected by Spring that manages Course entities
	 * 
	 */
	@Autowired
	private CourseDAO courseDAO;

	/**
	 * DAO injected by Spring that manages Tee entities
	 * 
	 */
	@Autowired
	private TeeDAO teeDAO;

	/**
	 * Service injected by Spring that provides CRUD operations for Course entities
	 * 
	 */
	@Autowired
	private CourseService courseService;

	/**
	 * Show all Tee entities by Course
	 * 
	 */
	@RequestMapping("/listCourseTees")
	public ModelAndView listCourseTees(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", courseDAO.findCourseByPrimaryKey(idKey));
		mav.setViewName("course/tees/listTees.jsp");

		return mav;
	}

	/**
	 * Show all Course entities
	 * 
	 */
	@RequestMapping("/indexCourse")
	public ModelAndView listCourses() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("courses", courseService.loadCourses());

		mav.setViewName("course/listCourses.jsp");

		return mav;
	}

	/**
	 * Select an existing Course entity
	 * 
	 */
	@RequestMapping("/selectCourse")
	public ModelAndView selectCourse(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", courseDAO.findCourseByPrimaryKey(idKey));
		mav.setViewName("course/viewCourse.jsp");

		return mav;
	}

	/**
	 * Select the child Tee entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteCourseTees")
	public ModelAndView confirmDeleteCourseTees(@RequestParam Integer course_id, @RequestParam Integer tee_id) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("tee", teeDAO.findTeeByPrimaryKey(tee_id));
		mav.addObject("course_id", course_id);
		mav.setViewName("course/tees/deleteTees.jsp");

		return mav;
	}

	/**
	 * Entry point to show all Course entities
	 * 
	 */
	public String indexCourse() {
		return "redirect:/indexCourse";
	}

	/**
	 * Select the Course entity for display allowing the user to confirm that they would like to delete the entity
	 * 
	 */
	@RequestMapping("/confirmDeleteCourse")
	public ModelAndView confirmDeleteCourse(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", courseDAO.findCourseByPrimaryKey(idKey));
		mav.setViewName("course/deleteCourse.jsp");

		return mav;
	}

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	@RequestMapping("/deleteCourseTees")
	public ModelAndView deleteCourseTees(@RequestParam Integer course_id, @RequestParam Integer tee_id) {
		ModelAndView mav = new ModelAndView();

		Course course = courseService.deleteCourseTees(tee_id, course_id);

		mav.addObject("course_id", course_id);
		mav.addObject("course", course);
		mav.setViewName("course/viewCourse.jsp");

		return mav;
	}

	/**
	 * Edit an existing Course entity
	 * 
	 */
	@RequestMapping("/editCourse")
	public ModelAndView editCourse(@RequestParam Integer idKey) {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", courseDAO.findCourseByPrimaryKey(idKey));
		mav.setViewName("course/editCourse.jsp");

		return mav;
	}

	/**
	 * Edit an existing Tee entity
	 * 
	 */
	@RequestMapping("/editCourseTees")
	public ModelAndView editCourseTees(@RequestParam Integer course_id, @RequestParam Integer tee_id) {
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("course_id", course_id);
		mav.addObject("tee", tee);
		mav.setViewName("course/tees/editTees.jsp");

		return mav;
	}

	/**
	 * View an existing Tee entity
	 * 
	 */
	@RequestMapping("/selectCourseTees")
	public ModelAndView selectCourseTees(@RequestParam Integer course_id, @RequestParam Integer tee_id) {
		Tee tee = teeDAO.findTeeByPrimaryKey(tee_id, -1, -1);

		ModelAndView mav = new ModelAndView();
		mav.addObject("course_id", course_id);
		mav.addObject("tee", tee);
		mav.setViewName("course/tees/viewTees.jsp");

		return mav;
	}

	/**
	 * Delete an existing Course entity
	 * 
	 */
	@RequestMapping("/deleteCourse")
	public String deleteCourse(@RequestParam Integer idKey) {
		Course course = courseDAO.findCourseByPrimaryKey(idKey);
		courseService.deleteCourse(course);
		return "forward:/indexCourse";
	}

	/**
	 * Create a new Course entity
	 * 
	 */
	@RequestMapping("/newCourse")
	public ModelAndView newCourse() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("course", new Course());
		mav.addObject("newFlag", true);
		mav.setViewName("course/editCourse.jsp");

		return mav;
	}

	/**
	 * Save an existing Tee entity
	 * 
	 */
	@RequestMapping("/saveCourseTees")
	public ModelAndView saveCourseTees(@RequestParam Integer course_id, @ModelAttribute Tee tee) {
		Course course = courseService.saveCourseTees(course_id, tee);

		ModelAndView mav = new ModelAndView();
		mav.addObject("course_id", course_id);
		mav.addObject("course", course);
		mav.setViewName("course/viewCourse.jsp");

		return mav;
	}

	/**
	 */
	@RequestMapping("/courseController/binary.action")
	public ModelAndView streamBinary(@ModelAttribute HttpServletRequest request, @ModelAttribute HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("streamedBinaryContentView");
		return mav;

	}

	/**
	 * Register custom, context-specific property editors
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder, HttpServletRequest request) { // Register static property editors.
		binder.registerCustomEditor(java.util.Calendar.class, new org.skyway.spring.util.databinding.CustomCalendarEditor());
		binder.registerCustomEditor(byte[].class, new org.springframework.web.multipart.support.ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(false));
		binder.registerCustomEditor(Boolean.class, new org.skyway.spring.util.databinding.EnhancedBooleanEditor(true));
		binder.registerCustomEditor(java.math.BigDecimal.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(java.math.BigDecimal.class, true));
		binder.registerCustomEditor(Integer.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Integer.class, true));
		binder.registerCustomEditor(java.util.Date.class, new org.skyway.spring.util.databinding.CustomDateEditor());
		binder.registerCustomEditor(String.class, new org.skyway.spring.util.databinding.StringEditor());
		binder.registerCustomEditor(Long.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Long.class, true));
		binder.registerCustomEditor(Double.class, new org.skyway.spring.util.databinding.NaNHandlingNumberEditor(Double.class, true));
	}

	/**
	 * Create a new Tee entity
	 * 
	 */
	@RequestMapping("/newCourseTees")
	public ModelAndView newCourseTees(@RequestParam Integer course_id) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("course_id", course_id);
		mav.addObject("tee", new Tee());
		mav.addObject("newFlag", true);
		mav.setViewName("course/tees/editTees.jsp");

		return mav;
	}

	/**
	 * Save an existing Course entity
	 * 
	 */
	@RequestMapping("/saveCourse")
	public String saveCourse(@ModelAttribute Course course) {
		courseService.saveCourse(course);
		return "forward:/indexCourse";
	}
}