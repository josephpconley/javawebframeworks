package joejava.golf.service;

import java.util.Set;

import joejava.golf.domain.Course;
import joejava.golf.domain.Tee;

import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import org.springframework.context.ApplicationContext;

import org.springframework.mock.web.MockHttpServletRequest;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;

/**
 * Class to run the service as a JUnit test. Each operation in the service is a separate test.
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners( {
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@ContextConfiguration(locations = {
		"file:./src/main/resources/golf-generated-security-context.xml",
		"file:./src/main/resources/golf-security-context.xml",
		"file:./src/main/resources/golf-generated-service-context.xml",
		"file:./src/main/resources/golf-service-context.xml",
		"file:./src/main/resources/golf-generated-dao-context.xml",
		"file:./src/main/resources/golf-dao-context.xml",
		"file:./src/main/resources/golf-generated-web-context.xml",
		"file:./src/main/resources/golf-web-context.xml" })
@Transactional
public class CourseServiceTest {

	/**
	 * The Spring application context.
	 *
	 */
	@SuppressWarnings("unused")
	private ApplicationContext context;

	/**
	 * The service being tested, injected by Spring.
	 *
	 */
	@Autowired
	protected CourseService service;

	/**
	 * Instantiates a new CourseServiceTest.
	 *
	 */
	public CourseServiceTest() {
		setupRequestContext();
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Course entity
	 * 
	 */
	@Test
	public void deleteCourse() {
		// TODO: JUnit - Populate test inputs for operation: deleteCourse 
		Course course = null;
		service.deleteCourse(course);
	}

	/**
	 * Operation Unit Test
	 * Save an existing Course entity
	 * 
	 */
	@Test
	public void saveCourse() {
		// TODO: JUnit - Populate test inputs for operation: saveCourse 
		Course course_1 = null;
		service.saveCourse(course_1);
	}

	/**
	 * Operation Unit Test
	 * Save an existing Tee entity
	 * 
	 */
	@Test
	public void saveCourseTees() {
		// TODO: JUnit - Populate test inputs for operation: saveCourseTees 
		Integer id = null;
		Tee tee = null;
		Course response = null;
		response = service.saveCourseTees(id, tee);
		// TODO: JUnit - Add assertions to test outputs of operation: saveCourseTees
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Tee entity
	 * 
	 */
	@Test
	public void deleteCourseTees() {
		// TODO: JUnit - Populate test inputs for operation: deleteCourseTees 
		Integer tee_id = null;
		Integer course_id = null;
		Course response = null;
		response = service.deleteCourseTees(tee_id, course_id);
		// TODO: JUnit - Add assertions to test outputs of operation: deleteCourseTees
	}

	/**
	 * Operation Unit Test
	 * Load an existing Course entity
	 * 
	 */
	@Test
	public void loadCourses() {
		Set<Course> response = null;
		response = service.loadCourses();
		// TODO: JUnit - Add assertions to test outputs of operation: loadCourses
	}

	/**
	 * Autowired to set the Spring application context.
	 *
	 */
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("session", new SessionScope());
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("request", new RequestScope());
	}

	/**
	 * Sets Up the Request context
	 *
	 */
	private void setupRequestContext() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		ServletRequestAttributes attributes = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attributes);
	}
}
