package joejava.golf.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import joejava.golf.domain.Tee;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Tee entities.
 * 
 */
@Repository("TeeDAO")
@Transactional
public class TeeDAOImpl extends AbstractJpaDao implements TeeDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Tee.class }));

	/**
	 * EntityManager injected by Spring for persistence unit joetomcat_no_ip_org
	 *
	 */
	@PersistenceContext(unitName = "joetomcat_no_ip_org")
	private EntityManager entityManager;

	/**
	 * Instantiates a new TeeDAOImpl
	 *
	 */
	public TeeDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit joetomcat_no_ip_org
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findAllTees
	 *
	 */
	@Transactional
	public Set<Tee> findAllTees() throws DataAccessException {

		return findAllTees(-1, -1);
	}

	/**
	 * JPQL Query - findAllTees
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tee> findAllTees(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllTees", startResult, maxRows);
		return new LinkedHashSet<Tee>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeeByTeeTypeContaining
	 *
	 */
	@Transactional
	public Set<Tee> findTeeByTeeTypeContaining(String teeType) throws DataAccessException {

		return findTeeByTeeTypeContaining(teeType, -1, -1);
	}

	/**
	 * JPQL Query - findTeeByTeeTypeContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tee> findTeeByTeeTypeContaining(String teeType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeeByTeeTypeContaining", startResult, maxRows, teeType);
		return new LinkedHashSet<Tee>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeeBySlope
	 *
	 */
	@Transactional
	public Set<Tee> findTeeBySlope(Integer slope) throws DataAccessException {

		return findTeeBySlope(slope, -1, -1);
	}

	/**
	 * JPQL Query - findTeeBySlope
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tee> findTeeBySlope(Integer slope, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeeBySlope", startResult, maxRows, slope);
		return new LinkedHashSet<Tee>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeeByTeeType
	 *
	 */
	@Transactional
	public Set<Tee> findTeeByTeeType(String teeType) throws DataAccessException {

		return findTeeByTeeType(teeType, -1, -1);
	}

	/**
	 * JPQL Query - findTeeByTeeType
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tee> findTeeByTeeType(String teeType, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeeByTeeType", startResult, maxRows, teeType);
		return new LinkedHashSet<Tee>(query.getResultList());
	}

	/**
	 * JPQL Query - findTeeById
	 *
	 */
	@Transactional
	public Tee findTeeById(Integer id) throws DataAccessException {

		return findTeeById(id, -1, -1);
	}

	/**
	 * JPQL Query - findTeeById
	 *
	 */

	@Transactional
	public Tee findTeeById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findTeeById", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTeeByPrimaryKey
	 *
	 */
	@Transactional
	public Tee findTeeByPrimaryKey(Integer id) throws DataAccessException {

		return findTeeByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findTeeByPrimaryKey
	 *
	 */

	@Transactional
	public Tee findTeeByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findTeeByPrimaryKey", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findTeeByRating
	 *
	 */
	@Transactional
	public Set<Tee> findTeeByRating(Integer rating) throws DataAccessException {

		return findTeeByRating(rating, -1, -1);
	}

	/**
	 * JPQL Query - findTeeByRating
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Tee> findTeeByRating(Integer rating, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findTeeByRating", startResult, maxRows, rating);
		return new LinkedHashSet<Tee>(query.getResultList());
	}

}
