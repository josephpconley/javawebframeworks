package joejava.golf.service;

import java.util.Set;

import joejava.golf.domain.Course;
import joejava.golf.domain.Tee;

/**
 * Spring service that handles CRUD requests for Course entities
 * 
 */
public interface CourseService {

	/**
	 * Delete an existing Course entity
	 * 
	 */
	public void deleteCourse(Course course);

	/**
	 * Save an existing Course entity
	 * 
	 */
	public void saveCourse(Course course_1);

	/**
	 * Save an existing Tee entity
	 * 
	 */
	public Course saveCourseTees(Integer id, Tee tee);

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	public Course deleteCourseTees(Integer tee_id, Integer course_id);

	/**
	 * Load an existing Course entity
	 * 
	 */
	public Set<Course> loadCourses();
}