package joejava.golf.web;

import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import org.springframework.context.ApplicationContext;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import org.springframework.test.context.ContextConfiguration;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;

/**
 * Unit test for the <code>TeeController</code> controller.
 *
 * @see joejava.golf.web.TeeController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"file:./src/main/resources/golf-generated-security-context.xml",
		"file:./src/main/resources/golf-security-context.xml",
		"file:./src/main/resources/golf-generated-service-context.xml",
		"file:./src/main/resources/golf-service-context.xml",
		"file:./src/main/resources/golf-generated-dao-context.xml",
		"file:./src/main/resources/golf-dao-context.xml",
		"file:./src/main/resources/golf-generated-web-context.xml",
		"file:./src/main/resources/golf-web-context.xml" })
public class TeeControllerTest {
	/**
	 * The Spring application context.
	 *
	 */
	private ApplicationContext context;

	/**
	 * Test <code>editTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>listTeeCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void listTeeCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/listTeeCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>editTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>listTeeRounds()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void listTeeRounds() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/listTeeRounds");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>editTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>listTeeHoles()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void listTeeHoles() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/listTeeHoles");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>indexTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void indexTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/indexTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>editTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteTee()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteTee() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteTee");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>teeControllerbinaryaction()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void teeControllerbinaryaction() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/teeController/binary.action");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		TeeController controller = (TeeController) context.getBean("TeeController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Autowired to set the Spring application context.
	 *
	 */
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("session", new SessionScope());
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("request", new RequestScope());
	}

	/**
	 * Returns a mock HttpServletRequest object.
	 *
	 */
	private MockHttpServletRequest getMockHttpServletRequest() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		ServletRequestAttributes attributes = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attributes);
		return request;
	}

	/**
	 * Returns a mock HttpServletResponse object.
	 *
	 */
	private MockHttpServletResponse getMockHttpServletResponse() {
		return new MockHttpServletResponse();
	}
}