package joejava.golf.service;

import java.util.Set;

import joejava.golf.domain.Course;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Round;
import joejava.golf.domain.Tee;

/**
 * Spring service that handles CRUD requests for Tee entities
 * 
 */
public interface TeeService {

	/**
	 * Save an existing Round entity
	 * 
	 */
	public Tee saveTeeRounds(Integer id, Round round);

	/**
	 * Save an existing Tee entity
	 * 
	 */
	public void saveTee(Tee tee);

	/**
	 * Delete an existing Hole entity
	 * 
	 */
	public Tee deleteTeeHoles(Integer hole_id, Integer tee_id);

	/**
	 * Save an existing Hole entity
	 * 
	 */
	public Tee saveTeeHoles(Integer id_1, Hole hole);

	/**
	 * Delete an existing Tee entity
	 * 
	 */
	public void deleteTee(Tee tee_1);

	/**
	 * Save an existing Course entity
	 * 
	 */
	public Tee saveTeeCourse(Integer id_2, Course course);

	/**
	 * Delete an existing Course entity
	 * 
	 */
	public Tee deleteTeeCourse(Integer tee_id_1, Integer course_id);

	/**
	 * Load an existing Tee entity
	 * 
	 */
	public Set<Tee> loadTees();

	/**
	 * Delete an existing Round entity
	 * 
	 */
	public Tee deleteTeeRounds(Integer round_id, Integer tee_id_2);
}