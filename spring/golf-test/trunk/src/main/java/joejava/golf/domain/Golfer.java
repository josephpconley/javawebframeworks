package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Id;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@Table(schema = "GOLF", name = "GOLFER")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Golfer")
@XmlRootElement(namespace = "golf/joejava/golf/domain")
public class Golfer implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ID", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "USERNAME", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String username;
	/**
	 */

	@Column(name = "PASSWORD", length = 20, nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String password;
	/**
	 */

	@Column(name = "FIRST_NAME", length = 50)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String firstName;
	/**
	 */

	@Column(name = "LAST_NAME", length = 50)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String lastName;
	/**
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "BIRTHDATE")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Calendar birthdate;
	/**
	 */

	@Column(name = "EMAIL", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String email;
	/**
	 */

	@Column(name = "HANDICAP")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer handicap;

	/**
	 */
	@OneToMany(mappedBy = "golfer", fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Round> rounds;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 */
	public void setBirthdate(Calendar birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 */
	public Calendar getBirthdate() {
		return this.birthdate;
	}

	/**
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 */
	public void setHandicap(Integer handicap) {
		this.handicap = handicap;
	}

	/**
	 */
	public Integer getHandicap() {
		return this.handicap;
	}

	/**
	 */
	public void setRounds(Set<Round> rounds) {
		this.rounds = rounds;
	}

	/**
	 */
	public Set<Round> getRounds() {
		if (rounds == null) {
			rounds = new java.util.LinkedHashSet<joejava.golf.domain.Round>();
		}
		return rounds;
	}

	/**
	 */
	public Golfer() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Golfer that) {
		setId(that.getId());
		setUsername(that.getUsername());
		setPassword(that.getPassword());
		setFirstName(that.getFirstName());
		setLastName(that.getLastName());
		setBirthdate(that.getBirthdate());
		setEmail(that.getEmail());
		setHandicap(that.getHandicap());
		setRounds(new java.util.LinkedHashSet<joejava.golf.domain.Round>(that.getRounds()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("username=[").append(username).append("] ");
		buffer.append("password=[").append(password).append("] ");
		buffer.append("firstName=[").append(firstName).append("] ");
		buffer.append("lastName=[").append(lastName).append("] ");
		buffer.append("birthdate=[").append(birthdate).append("] ");
		buffer.append("email=[").append(email).append("] ");
		buffer.append("handicap=[").append(handicap).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Golfer))
			return false;
		Golfer equalCheck = (Golfer) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
