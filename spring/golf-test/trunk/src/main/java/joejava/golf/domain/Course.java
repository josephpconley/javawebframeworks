package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@NamedQueries( {
		@NamedQuery(name = "findAllCourses", query = "select myCourse from Course myCourse"),
		@NamedQuery(name = "findCourseById", query = "select myCourse from Course myCourse where myCourse.id = ?1"),
		@NamedQuery(name = "findCourseByName", query = "select myCourse from Course myCourse where myCourse.name = ?1"),
		@NamedQuery(name = "findCourseByNameContaining", query = "select myCourse from Course myCourse where myCourse.name like ?1"),
		@NamedQuery(name = "findCourseByPrimaryKey", query = "select myCourse from Course myCourse where myCourse.id = ?1") })
@Table(schema = "GOLF", name = "COURSE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Course")
@XmlRootElement(namespace = "golf/joejava/golf/domain")
public class Course implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ID", nullable = false)
	@Basic(fetch = FetchType.EAGER)
	@Id
	@XmlElement
	Integer id;
	/**
	 */

	@Column(name = "NAME", length = 100)
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	String name;

	/**
	 */
	@OneToMany(mappedBy = "course", cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@XmlElement(name = "", namespace = "")
	java.util.Set<joejava.golf.domain.Tee> tees;

	/**
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 */
	public String getName() {
		return this.name;
	}

	/**
	 */
	public void setTees(Set<Tee> tees) {
		this.tees = tees;
	}

	/**
	 */
	public Set<Tee> getTees() {
		if (tees == null) {
			tees = new java.util.LinkedHashSet<joejava.golf.domain.Tee>();
		}
		return tees;
	}

	/**
	 */
	public Course() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Course that) {
		setId(that.getId());
		setName(that.getName());
		setTees(new java.util.LinkedHashSet<joejava.golf.domain.Tee>(that.getTees()));
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("id=[").append(id).append("] ");
		buffer.append("name=[").append(name).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (int) (prime * result + ((id == null) ? 0 : id.hashCode()));
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Course))
			return false;
		Course equalCheck = (Course) obj;
		if ((id == null && equalCheck.id != null) || (id != null && equalCheck.id == null))
			return false;
		if (id != null && !id.equals(equalCheck.id))
			return false;
		return true;
	}
}
