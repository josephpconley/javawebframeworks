package joejava.golf.dao;

import org.skyway.spring.util.dao.JpaDao;

/**
 * DAO to manage Golfer entities.
 * 
 */
public interface GolferDAO extends JpaDao {

}