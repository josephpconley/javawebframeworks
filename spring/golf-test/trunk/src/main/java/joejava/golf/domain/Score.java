package joejava.golf.domain;

import java.io.Serializable;

import java.lang.StringBuilder;

import java.util.Set;

import javax.xml.bind.annotation.*;

import javax.persistence.*;

/**
 */

@Entity
@Table(schema = "GOLF", name = "SCORE")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "golf/joejava/golf/domain", name = "Score")
public class Score implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 */

	@Column(name = "ROUND_ID")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer roundId;
	/**
	 */

	@Column(name = "HOLE_ID")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer holeId;
	/**
	 */

	@Column(name = "SCORE")
	@Basic(fetch = FetchType.EAGER)
	@XmlElement
	Integer scoreField;

	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "ROUND_ID", referencedColumnName = "ID", insertable = false, updatable = false) })
	@XmlTransient
	Round round;
	/**
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( { @JoinColumn(name = "HOLE_ID", referencedColumnName = "ID", insertable = false, updatable = false) })
	@XmlTransient
	Hole hole;

	/**
	 */
	public void setRoundId(Integer roundId) {
		this.roundId = roundId;
	}

	/**
	 */
	public Integer getRoundId() {
		return this.roundId;
	}

	/**
	 */
	public void setHoleId(Integer holeId) {
		this.holeId = holeId;
	}

	/**
	 */
	public Integer getHoleId() {
		return this.holeId;
	}

	/**
	 */
	public void setScoreField(Integer scoreField) {
		this.scoreField = scoreField;
	}

	/**
	 */
	public Integer getScoreField() {
		return this.scoreField;
	}

	/**
	 */
	public void setRound(Round round) {
		this.round = round;
	}

	/**
	 */
	public Round getRound() {
		return round;
	}

	/**
	 */
	public void setHole(Hole hole) {
		this.hole = hole;
	}

	/**
	 */
	public Hole getHole() {
		return hole;
	}

	/**
	 */
	public Score() {
	}

	/**
	 * Copies the contents of the specified bean into this bean.
	 *
	 */
	public void copy(Score that) {
		setRoundId(that.getRoundId());
		setHoleId(that.getHoleId());
		setScoreField(that.getScoreField());
		setRound(that.getRound());
		setHole(that.getHole());
	}

	/**
	 * Returns a textual representation of a bean.
	 *
	 */
	public String toString() {

		StringBuilder buffer = new StringBuilder();

		buffer.append("roundId=[").append(roundId).append("] ");
		buffer.append("holeId=[").append(holeId).append("] ");
		buffer.append("scoreField=[").append(scoreField).append("] ");

		return buffer.toString();
	}

	/**
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		return result;
	}

	/**
	 */
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Score))
			return false;
		Score equalCheck = (Score) obj;
		return true;
	}
}
