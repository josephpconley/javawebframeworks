package joejava.golf.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import joejava.golf.domain.Course;

import org.skyway.spring.util.dao.AbstractJpaDao;

import org.springframework.dao.DataAccessException;

import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

/**
 * DAO to manage Course entities.
 * 
 */
@Repository("CourseDAO")
@Transactional
public class CourseDAOImpl extends AbstractJpaDao implements CourseDAO {

	/**
	 * Set of entity classes managed by this DAO.  Typically a DAO manages a single entity.
	 *
	 */
	private final static Set<Class<?>> dataTypes = new HashSet<Class<?>>(Arrays.asList(new Class<?>[] { Course.class }));

	/**
	 * EntityManager injected by Spring for persistence unit joetomcat_no_ip_org
	 *
	 */
	@PersistenceContext(unitName = "joetomcat_no_ip_org")
	private EntityManager entityManager;

	/**
	 * Instantiates a new CourseDAOImpl
	 *
	 */
	public CourseDAOImpl() {
		super();
	}

	/**
	 * Get the entity manager that manages persistence unit joetomcat_no_ip_org
	 *
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Returns the set of entity classes managed by this DAO.
	 *
	 */
	public Set<Class<?>> getTypes() {
		return dataTypes;
	}

	/**
	 * JPQL Query - findCourseById
	 *
	 */
	@Transactional
	public Course findCourseById(Integer id) throws DataAccessException {

		return findCourseById(id, -1, -1);
	}

	/**
	 * JPQL Query - findCourseById
	 *
	 */

	@Transactional
	public Course findCourseById(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findCourseById", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * JPQL Query - findAllCourses
	 *
	 */
	@Transactional
	public Set<Course> findAllCourses() throws DataAccessException {

		return findAllCourses(-1, -1);
	}

	/**
	 * JPQL Query - findAllCourses
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Course> findAllCourses(int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findAllCourses", startResult, maxRows);
		return new LinkedHashSet<Course>(query.getResultList());
	}

	/**
	 * JPQL Query - findCourseByNameContaining
	 *
	 */
	@Transactional
	public Set<Course> findCourseByNameContaining(String name) throws DataAccessException {

		return findCourseByNameContaining(name, -1, -1);
	}

	/**
	 * JPQL Query - findCourseByNameContaining
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Course> findCourseByNameContaining(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCourseByNameContaining", startResult, maxRows, name);
		return new LinkedHashSet<Course>(query.getResultList());
	}

	/**
	 * JPQL Query - findCourseByName
	 *
	 */
	@Transactional
	public Set<Course> findCourseByName(String name) throws DataAccessException {

		return findCourseByName(name, -1, -1);
	}

	/**
	 * JPQL Query - findCourseByName
	 *
	 */

	@SuppressWarnings("unchecked")
	@Transactional
	public Set<Course> findCourseByName(String name, int startResult, int maxRows) throws DataAccessException {
		Query query = createNamedQuery("findCourseByName", startResult, maxRows, name);
		return new LinkedHashSet<Course>(query.getResultList());
	}

	/**
	 * JPQL Query - findCourseByPrimaryKey
	 *
	 */
	@Transactional
	public Course findCourseByPrimaryKey(Integer id) throws DataAccessException {

		return findCourseByPrimaryKey(id, -1, -1);
	}

	/**
	 * JPQL Query - findCourseByPrimaryKey
	 *
	 */

	@Transactional
	public Course findCourseByPrimaryKey(Integer id, int startResult, int maxRows) throws DataAccessException {
		try {
			return executeQueryByNameSingleResult("findCourseByPrimaryKey", id);
		} catch (NoResultException nre) {
			return null;
		}
	}

}
