package joejava.golf.dao;

import org.skyway.spring.util.dao.JpaDao;

/**
 * DAO to manage Score entities.
 * 
 */
public interface ScoreDAO extends JpaDao {

}