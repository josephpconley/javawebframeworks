package joejava.golf.web;

import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import org.springframework.context.ApplicationContext;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import org.springframework.test.context.ContextConfiguration;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;

/**
 * Unit test for the <code>CourseController</code> controller.
 *
 * @see joejava.golf.web.CourseController
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"file:./src/main/resources/golf-generated-security-context.xml",
		"file:./src/main/resources/golf-security-context.xml",
		"file:./src/main/resources/golf-generated-service-context.xml",
		"file:./src/main/resources/golf-service-context.xml",
		"file:./src/main/resources/golf-generated-dao-context.xml",
		"file:./src/main/resources/golf-dao-context.xml",
		"file:./src/main/resources/golf-generated-web-context.xml",
		"file:./src/main/resources/golf-web-context.xml" })
public class CourseControllerTest {
	/**
	 * The Spring application context.
	 *
	 */
	private ApplicationContext context;

	/**
	 * Test <code>editCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>listCourseTees()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void listCourseTees() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/listCourseTees");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>indexCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void indexCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/indexCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>selectCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void selectCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/selectCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>editCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void editCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/editCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>saveCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void saveCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/saveCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>newCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void newCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/newCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>confirmDeleteCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void confirmDeleteCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/confirmDeleteCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>deleteCourse()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void deleteCourse() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/deleteCourse");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Test <code>courseControllerbinaryaction()</code>.
	 */
	@Test
	@SuppressWarnings("unused")
	public void courseControllerbinaryaction() throws Exception {
		MockHttpServletRequest request = getMockHttpServletRequest();
		request.setRequestURI("/courseController/binary.action");
		MockHttpServletResponse response = getMockHttpServletResponse();

		// Get the singleton controller instance
		CourseController controller = (CourseController) context.getBean("CourseController");

		// TODO Invoke method and Assert return values

	}

	/**
	 * Autowired to set the Spring application context.
	 *
	 */
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("session", new SessionScope());
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("request", new RequestScope());
	}

	/**
	 * Returns a mock HttpServletRequest object.
	 *
	 */
	private MockHttpServletRequest getMockHttpServletRequest() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		ServletRequestAttributes attributes = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attributes);
		return request;
	}

	/**
	 * Returns a mock HttpServletResponse object.
	 *
	 */
	private MockHttpServletResponse getMockHttpServletResponse() {
		return new MockHttpServletResponse();
	}
}