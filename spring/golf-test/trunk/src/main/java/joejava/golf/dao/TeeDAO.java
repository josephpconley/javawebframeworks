package joejava.golf.dao;

import java.util.Set;

import joejava.golf.domain.Tee;

import org.skyway.spring.util.dao.JpaDao;

import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Tee entities.
 * 
 */
public interface TeeDAO extends JpaDao {

	/**
	 * JPQL Query - findAllTees
	 *
	 */
	public Set<Tee> findAllTees() throws DataAccessException;

	/**
	 * JPQL Query - findAllTees
	 *
	 */
	public Set<Tee> findAllTees(int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByTeeTypeContaining
	 *
	 */
	public Set<Tee> findTeeByTeeTypeContaining(String teeType) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByTeeTypeContaining
	 *
	 */
	public Set<Tee> findTeeByTeeTypeContaining(String teeType, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeBySlope
	 *
	 */
	public Set<Tee> findTeeBySlope(Integer slope) throws DataAccessException;

	/**
	 * JPQL Query - findTeeBySlope
	 *
	 */
	public Set<Tee> findTeeBySlope(Integer slope, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByTeeType
	 *
	 */
	public Set<Tee> findTeeByTeeType(String teeType_1) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByTeeType
	 *
	 */
	public Set<Tee> findTeeByTeeType(String teeType_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeById
	 *
	 */
	public Tee findTeeById(Integer id) throws DataAccessException;

	/**
	 * JPQL Query - findTeeById
	 *
	 */
	public Tee findTeeById(Integer id, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByPrimaryKey
	 *
	 */
	public Tee findTeeByPrimaryKey(Integer id_1) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByPrimaryKey
	 *
	 */
	public Tee findTeeByPrimaryKey(Integer id_1, int startResult, int maxRows) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByRating
	 *
	 */
	public Set<Tee> findTeeByRating(Integer rating) throws DataAccessException;

	/**
	 * JPQL Query - findTeeByRating
	 *
	 */
	public Set<Tee> findTeeByRating(Integer rating, int startResult, int maxRows) throws DataAccessException;

}