package joejava.golf.service;

import java.util.Set;

import joejava.golf.domain.Course;
import joejava.golf.domain.Hole;
import joejava.golf.domain.Round;
import joejava.golf.domain.Tee;

import org.junit.Test;

import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import org.springframework.context.ApplicationContext;

import org.springframework.mock.web.MockHttpServletRequest;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;

/**
 * Class to run the service as a JUnit test. Each operation in the service is a separate test.
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners( {
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@ContextConfiguration(locations = {
		"file:./src/main/resources/golf-generated-security-context.xml",
		"file:./src/main/resources/golf-security-context.xml",
		"file:./src/main/resources/golf-generated-service-context.xml",
		"file:./src/main/resources/golf-service-context.xml",
		"file:./src/main/resources/golf-generated-dao-context.xml",
		"file:./src/main/resources/golf-dao-context.xml",
		"file:./src/main/resources/golf-generated-web-context.xml",
		"file:./src/main/resources/golf-web-context.xml" })
@Transactional
public class TeeServiceTest {

	/**
	 * The Spring application context.
	 *
	 */
	@SuppressWarnings("unused")
	private ApplicationContext context;

	/**
	 * The service being tested, injected by Spring.
	 *
	 */
	@Autowired
	protected TeeService service;

	/**
	 * Instantiates a new TeeServiceTest.
	 *
	 */
	public TeeServiceTest() {
		setupRequestContext();
	}

	/**
	 * Operation Unit Test
	 * Save an existing Round entity
	 * 
	 */
	@Test
	public void saveTeeRounds() {
		// TODO: JUnit - Populate test inputs for operation: saveTeeRounds 
		Integer id = null;
		Round round = null;
		Tee response = null;
		response = service.saveTeeRounds(id, round);
		// TODO: JUnit - Add assertions to test outputs of operation: saveTeeRounds
	}

	/**
	 * Operation Unit Test
	 * Save an existing Tee entity
	 * 
	 */
	@Test
	public void saveTee() {
		// TODO: JUnit - Populate test inputs for operation: saveTee 
		Tee tee = null;
		service.saveTee(tee);
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Hole entity
	 * 
	 */
	@Test
	public void deleteTeeHoles() {
		// TODO: JUnit - Populate test inputs for operation: deleteTeeHoles 
		Integer hole_id = null;
		Integer tee_id = null;
		Tee response = null;
		response = service.deleteTeeHoles(hole_id, tee_id);
		// TODO: JUnit - Add assertions to test outputs of operation: deleteTeeHoles
	}

	/**
	 * Operation Unit Test
	 * Save an existing Hole entity
	 * 
	 */
	@Test
	public void saveTeeHoles() {
		// TODO: JUnit - Populate test inputs for operation: saveTeeHoles 
		Integer id_1 = null;
		Hole hole = null;
		Tee response = null;
		response = service.saveTeeHoles(id_1, hole);
		// TODO: JUnit - Add assertions to test outputs of operation: saveTeeHoles
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Tee entity
	 * 
	 */
	@Test
	public void deleteTee() {
		// TODO: JUnit - Populate test inputs for operation: deleteTee 
		Tee tee_1 = null;
		service.deleteTee(tee_1);
	}

	/**
	 * Operation Unit Test
	 * Save an existing Course entity
	 * 
	 */
	@Test
	public void saveTeeCourse() {
		// TODO: JUnit - Populate test inputs for operation: saveTeeCourse 
		Integer id_2 = null;
		Course course = null;
		Tee response = null;
		response = service.saveTeeCourse(id_2, course);
		// TODO: JUnit - Add assertions to test outputs of operation: saveTeeCourse
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Course entity
	 * 
	 */
	@Test
	public void deleteTeeCourse() {
		// TODO: JUnit - Populate test inputs for operation: deleteTeeCourse 
		Integer tee_id_1 = null;
		Integer course_id = null;
		Tee response = null;
		response = service.deleteTeeCourse(tee_id_1, course_id);
		// TODO: JUnit - Add assertions to test outputs of operation: deleteTeeCourse
	}

	/**
	 * Operation Unit Test
	 * Load an existing Tee entity
	 * 
	 */
	@Test
	public void loadTees() {
		Set<Tee> response = null;
		response = service.loadTees();
		// TODO: JUnit - Add assertions to test outputs of operation: loadTees
	}

	/**
	 * Operation Unit Test
	 * Delete an existing Round entity
	 * 
	 */
	@Test
	public void deleteTeeRounds() {
		// TODO: JUnit - Populate test inputs for operation: deleteTeeRounds 
		Integer round_id = null;
		Integer tee_id_2 = null;
		Tee response = null;
		response = service.deleteTeeRounds(round_id, tee_id_2);
		// TODO: JUnit - Add assertions to test outputs of operation: deleteTeeRounds
	}

	/**
	 * Autowired to set the Spring application context.
	 *
	 */
	@Autowired
	public void setContext(ApplicationContext context) {
		this.context = context;
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("session", new SessionScope());
		((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerScope("request", new RequestScope());
	}

	/**
	 * Sets Up the Request context
	 *
	 */
	private void setupRequestContext() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		ServletRequestAttributes attributes = new ServletRequestAttributes(request);
		RequestContextHolder.setRequestAttributes(attributes);
	}
}
