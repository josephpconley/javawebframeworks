<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
    <title>Pensieve</title>
</head>
<body>
	<form:form method="post" action="addContact">
	    <table>
		    <tr>
		        <td><form:label path="firstName"><spring:message code="label.firstName"/></form:label></td>
		        <td><form:input path="firstName" /></td>
		    </tr>
		    <tr>
		        <td><form:label path="lastName"><spring:message code="label.lastName"/></form:label></td>
		        <td><form:input path="lastName" /></td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <input type="submit" value="<spring:message code="label.addcontact"/>"/>
		        </td>
		    </tr>
		</table>  
	</form:form>

	<h3>Contacts</h3>
		<c:if  test="${!empty contactList}">
			<table class="data">
			<tr>
			    <th>First Name</th>
			    <th>Last Name</th>
			    <th>&nbsp;</th>
			</tr>
			<c:forEach items="${contactList}" var="contact">
			    <tr>
			        <td>${contact.firstName}</td>
			        <td>${contact.lastName}</td>
			        <td><a href="delete/${contact.id}">delete</a></td>
			    </tr>
			</c:forEach>
			</table>
		</c:if>	
</body>
</html>