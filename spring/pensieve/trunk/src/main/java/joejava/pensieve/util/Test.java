package joejava.pensieve.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Test {
	public static void main(String[] args){
		ApplicationContext context =  new FileSystemXmlApplicationContext("target/classes/META-INF/spring/spring-servlet.xml");
		//BasicDataSource ds = (BasicDataSource)context.getBean("dataSource");
		//Contact con = (Contact) context.getBean("contact");
		/*
		Contact con = new Contact();
		List<Contact> list = con.findAllContacts();
		System.out.println(list.size());
		for(Contact c : list){
			System.out.println(c.toString());
		}
		*/
		//BeanFactory factory = new XmlBeanFactory(new FileSystemResource(""));
		
		GreetingService greetingService = (GreetingService) context.getBean("greetingService");
		greetingService.sayGreeting();		
	}
}
