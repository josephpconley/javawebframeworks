package joejava.pensieve.web;

import java.util.List;
import java.util.Map;

import joejava.pensieve.domain.Contact;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactController {
    
	@RequestMapping(value = "/addContact", method = RequestMethod.POST)
    public String addContact(@ModelAttribute("contact") Contact contact, BindingResult result) {
        System.out.println(contact.toString());
        return "redirect:contacts";
    }
 
	/*
	@RequestMapping("/contacts")
    public ModelAndView showContacts() {
        return new ModelAndView("contact", "command", new Contact());
    }
    */
	
	@RequestMapping("/index")
	public String listContacts(Map<String,Object> map){
		map.put("contact",new Contact());

		List<Contact> contacts = Contact.findAllContacts(); 
		map.put("contactList",contacts);

		System.out.println(contacts.size());
		return "contact";
	}
}
