package joejava.pensieve.domain;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.roo.addon.entity.RooEntity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RooJavaBean
@RooToString
@RooEntity
public class Address {

    @NotNull
    @Size(max = 200)
    private String streetAddress;

    private String city;

    @Size(max = 3)
    private String stateProv;

    private Integer zip;
}
