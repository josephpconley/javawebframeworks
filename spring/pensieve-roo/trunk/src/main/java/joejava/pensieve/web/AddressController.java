package joejava.pensieve.web;

import org.springframework.roo.addon.web.mvc.controller.RooWebScaffold;
import joejava.pensieve.domain.Address;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@RooWebScaffold(path = "addresses", formBackingObject = Address.class)
@RequestMapping("/addresses")
@Controller
public class AddressController {
}
