// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package joejava.pensieve.web;

import java.io.UnsupportedEncodingException;
import java.lang.Long;
import java.lang.String;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import joejava.pensieve.domain.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect AddressController_Roo_Controller {
    
    @Autowired
    private GenericConversionService AddressController.conversionService;
    
    @RequestMapping(method = RequestMethod.POST)
    public String AddressController.create(@Valid Address address, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("address", address);
            return "addresses/create";
        }
        address.persist();
        return "redirect:/addresses/" + encodeUrlPathSegment(address.getId().toString(), request);
    }
    
    @RequestMapping(params = "form", method = RequestMethod.GET)
    public String AddressController.createForm(Model model) {
        model.addAttribute("address", new Address());
        return "addresses/create";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String AddressController.show(@PathVariable("id") Long id, Model model) {
        model.addAttribute("address", Address.findAddress(id));
        model.addAttribute("itemId", id);
        return "addresses/show";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String AddressController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            model.addAttribute("addresses", Address.findAddressEntries(page == null ? 0 : (page.intValue() - 1) * sizeNo, sizeNo));
            float nrOfPages = (float) Address.countAddresses() / sizeNo;
            model.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            model.addAttribute("addresses", Address.findAllAddresses());
        }
        return "addresses/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public String AddressController.update(@Valid Address address, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("address", address);
            return "addresses/update";
        }
        address.merge();
        return "redirect:/addresses/" + encodeUrlPathSegment(address.getId().toString(), request);
    }
    
    @RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
    public String AddressController.updateForm(@PathVariable("id") Long id, Model model) {
        model.addAttribute("address", Address.findAddress(id));
        return "addresses/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String AddressController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model model) {
        Address.findAddress(id).remove();
        model.addAttribute("page", (page == null) ? "1" : page.toString());
        model.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/addresses?page=" + ((page == null) ? "1" : page.toString()) + "&size=" + ((size == null) ? "10" : size.toString());
    }
    
    Converter<Address, String> AddressController.getAddressConverter() {
        return new Converter<Address, String>() {
            public String convert(Address address) {
                return new StringBuilder().append(address.getStreetAddress()).append(" ").append(address.getCity()).append(" ").append(address.getStateProv()).toString();
            }
        };
    }
    
    @PostConstruct
    void AddressController.registerConverters() {
        conversionService.addConverter(getAddressConverter());
    }
    
    private String AddressController.encodeUrlPathSegment(String pathSegment, HttpServletRequest request) {
        String enc = request.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        }
        catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
