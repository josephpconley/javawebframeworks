<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Spring 3 MVC Series - Contact Manager</title>
    <style type="text/css" media="screen">
		@import url("/spring/css/styles.css");
    </style>
</head>
<body>

<h2>Contact Manager</h2>

<!-- use href for add just like delete below? -->
<form:form method="post" action="add" commandName="contact">

	<table>
	<tr>
		<td><form:label path="firstName"><spring:message code="label.firstName"/></form:label></td>
		<td><form:input path="firstName" /></td> 
	</tr>
	<tr>
		<td><form:label path="lastName"><spring:message code="label.lastName"/></form:label></td>
		<td><form:input path="lastName" /></td>
	</tr>
	<tr>
		<td><form:label path="birthDate"><spring:message code="label.birthDate"/></form:label></td>
		<td><form:input path="birthDate" /></td>
	</tr>	
	<tr>
		<td colspan="2">
			<input type="submit" value="<spring:message code="label.addcontact"/>"/>
		</td>
	</tr>
</table>	
</form:form>

	
<h3>Contacts</h3>
<c:if  test="${!empty contactList}">
<table class="data">
<tr>
	<th>Name</th>
	<th>&nbsp;</th>
</tr>
<c:forEach items="${contactList}" var="contact">
	<tr>
		<td>${contact.lastName}, ${contact.firstName} </td>
		<td>${contact.birthDate}</td>
		<td><a href="delete/${contact.id}">delete</a></td>
	</tr>
</c:forEach>
</table>
</c:if>


</body>
</html>
