<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>Spring 3 MVC Series - Contact Manager</title>
	<style type="text/css" media="screen">
		@import url("/spring/css/styles.css");
    </style>
</head>
<body>

<h2>Show Contact</h2>

	<c:if  test="${!empty contact}">
	<table class="data">
		<tr>
			<th>Name</th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<td>${contact.lastName}, ${contact.firstName} </td>
			<td><a href="delete/${contact.id}">delete</a></td>
		</tr>
	</table>
	</c:if>

</body>
</html>
