package spring.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.stream.StreamSource;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;
import org.springframework.web.client.RestTemplate;

import spring.domain.Contact;

public class RestClient {
	public static void main(String[] args) throws XmlMappingException, IOException{
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext-content*");
		
		//GET
		String url = "http://localhost:81/spring/contacts/{id}.xml";
		RestTemplate rest = new RestTemplate();
		
		String xml = rest.getForObject(url, String.class, "1");
		Unmarshaller m = (Unmarshaller)con.getBean("jaxbMarshaller");
		
		Contact joe = (Contact)m.unmarshal(new StreamSource(new StringReader(xml)));
		System.out.println(joe.toString());
	}
}
