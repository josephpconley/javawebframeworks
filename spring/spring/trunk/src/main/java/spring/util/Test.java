package spring.util;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.stream.StreamResult;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import spring.domain.Address;
import spring.domain.Contact;
import spring.domain.Email;
import spring.domain.Event;
import spring.domain.Phone;
import spring.service.AddressService;
import spring.service.ContactService;
import spring.service.EmailService;
import spring.service.EventService;
import spring.service.PhoneService;

@Service
public class Test {
	
	public static void main(String[] args) throws Exception{
		System.out.println("Starting test");
		ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext*");
		/*	EMAIL
		MailService mail = (MailService) con.getBean("mailService");
		mail.sendMessage("joe@pensieve.com","josephpconley@gmail.com","Test","sup");
		*/

		/*	DB CALL */
		ContactService contactService = (ContactService) con.getBean("contactServiceImpl");
		EmailService emailService = (EmailService) con.getBean("emailServiceImpl");
		PhoneService phoneService = (PhoneService)con.getBean("phoneServiceImpl");
		EventService eventService = (EventService)con.getBean("eventServiceImpl");
		AddressService addressService = (AddressService)con.getBean("addressServiceImpl");
		
		List<Contact> contacts = contactService.listContact();
		System.out.println(contacts.size());
		for(Contact c : contacts){
			//String bday = c.getFirstName() + " " + c.getLastName() + "'s Birthday!";
			System.out.println(c.toString());
			//mailService.sendMessage("test@123.com","josephpconley@gmail.com","test",c.toString());
			contactService.deleteContact(c.getId());
		}
		
		Contact joe = new Contact();
		joe.setFirstName("Joe");
		joe.setLastName("Conley");
		joe.setNickname("Jolty");
		
		Email gmail = new Email();
		gmail.setAddress("josephpconley@gmail.com");
		gmail.setType("gmail");
		emailService.addEmail(gmail);

		Phone phone = new Phone();
		phone.setNumber("6104163219");
		phone.setType("Mobile");
		phoneService.addPhone(phone);
		
		Event bday = new Event();
		Calendar calendar = Calendar.getInstance();
		calendar.set(1984, 8, 5);
		bday.setStartDate(calendar.getTime());
		bday.setType("Birthday");
		eventService.addEvent(bday);
		
		Address address = new Address();
		address.setStreetAddress("71 Hargrave Lane");
		address.setCity("Media");
		addressService.addAddress(address);
		
		joe.getEmail().add(gmail);
		joe.getPhone().add(phone);
		joe.getEvent().add(bday);
		joe.getAddress().add(address);
		contactService.addContact(joe);

		Marshaller m = (Marshaller)con.getBean("jaxbMarshaller");
		StringWriter out = new StringWriter();
		StreamResult xml = new StreamResult(out);
		m.marshal(joe, xml);

		String url = "http://localhost:81/spring/contacts/add";
		RestTemplate rest = new RestTemplate();
		
		String post = rest.postForObject(url, joe, String.class);
		System.out.println(post);
		
		/**/
		
		/*	Generating XML 

		ContactService db = (ContactService)con.getBean("contactServiceImpl");
		Contact joe = db.getContact(1);
		System.out.println(joe.toString());

		Marshaller m = (Marshaller)con.getBean("jaxbMarshaller");
		StringWriter out = new StringWriter();
		StreamResult xml = new StreamResult(out);
		m.marshal(joe, xml);

		
		/*	Using a URL from our webapp to retrieve xml
		URL xml = new URL("http://joetomcat.no-ip.org:81/spring/contacts/1.xml");
		InputStream is = (InputStream) xml.openStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		} 
	 
		br.close();	
		*/
	}
}
