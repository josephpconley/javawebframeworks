package spring.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {
	
	@Before("call(* spring.dao..*.*(..))")
	public void logDatabase(){
		System.out.println("Making a database call");
	}
}
