/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.dao.EventDAO;
import spring.domain.Event;

/**
 * @author jconley
 *
 */
@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventDAO eventDAO;
	
	@Transactional
	public void addEvent(Event event) {
		eventDAO.addEvent(event);
	}

	@Transactional
	public List<Event> listEvent() {
		return eventDAO.listEvent();
	}

	@Transactional
	public void deleteEvent(Integer id) {
		eventDAO.deleteEvent(id);
	}
	
	@Transactional
	public Event getEvent(Integer id){
		return eventDAO.getEvent(id);
	}
}
