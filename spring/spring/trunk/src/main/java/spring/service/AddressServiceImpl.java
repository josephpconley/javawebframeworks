/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.dao.AddressDAO;
import spring.domain.Address;

/**
 * @author jconley
 *
 */
@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDAO addressDAO;
	
	@Transactional
	public void addAddress(Address address) {
		addressDAO.addAddress(address);
	}

	@Transactional
	public List<Address> listAddress() {
		return addressDAO.listAddress();
	}

	@Transactional
	public void deleteAddress(Integer id) {
		addressDAO.deleteAddress(id);
	}
	
	@Transactional
	public Address getAddress(Integer id){
		return addressDAO.getAddress(id);
	}
}
