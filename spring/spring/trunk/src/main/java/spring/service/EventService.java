/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.domain.Event;

/**
 * @author jconley
 *
 */
public interface EventService {
	 
    public void addEvent(Event event);
    public List<Event> listEvent();
    public void deleteEvent(Integer id);
    public Event getEvent(Integer id);
}
