/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.domain.Phone;

/**
 * @author jconley
 *
 */
public interface PhoneService {
	 
    public void addPhone(Phone phone);
    public List<Phone> listPhone();
    public void deletePhone(Integer id);
    public Phone getPhone(Integer id);
}
