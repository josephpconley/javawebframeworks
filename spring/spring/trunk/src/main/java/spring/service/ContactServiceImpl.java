/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.dao.ContactDAO;
import spring.domain.Contact;

/**
 * @author jconley
 *
 */
@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ContactDAO contactDAO;
	
	@Transactional
	public void addContact(Contact contact) {
		contactDAO.addContact(contact);
	}

	@Transactional
	public List<Contact> listContact() {
		return contactDAO.listContact();
	}

	@Transactional
	public void deleteContact(Integer id) {
		contactDAO.deleteContact(id);
	}
	
	@Transactional
	public Contact getContact(Integer id){
		return contactDAO.getContact(id);
	}
}
