/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.domain.Address;

/**
 * @author jconley
 *
 */
public interface AddressService {
	 
    public void addAddress(Address address);
    public List<Address> listAddress();
    public void deleteAddress(Integer id);
    public Address getAddress(Integer id);
}
