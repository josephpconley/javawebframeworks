/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.domain.Email;

/**
 * @author jconley
 *
 */
public interface EmailService {
	 
    public void addEmail(Email email);
    public List<Email> listEmail();
    public void deleteEmail(Integer id);
    public Email getEmail(Integer id);
}
