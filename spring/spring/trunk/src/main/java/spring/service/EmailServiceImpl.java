/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.dao.EmailDAO;
import spring.domain.Email;

/**
 * @author jconley
 *
 */
@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	private EmailDAO emailDAO;
	
	@Transactional
	public void addEmail(Email email) {
		emailDAO.addEmail(email);
	}

	@Transactional
	public List<Email> listEmail() {
		return emailDAO.listEmail();
	}

	@Transactional
	public void deleteEmail(Integer id) {
		emailDAO.deleteEmail(id);
	}
	
	@Transactional
	public Email getEmail(Integer id){
		return emailDAO.getEmail(id);
	}
}
