/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.dao.PhoneDAO;
import spring.domain.Phone;

/**
 * @author jconley
 *
 */
@Service
public class PhoneServiceImpl implements PhoneService {

	@Autowired
	private PhoneDAO phoneDAO;
	
	@Transactional
	public void addPhone(Phone phone) {
		phoneDAO.addPhone(phone);
	}

	@Transactional
	public List<Phone> listPhone() {
		return phoneDAO.listPhone();
	}

	@Transactional
	public void deletePhone(Integer id) {
		phoneDAO.deletePhone(id);
	}
	
	@Transactional
	public Phone getPhone(Integer id){
		return phoneDAO.getPhone(id);
	}
}
