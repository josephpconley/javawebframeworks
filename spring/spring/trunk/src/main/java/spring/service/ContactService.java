/**
 * 
 */
package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.domain.Contact;

/**
 * @author jconley
 *
 */
public interface ContactService {
	 
    public void addContact(Contact contact);
    public List<Contact> listContact();
    public void deleteContact(Integer id);
    public Contact getContact(Integer id);
}
