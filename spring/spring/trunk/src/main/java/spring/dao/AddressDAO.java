/**
 * 
 */
package spring.dao;

import java.util.List;

import spring.domain.Address;

/**
 * @author jconley
 *
 */
public interface AddressDAO {
	public void addAddress(Address address);
    public List<Address> listAddress();
    public void deleteAddress(Integer id);
    public Address getAddress(Integer id);

}
