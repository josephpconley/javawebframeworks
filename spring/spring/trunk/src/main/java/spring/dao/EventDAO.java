/**
 * 
 */
package spring.dao;

import java.util.List;

import spring.domain.Event;

/**
 * @author jconley
 *
 */
public interface EventDAO {
	public void addEvent(Event event);
    public List<Event> listEvent();
    public void deleteEvent(Integer id);
    public Event getEvent(Integer id);

}
