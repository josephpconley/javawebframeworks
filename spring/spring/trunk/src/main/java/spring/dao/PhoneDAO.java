/**
 * 
 */
package spring.dao;

import java.util.List;

import spring.domain.Phone;

/**
 * @author jconley
 *
 */
public interface PhoneDAO {
	public void addPhone(Phone phone);
    public List<Phone> listPhone();
    public void deletePhone(Integer id);
    public Phone getPhone(Integer id);

}
