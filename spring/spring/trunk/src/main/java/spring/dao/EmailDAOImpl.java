/**
 * 
 */
package spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.domain.Email;

/**
 * @author jconley
 *
 */
@Repository
public class EmailDAOImpl implements EmailDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public void addEmail(Email email) {
        sessionFactory.getCurrentSession().save(email);
    }
 
    public List<Email> listEmail() {
        return sessionFactory.getCurrentSession().createQuery("from Email").list();
    }
 
    public void deleteEmail(Integer id) {
        Email email = (Email) sessionFactory.getCurrentSession().load(Email.class, id);
        if (null != email) {
            sessionFactory.getCurrentSession().delete(email);
        }
    }

	public Email getEmail(Integer id) {
		return (Email)sessionFactory.getCurrentSession().createQuery("from Email where ID = " + id).uniqueResult();
	}
}
