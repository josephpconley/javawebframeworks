/**
 * 
 */
package spring.dao;

import java.util.List;

import spring.domain.Contact;

/**
 * @author jconley
 *
 */
public interface ContactDAO {
	public void addContact(Contact contact);
    public List<Contact> listContact();
    public void deleteContact(Integer id);
    public Contact getContact(Integer id);

}
