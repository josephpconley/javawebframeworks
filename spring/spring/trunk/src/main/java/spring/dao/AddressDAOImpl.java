/**
 * 
 */
package spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.domain.Address;

/**
 * @author jconley
 *
 */
@Repository
public class AddressDAOImpl implements AddressDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public void addAddress(Address address) {
        sessionFactory.getCurrentSession().save(address);
    }
 
    public List<Address> listAddress() {
        return sessionFactory.getCurrentSession().createQuery("from Address").list();
    }
 
    public void deleteAddress(Integer id) {
        Address address = (Address) sessionFactory.getCurrentSession().load(Address.class, id);
        if (null != address) {
            sessionFactory.getCurrentSession().delete(address);
        }
    }

	public Address getAddress(Integer id) {
		return (Address)sessionFactory.getCurrentSession().createQuery("from Address where ID = " + id).uniqueResult();
	}
}
