/**
 * 
 */
package spring.dao;

import java.util.List;

import spring.domain.Email;

/**
 * @author jconley
 *
 */
public interface EmailDAO {
	public void addEmail(Email email);
    public List<Email> listEmail();
    public void deleteEmail(Integer id);
    public Email getEmail(Integer id);

}
