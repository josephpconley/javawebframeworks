/**
 * 
 */
package spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.domain.Phone;

/**
 * @author jconley
 *
 */
@Repository
public class PhoneDAOImpl implements PhoneDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public void addPhone(Phone phone) {
        sessionFactory.getCurrentSession().save(phone);
    }
 
    public List<Phone> listPhone() {
        return sessionFactory.getCurrentSession().createQuery("from Phone").list();
    }
 
    public void deletePhone(Integer id) {
        Phone phone = (Phone) sessionFactory.getCurrentSession().load(Phone.class, id);
        if (null != phone) {
            sessionFactory.getCurrentSession().delete(phone);
        }
    }

	public Phone getPhone(Integer id) {
		return (Phone)sessionFactory.getCurrentSession().createQuery("from Phone where ID = " + id).uniqueResult();
	}
}
