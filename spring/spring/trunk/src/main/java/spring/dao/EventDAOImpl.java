/**
 * 
 */
package spring.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.domain.Event;

/**
 * @author jconley
 *
 */
@Repository
public class EventDAOImpl implements EventDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    public void addEvent(Event event) {
        sessionFactory.getCurrentSession().save(event);
    }
 
    public List<Event> listEvent() {
        return sessionFactory.getCurrentSession().createQuery("from Event").list();
    }
 
    public void deleteEvent(Integer id) {
        Event event = (Event) sessionFactory.getCurrentSession().load(Event.class, id);
        if (null != event) {
            sessionFactory.getCurrentSession().delete(event);
        }
    }

	public Event getEvent(Integer id) {
		return (Event)sessionFactory.getCurrentSession().createQuery("from Event where ID = " + id).uniqueResult();
	}
}
