package spring.jobs;

import java.util.List;

import spring.domain.Contact;
import spring.service.ContactService;
import spring.service.MailService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class BirthdayJob {
	private static Logger log = Logger.getLogger(BirthdayJob.class);
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private MailService mailService;
	
    //				    s m h dom mon dow
	//@Scheduled(cron="50 * * *   *   *")
    //@Scheduled(cron="30 30 8 * * *")
	@Scheduled(cron="30 30 8 * * *")
    public void runAlerts(){
    	log.info("Running alerts");
    	/*
    	ClassPathXmlApplicationContext con = new ClassPathXmlApplicationContext("classpath*:META-INF/spring/applicationContext*.xml");
    	BeanFactory factory = con;
		MailUtility test = (MailUtility) factory.getBean("mailUtility");
    	*/
    	
		List<Contact> birthdays = contactService.listContact();
		for(Contact c : birthdays){
			//String bday = c.getFirstName() + " " + c.getLastName() + "'s Birthday!";
			//test.sendMessage("joe@pensieve.com","Test","josephpconley@gmail.com",bday);
			log.info(c.toString());
			//mailService.sendMessage("test@123.com","josephpconley@gmail.com","test",c.toString());
		}
    }
}
