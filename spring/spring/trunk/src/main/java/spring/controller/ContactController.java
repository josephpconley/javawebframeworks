package spring.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import spring.domain.Contact;
import spring.service.ContactService;

@Controller
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	private ContactService contactService;

	@RequestMapping("/list")
	public String listContacts(Map<String, Object> map) {
		map.put("contact", new Contact());
		map.put("contactList", contactService.listContact());
		return "contacts/list";
	}

	@RequestMapping("/{id}")
	public String getContact(@PathVariable("id") Integer contactId,Map<String, Object> map) {
		Contact contact = contactService.getContact(contactId);
		System.out.println(contact.toString());

		map.put("contact",contact);
		return "contacts/show";
	}
	
	//ALWAYS redirect after DELETES/UPDATES
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("contact") Contact contact, BindingResult result) {
		contactService.addContact(contact);
		return "redirect:/contacts/list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addContact(@RequestBody Contact contact) {
		contactService.addContact(contact);
	}	
	
	@RequestMapping("/delete/{id}")
	public String deleteContact(@PathVariable("id")	Integer contactId) {
		contactService.deleteContact(contactId);
		return "redirect:/contacts/list";
	}
}
